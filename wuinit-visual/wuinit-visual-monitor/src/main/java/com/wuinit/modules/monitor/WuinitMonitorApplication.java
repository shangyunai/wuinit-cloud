package com.wuinit.modules.monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import de.codecentric.boot.admin.server.config.EnableAdminServer;

/**
 * 监控中心
 *
 * @author wuinit
 */
@EnableAdminServer
@SpringBootApplication
public class WuinitMonitorApplication {
    public static void main(String[] args) {
        SpringApplication.run(WuinitMonitorApplication.class, args);
        System.out.println("======= wuinit monitor start =======");
    }
}
