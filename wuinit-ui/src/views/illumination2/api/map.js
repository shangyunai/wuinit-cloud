import request from '@/utils/request'

// 地图要素编辑
export function geoserverWfs(params) {
  return request({
    url: '/illumination/street/geoserver/wfs',
    headers: {
      isToken: false
    },
    method: 'post',
    data: params
  })
}

//获取uuid
export function getUUID() {
  return request({
    url: '/illumination/street/get/uuid',
    headers: {
      isToken: false
    },
    method: 'get'
  })
}

//获取道路列表树
export function getStreetTree(status) {
  return request({
    url: '/illumination/street/get/street/tree?status=' + status,
    headers: {
      isToken: false
    },
    method: 'get',
  })
}

//获取 geoserver 配置
export function getGeoserverConfig() {
  return request({
    url: '/illumination/street/get/geoserver/config',
    headers: {
      isToken: false
    },
    method: 'get'
  })
}

//根据点获取道路信息
export function getStreetByPoint(pointStr) {
  return request({
    url: '/illumination/street/get/street/by/point?pointStr=' + pointStr,
    headers: {
      isToken: false
    },
    method: 'get',
  })
}

//根据点获取道路信息
export function getStreetByUuid(params) {
  return request({
    url: '/illumination/street/list',
    headers: {
      isToken: false
    },
    method: 'get',
    params: params
  })
}

export function getStreets(params) {
  return request({
    url: '/illumination/street/list',
    headers: {
      isToken: false
    },
    method: 'get',
    params: params
  })
}

//获取所有点信息
export function getAllPoint(params) {
  return request({
    url: '/illumination/point/all',
    headers: {
      isToken: false
    },
    method: 'get',
    params: params
  })
}

//获取所有点信息
export function getAllPointCheck(params) {
  return request({
    url: '/illumination/point/all/check',
    headers: {
      isToken: false
    },
    method: 'get',
    params: params
  })
}

//分页查询点信息
export function getPoints(params) {
  return request({
    url: '/illumination/point/list',
    headers: {
      isToken: false
    },
    method: 'get',
    params: params
  })
}


// 根据uuid获取点集合
export function getPointByUuid(params) {
  return request({
    url: '/illumination/point/by/uuid',
    headers: {
      isToken: false
    },
    method: 'post',
    data: params
  })
}


// 根据uuid获取点集合
export function updateStreetStatus() {
  return request({
    url: '/illumination/street/update/street/status',
    headers: {
      isToken: false
    },
    method: 'get',
  })
}

// 更新街道信息
export function updateStreet(params) {
  return request({
    url: '/illumination/street',
    headers: {
      isToken: false
    },
    method: 'put',
    data: params
  })
}

//kafka通知正在检测的道路信息
export function sendStreetToKafka(uuid) {
  return request({
    url: '/illumination/street/send/kafka?uuid=' + uuid,
    headers: {
      isToken: false
    },
    method: 'get'
  })
}

//kafka通知检测状态
export function sendStreetCheckStatusToKafka(status) {
  return request({
    url: '/illumination/street/send/kafka/check/status?status=' + status,
    headers: {
      isToken: false
    },
    method: 'get'
  })
}

//获取道路额外属性信息
export function getStreetExt(uuid) {
  return request({
    url: '/illumination/street/ext/get?uuid=' + uuid,
    headers: {
      isToken: false
    },
    method: 'get',
  })
}

// 保存道路额外属性信息
export function saveStreetExt(params) {
  return request({
    url: '/illumination/street/ext/save',
    headers: {
      isToken: false
    },
    method: 'post',
    data: params
  })
}

// 获取道路检测结果
export function getStreetResult() {
  return request({
    url: '/illumination/street/result/list/all',
    headers: {
      isToken: false
    },
    method: 'post',
  })
}

// 获取道路绑定图片信息
export function getPointImageUrl(params) {
  return request({
    url: '/illumination/point/get/image/url',
    headers: {
      isToken: false
    },
    method: 'post',
    data: params
  })
}

// 获取道路绑定图片信息列表
export function getPointImagesUrl(params) {
  return request({
    url: '/illumination/point/get/image/url/list',
    headers: {
      isToken: false
    },
    method: 'post',
    data: params
  })
}

// 获取有备注不能正常检测的道路
export function getCanNotCheck() {
  return request({
    url: '/illumination/street/get/can/not/check',
    headers: {
      isToken: false
    },
    method: 'get',
  })
}
