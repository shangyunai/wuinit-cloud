import request from '@/utils/request'

//补全交叉点
export function completeCrossLinePoints() {
  return request({
    url: '/illumination/dataclean/complete/cross/line/point',
    headers: {
      isToken: false
    },
    method: 'get',
  })
}

//补全道路缺失的道路框外点
export function fixStreetLinePoint() {
  return request({
    url: '/illumination/dataclean/fix/street/line/point',
    headers: {
      isToken: false
    },
    method: 'get',
  })
}

//判断点合格性
export function checkPointQualification() {
  return request({
    url: '/illumination/dataclean/check/point/qualification',
    headers: {
      isToken: false
    },
    method: 'get',
  })
}

//判断道路合格性
export function checkStreetQualification() {
  return request({
    url: '/illumination/dataclean/check/street/qualification',
    headers: {
      isToken: false
    },
    method: 'get',
  })
}

//重新计算点位所属道路信息
export function recalculatePoints() {
  return request({
    url: '/illumination/dataclean/recalculate/point',
    headers: {
      isToken: false
    },
    method: 'get',
  })
}

//实时校验时间范围内，上传点位结果
export function dataResultNow(params) {
  return request({
    url: '/illumination/dataclean/now/by/uuid',
    headers: {
      isToken: false
    },
    method: 'post',
    data: params
  })
}
