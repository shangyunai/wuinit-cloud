import request from '@/utils/request'

//根据状态获取道路树列表
export function getStreetTree(status) {
  return request({
    url: '/illumination/show/result/get/street/tree?status=' + status,
    headers: {
      isToken: false
    },
    method: 'get',
  })

}

//根据状态获取道路树列表
export function getRegionStreetRate() {
  return request({
    url: '/illumination/show/result/get/region/street/rate',
    headers: {
      isToken: false
    },
    method: 'get',
  })

}

//按道路类型统计照度区间
export function getStreetLightRateByType(streetType) {
  return request({
    url: '/illumination/show/result/get/street/light/rate/by/type?streetType=' + streetType,
    headers: {
      isToken: false
    },
    method: 'get',
  })

}


