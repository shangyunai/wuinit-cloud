let dateFormat = {
    padLeftZero: function (str) {
        return ('00' + str).substr(str.length)
    },
    formatDate: function (date, fmt) {
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
        }
        let o = {
            'M+': date.getMonth() + 1,
            'd+': date.getDate(),
            'h+': date.getHours(),
            'm+': date.getMinutes(),
            's+': date.getSeconds()
        }
        for (let k in o) {
            if (new RegExp(`(${k})`).test(fmt)) {
                let str = o[k] + ''
                fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : this.padLeftZero(str))
            }
        }
        return fmt
    },
    //计算剩余天数
    calculateDays: function (startTime, endTime) {
        let days = '-'
        let daysTime = endTime - startTime;
        if (daysTime > 0) {
            days = daysTime / (24 * 60 * 60 * 1000);
            days = Math.ceil(days)
        }
        return days
    },
    //获取当天0点时间
    getDayStartTime: function (time) {
        return new Date(new Date(new Date(time).toLocaleDateString()).getTime()).getTime()
    },
    //获取当天24点时间
    getEndStartTime: function (time) {
        return new Date(new Date(new Date(time).toLocaleDateString()).getTime() + 24 * 60 * 60 * 1000).getTime()
    },
    //获取当前时间
    getNowTime: function () {
        return new Date().getTime()
    },
    //判断时间跨度是否超出180
    checkTimeSpan(startTime, endTime) {
        let dayStart = dateFormat.getDayStartTime(startTime)
        let dayEnd = dateFormat.getEndStartTime(endTime)
        let timeSpan = dayEnd - dayStart
        let days = timeSpan / (24 * 60 * 60 * 1000);
        days = Math.ceil(days)
        if (days > 180) {
            return false
        } else {
            return true
        }
    },
    getWeekDay(date) {
        let week;
        if (date.getDay() == 0) week = "7"
        if (date.getDay() == 1) week = "1"
        if (date.getDay() == 2) week = "2"
        if (date.getDay() == 3) week = "3"
        if (date.getDay() == 4) week = "4"
        if (date.getDay() == 5) week = "5"
        if (date.getDay() == 6) week = "6"
        return week;
    },
}


export default dateFormat;
