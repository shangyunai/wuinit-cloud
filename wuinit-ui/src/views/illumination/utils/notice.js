import {Message} from 'element-ui';

function notice(msg, type) {
  Message({
    message: msg,
    type: type,
    offset: window.innerHeight / 2,
  })
}

export {
  notice
}
