// 初始化cesium地图 JS文件
// 首先获取Cesium API
const Cesium = window.Cesium;
let viewer = null;
let tiandituSlProvider = null;
let tiandituSlBzProvider = null;
let tiandituYxProvider = null;
let tiandituWpProvider = null;
let cdArcgisWpProvider = null

/**
 * 初始化地球视图函数
 */
function initCesiumMap(dom) {
  //天地图key
  let tdtKey = "7dc4a1a27bb4565b87ddba9ba762ca4a"
  // 配置cesium专属Access Tokens,就是cesium的访问令牌，每一个使用cesium的用户都需要自己注册，然后获取自己的Access Tokens；
  Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIyNGU4M2UwZC1iMjQzLTRhZTQtYjU4MC03MjEyYWM4ZmRhOTEiLCJpZCI6MTYzMDMzLCJpYXQiOjE2OTMyMTM0MTZ9.LmsdYsHfCcichY3j33-a4pX7eUJCmbwaMRa4Lix1Hcs'
  viewer = new Cesium.Viewer(dom, {
    animation: false, // 是否显示动画控件
    baseLayerPicker: false, // 是否显示图层选择控件
    vrButton: false, // 是否显示VR控件
    geocoder: false, // 是否显示地名查找控件
    timeline: false, // 是否显示时间线控件
    sceneModePicker: false, // 是否显示投影方式控件
    navigationHelpButton: false, // 是否显示帮助信息控件
    navigationInstructionsInitiallyVisible: true, // 帮助按钮，初始化的时候是否展开
    infoBox: false, // 是否显示点击要素之后显示的信息
    fullscreenButton: false, // 是否显示全屏按钮
    selectionIndicator: false, // 是否显示选中指示框
    homeButton: false, // 是否显示返回主视角控件
    scene3DOnly: true, // 如果设置为true，则所有几何图形以3D模式绘制以节约GPU资源
    contextOptions: {
      requestWebgl1: true,
    }
  })
  // 去掉logo
  viewer.cesiumWidget.creditContainer.style.display = "none";
  /*矢量地图*/
  tiandituSlProvider = new Cesium.WebMapTileServiceImageryProvider({
    url: "http://t0.tianditu.gov.cn/vec_w/wmts?tk=" + tdtKey,
    layer: "vec",
    style: "default",
    format: "tiles",
    tileMatrixSetID: "w",
    credit: new Cesium.Credit("天地图"),
    maximumLevel: 18,
    tileWidth: 256,
    tileHeight: 256,
    subdomains: ["t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7"],
    tilingScheme: new Cesium.WebMercatorTilingScheme(),
    show: true,
  });
  /*矢量标注*/
  tiandituSlBzProvider = new Cesium.WebMapTileServiceImageryProvider({
    url: "http://t0.tianditu.gov.cn/cva_w/wmts?tk=" + tdtKey,
    layer: "cva",
    style: "default",
    format: "tiles",
    tileMatrixSetID: "w",
    credit: new Cesium.Credit("天地图"),
    maximumLevel: 18,
    tileWidth: 256,
    tileHeight: 256,
    subdomains: ["t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7"],
    tilingScheme: new Cesium.WebMercatorTilingScheme(),
    show: true,
  });
  /*影像地图*/
  // tiandituYxProvider = new Cesium.WebMapTileServiceImageryProvider({
  //   url: "http://t0.tianditu.gov.cn/img_w/wmts?tk=" + tdtKey,
  //   layer: "img",
  //   style: "default",
  //   format: "tiles",
  //   tileMatrixSetID: "w",
  //   credit: new Cesium.Credit("天地图"),
  //   maximumLevel: 18,
  //   tileWidth: 256,
  //   tileHeight: 256,
  //   subdomains: ["t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7"],
  //   tilingScheme: new Cesium.WebMercatorTilingScheme(),
  //   show: true,
  // });
  /*瓦片地图*/
  // tiandituWpProvider = new Cesium.WebMapTileServiceImageryProvider({
  //   url: "http://{s}.tianditu.gov.cn/vec_w/wmts?service=wmts&request=GetTile&version=1.0.0" + "&LAYER=vec&tileMatrixSet=w&TileMatrix={TileMatrix}&TileRow={TileRow}&TileCol={TileCol}" + "&style=default&format=tiles&tk=" + tdtKey,
  //   layer: "img",
  //   style: "default",
  //   format: "tiles",
  //   tileMatrixSetID: "w",
  //   credit: new Cesium.Credit("天地图"),
  //   maximumLevel: 18,
  //   tileWidth: 256,
  //   tileHeight: 256,
  //   subdomains: ["t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7"],
  //   tilingScheme: new Cesium.WebMercatorTilingScheme(),
  //   show: true,
  // })
}

function zeroFill(num, len, radix) {
  let str = num.toString(radix || 10)
  while (str.length < len) {
    str = '0' + str
  }
  return str
}

// 导出
export {
  Cesium,
  viewer,
  initCesiumMap,
  tiandituSlProvider,
  tiandituSlBzProvider,
  tiandituYxProvider,
  tiandituWpProvider,
  cdArcgisWpProvider
};
