import {Loading} from 'element-ui';

let loading = null

function showLoading() {
  loading = Loading.service({
    fullscreen: true, lock: true, text: 'Loading', spinner: 'el-icon-loading', background: 'rgba(0, 0, 0, 0.7)'
  });
  setTimeout(() => {
    loading.close();
  }, 30000);
}

function closeLoading() {
  if (loading) {
    loading.close();
  }
}

export {
  showLoading, closeLoading
}
