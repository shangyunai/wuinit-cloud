import Vue from 'vue';
import BigView from '@/components/img'; //引入刚才写的弹框组件

let AlertConstructor = Vue.extend(BigView);
const imageCommon = {
    showBig(url) {
        let alertDom = new AlertConstructor({
            el: document.createElement('div')
        });
        alertDom.showBigImage(url);
        document.body.appendChild(alertDom.$el);
    }
}
export default imageCommon
