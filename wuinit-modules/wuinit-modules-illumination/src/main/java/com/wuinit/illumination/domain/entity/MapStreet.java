package com.wuinit.illumination.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuinit.common.core.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 地图街道对象 map_street
 *
 * @author wuinit
 * @date 2023-09-30
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "map_street")
public class MapStreet implements Serializable {
    private static final long serialVersionUID = 2L;

    /**
     * 应用ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 道路唯一标识
     */
    private String uuid;
    /**
     * 省
     */
    @Excel(name = "省")
    private String province;

    /**
     * 市
     */
    @Excel(name = "市")
    private String city;

    /**
     * 区
     */
    @Excel(name = "区")
    private String region;

    /**
     * 道路名称
     */
    @Excel(name = "道路名称")
    private String streetName;

    /**
     * 道路等级
     */
    @Excel(name = "道路等级")
    private String streetLevel;

    /**
     * 道路描述
     */
    @Excel(name = "道路描述")
    private String streetDecs;

    /**
     * 道路类型
     */
    @Excel(name = "道路类型")
    private String streetType;

    /**
     * 道路状态
     */
    @Excel(name = "道路状态")
    private String streetStatus;

    /**
     * 道路中心点
     */
    @Excel(name = "道路中心点")
    private String streetCenter;

    /**
     * 道路起点
     */
    @Excel(name = "道路起点")
    private String streetStart;

    /**
     * 道路终点
     */
    @Excel(name = "道路终点")
    private String streetEnd;
}
