package com.wuinit.illumination.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuinit.illumination.domain.entity.MapPointImage;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MapPointImageMapper extends BaseMapper<MapPointImage> {

}
