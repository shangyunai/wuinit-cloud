package com.wuinit.illumination;


import com.wuinit.illumination.datacleaning.DataCleaningUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


@Component
@Order(value = 2)
public class WuinitIlluminationApplicationRunner implements org.springframework.boot.ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(WuinitIlluminationApplicationRunner.class);

    @Autowired
    private DataCleaningUtil dataCleaningUtil;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("dataCleaning start");
        //dataCleaningUtil.fixStreetLinePoint();
        log.info("dataCleaning finish");
    }


}
