package com.wuinit.illumination.kafka;

import com.alibaba.nacos.shaded.com.google.gson.Gson;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuinit.common.core.utils.StringUtils;
import com.wuinit.illumination.api.MapApi;
import com.wuinit.illumination.config.GeoserverConfig;
import com.wuinit.illumination.domain.entity.MapPoint;
import com.wuinit.illumination.domain.entity.MapStreet;
import com.wuinit.illumination.domain.result.MapFeatureResult;
import com.wuinit.illumination.service.MapPointService;
import com.wuinit.illumination.service.MapStreetService;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class KafkaConsumer {

    @Autowired
    private MapPointService mapPointService;
    @Autowired
    private MapStreetService mapStreetService;
    @Autowired
    private GeoserverConfig geoserverConfig;
    @Autowired
    private RestTemplate restTemplate;

    @Value("${kafka.topic.point}")
    private String topicPoint;

    //监听消费
    @KafkaListener(topics = "${kafka.topic.point}")
    public void onMessage(ConsumerRecord<String, Object> record) {
        String value = record.value().toString();
        log.info("---value----:{}", value);
        try {
            KafkaMessage kafkaMessage = new Gson().fromJson(value, KafkaMessage.class);
            String lon = kafkaMessage.getLon();
            String lat = kafkaMessage.getLat();
            if (StringUtils.isNotEmpty(lon) && StringUtils.isNotEmpty(lat)) {
                //查询属于那条街道
                List<String> uuids = new ArrayList<>();
                String point = "POINT(" + lon.trim() + " " + lat.trim() + ")";
                //相交
                String streetByPoint = MapApi.getInstance().getStreetByPoint(point, geoserverConfig, restTemplate);
                if (streetByPoint != null) {
                    MapFeatureResult mapFeatureResult = new Gson().fromJson(streetByPoint, MapFeatureResult.class);
                    if (mapFeatureResult != null) {
                        List<MapFeatureResult.FeaturesDTO> features = mapFeatureResult.getFeatures();
                        if (!Collections.isEmpty(features)) {
                            features.stream().forEach(feature -> {
                                MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                if (properties != null) {
                                    String uuid = properties.getUuid();
                                    if (StringUtils.isNotEmpty(uuid)) {
                                        uuids.add(uuid);
                                    }
                                }
                            });
                        } else {
                            //包含
                            String streetByPointContains = MapApi.getInstance().getStreetByPointContains(point, geoserverConfig, restTemplate);
                            if (streetByPointContains != null) {
                                MapFeatureResult mapFeatureResultContains = new Gson().fromJson(streetByPointContains, MapFeatureResult.class);
                                if (mapFeatureResultContains != null) {
                                    List<MapFeatureResult.FeaturesDTO> featuresContains = mapFeatureResultContains.getFeatures();
                                    if (!Collections.isEmpty(featuresContains)) {
                                        featuresContains.stream().forEach(feature -> {
                                            MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                            if (properties != null) {
                                                String uuid = properties.getUuid();
                                                if (StringUtils.isNotEmpty(uuid)) {
                                                    uuids.add(uuid);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
                MapPoint mapPoint = new MapPoint();
                mapPoint.setLon(new BigDecimal(kafkaMessage.getLon().trim()));
                mapPoint.setLat(new BigDecimal(kafkaMessage.getLat().trim()));
                String time = kafkaMessage.getTime();
                if (StringUtils.isNotEmpty(time)) {
                    mapPoint.setTime(Long.valueOf(time.trim()));
                }
                String region = kafkaMessage.getRegion();
                if (StringUtils.isNotEmpty(region)) {
                    mapPoint.setRegion(region.trim());
                }
                String roadname = kafkaMessage.getRoadname();
                if (StringUtils.isNotEmpty(roadname)) {
                    mapPoint.setRoadname(roadname.trim());
                }
                String roadlevel = kafkaMessage.getRoadlevel();
                if (StringUtils.isNotEmpty(roadlevel)) {
                    mapPoint.setRoadlevel(roadlevel.trim());
                }
                String lightdistribution = kafkaMessage.getLightdistribution();
                if (StringUtils.isNotEmpty(lightdistribution)) {
                    mapPoint.setLightdistribution(lightdistribution.trim());
                }
                String lighttype = kafkaMessage.getLighttype();
                if (StringUtils.isNotEmpty(lighttype)) {
                    mapPoint.setLighttype(lighttype.trim());
                }
                boolean dataOk = kafkaMessage.isDataOk();
                if (dataOk) {
                    mapPoint.setIsDataOk(1);
                } else {
                    mapPoint.setIsDataOk(0);
                }
                String dataCuuid = kafkaMessage.getDataCuuid();
                if (StringUtils.isNotEmpty(dataCuuid)) {
                    mapPoint.setDataCuuid(dataCuuid.trim());
                }
                mapPoint.setLightAverage(new BigDecimal(kafkaMessage.getLightAverage().trim()));
                mapPoint.setLightuniformity(new BigDecimal(kafkaMessage.getLightuniformity().trim()));
                mapPoint.setAveragebrightness(new BigDecimal(kafkaMessage.getAveragebrightness().trim()));
                //判断亮度均匀度问题
                if ("nan".equals(kafkaMessage.getBrightnessuniformity().trim())) {
                    mapPoint.setBrightnessuniformity(new BigDecimal(0));
                } else {
                    mapPoint.setBrightnessuniformity(new BigDecimal(kafkaMessage.getBrightnessuniformity().trim()));
                }
                mapPoint.setMax(new BigDecimal(kafkaMessage.getMax().trim()));
                mapPoint.setMin(new BigDecimal(kafkaMessage.getMin().trim()));
                mapPoint.setLongitudinal(new BigDecimal(kafkaMessage.getLongitudinal().trim()));
                mapPoint.setSpeed(new BigDecimal(kafkaMessage.getSpeed().trim()));
                String compliance = kafkaMessage.getCompliance();
                if (StringUtils.isNotEmpty(compliance)) {
                    mapPoint.setCompliance(compliance.trim());
                }
                String result = kafkaMessage.getResult();
                if (StringUtils.isNotEmpty(result)) {
                    mapPoint.setResult(result.trim());
                }
                //判断重复点位不入库
                QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
                mapPointQueryWrapper.eq("lon", mapPoint.getLon());
                mapPointQueryWrapper.eq("lat", mapPoint.getLat());
                mapPointQueryWrapper.eq("time", mapPoint.getTime());
                MapPoint mapPointOne = mapPointService.getOne(mapPointQueryWrapper, false);
                if (mapPointOne == null) {
                    if (!Collections.isEmpty(uuids)) {
                        uuids.stream().forEach(uuid -> {
                            mapPoint.setStreetUuid(uuid);
                            mapPointService.save(mapPoint);
                            if (mapPoint.getIsDataOk() == 1) {
                                //有效数据，判断合格性
                                QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
                                mapStreetQueryWrapper.eq("uuid", uuid);
                                MapStreet mapStreet = mapStreetService.getOne(mapStreetQueryWrapper, false);
                                if (mapStreet != null) {
                                    String streetType = mapStreet.getStreetType();
                                    BigDecimal lightAverage = mapPoint.getLightAverage();
                                    switch (streetType) {
                                        case "主干路":
                                        case "快速路":
                                            if (lightAverage.compareTo(new BigDecimal(30)) >= 0) {
                                                //合格
                                                mapPoint.setCompliance("合格");
                                            } else {
                                                //偏低
                                                mapPoint.setCompliance("偏低");
                                            }
                                            break;
                                        case "次干路":
                                            if (lightAverage.compareTo(new BigDecimal(20)) >= 0) {
                                                //合格
                                                mapPoint.setCompliance("合格");
                                            } else {
                                                //偏低
                                                mapPoint.setCompliance("偏低");
                                            }
                                            break;
                                        case "支路":
                                            if (lightAverage.compareTo(new BigDecimal(10)) >= 0) {
                                                //合格
                                                mapPoint.setCompliance("合格");
                                            } else {
                                                //偏低
                                                mapPoint.setCompliance("偏低");
                                            }
                                            break;
                                    }
                                    mapPointService.updateById(mapPoint);
                                    log.info("=======update point qualification:{}======", mapPoint.getCompliance());
                                }
                            }
                            log.info("------ insert mappoint ok -------");
                        });
                    } else {
                        mapPointService.save(mapPoint);
                        log.info("------ insert mappoint ok -------");
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("kafka save mappoint error:[{}]", e.toString());
        }

    }
}
