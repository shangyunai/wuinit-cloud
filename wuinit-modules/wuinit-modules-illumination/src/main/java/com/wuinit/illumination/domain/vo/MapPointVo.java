package com.wuinit.illumination.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuinit.common.core.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 地图点位对象 map_point
 *
 * @author wuinit
 * @date 2023-10-17
 */
@Data
public class MapPointVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 经度
     */
    @Excel(name = "经度")
    private BigDecimal lon;

    /**
     * 纬度
     */
    @Excel(name = "纬度")
    private BigDecimal lat;

    /**
     * 街道uuid
     */
    @Excel(name = "街道uuid")
    private String streetUuid;

    /**
     * 测量时间戳
     */
    @Excel(name = "测量时间戳")
    private Long time;

    /**
     * 区域名称
     */
    @Excel(name = "区域名称")
    private String Region;

    /**
     * 道路名称
     */
    @Excel(name = "道路名称")
    private String Roadname;

    /**
     * 道路等级
     */
    @Excel(name = "道路等级")
    private String Roadlevel;

    /**
     * 路灯分布
     */
    @Excel(name = "路灯分布")
    private String lightdistribution;

    /**
     * 路灯类型
     */
    @Excel(name = "路灯类型")
    private String lighttype;

    /**
     * 平均照度
     */
    @Excel(name = "平均照度")
    private BigDecimal lightAverage;

    /**
     * 照度均匀度
     */
    @Excel(name = "照度均匀度")
    private BigDecimal lightuniformity;

    /**
     * 平均亮度
     */
    @Excel(name = "平均亮度")
    private BigDecimal Averagebrightness;

    /**
     * 亮度均匀度
     */
    @Excel(name = "亮度均匀度")
    private BigDecimal Brightnessuniformity;

    /**
     * 照度最大值
     */
    @Excel(name = "照度最大值")
    private BigDecimal max;

    /**
     * 照度最小值
     */
    @Excel(name = "照度最小值")
    private BigDecimal min;

    /**
     * 纵向均匀度
     */
    @Excel(name = "纵向均匀度")
    private BigDecimal Longitudinal;

    /**
     * 测量速度
     */
    @Excel(name = "测量速度")
    private BigDecimal speed;

    /**
     * 道路是否达标 达标/ 不合格
     */
    @Excel(name = "道路是否达标 达标/ 不合格")
    private String Compliance;

    /**
     * 陡增减结果 正常/陡增/陡降
     */
    @Excel(name = "陡增减结果 正常/陡增/陡降")
    private String result;

    /**
     * 是否是有效数据 0 无效 1 有效
     */
    @Excel(name = "是否是有效数据 0 无效 1 有效")
    private Integer isDataOk;

    /**
     * 检测道路uuid
     */
    @Excel(name = "检测道路uuid")
    private String dataCuuid;

    /**
     * 检测点抓拍图片
     */
    @Excel(name = "检测点抓拍图片")
    private String imageUrl;

}
