package com.wuinit.illumination.controller;

import com.wuinit.common.core.utils.poi.ExcelUtil;
import com.wuinit.common.core.web.controller.BaseController;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.common.core.web.page.PageDomain;
import com.wuinit.common.core.web.page.TableDataInfo;
import com.wuinit.common.core.web.page.TableSupport;
import com.wuinit.common.log.annotation.Log;
import com.wuinit.common.log.enums.BusinessType;
import com.wuinit.illumination.domain.entity.MapStreet;
import com.wuinit.illumination.domain.entity.MapStreetExt;
import com.wuinit.illumination.domain.entity.MapStreetResult;
import com.wuinit.illumination.domain.form.MapStreetForm;
import com.wuinit.illumination.domain.query.MapStreetResultQuery;
import com.wuinit.illumination.service.MapStreetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 地图街道Controller
 *
 * @author wuinit
 * @date 2023-09-30
 */
@RestController
@RequestMapping("/street")
public class MapStreetController extends BaseController {
    @Autowired
    private MapStreetService mapStreetService;

    /**
     * 查询地图街道列表
     */
    //@RequiresPermissions("illumination:street:list")
    @GetMapping("/list")
    public TableDataInfo list(MapStreet mapStreet) {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        return mapStreetService.list(mapStreet, pageNum, pageSize);
    }

    /**
     * 导出地图街道列表
     */
    //@RequiresPermissions("illumination:street:export")
    @Log(title = "地图街道", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MapStreet mapStreet) {
        List<MapStreet> list = new ArrayList<>();
        ExcelUtil<MapStreet> util = new ExcelUtil<MapStreet>(MapStreet.class);
        util.exportExcel(response, list, "地图街道数据");
    }

    /**
     * 获取地图街道详细信息
     */
    //@RequiresPermissions("illumination:street:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return mapStreetService.getInfo(id);
    }

    /**
     * 新增地图街道
     */
    //@RequiresPermissions("illumination:street:add")
    @Log(title = "地图街道", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MapStreet mapStreet) {
        return mapStreetService.insert(mapStreet);
    }

    /**
     * 修改地图街道
     */
    //@RequiresPermissions("illumination:street:edit")
    @Log(title = "地图街道", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MapStreet mapStreet) {
        return mapStreetService.update(mapStreet);
    }

    /**
     * 删除地图街道
     */
    //@RequiresPermissions("illumination:street:remove")
    @Log(title = "地图街道", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return mapStreetService.delete(ids);
    }

    /**
     * 地图服务WFS 地图要素操作
     *
     * @param mapStreetForm
     * @return
     */
    @PostMapping("/geoserver/wfs")
    public AjaxResult geoserverWfs(@RequestBody MapStreetForm mapStreetForm) {
        return mapStreetService.geoserverWfs(mapStreetForm);
    }

    /**
     * 获取道路唯一标识
     *
     * @return
     */
    @GetMapping("/get/uuid")
    public AjaxResult getUUID() {
        return mapStreetService.getUUID();
    }

    /**
     * 获取道路列表树
     *
     * @return
     */
    @GetMapping("/get/street/tree")
    public AjaxResult getStreetTree(String status) {
        return mapStreetService.getStreetTree(status);
    }

    /**
     * 获取地图相关配置
     *
     * @return
     */
    @GetMapping("/get/geoserver/config")
    public AjaxResult getGeoserverConfig() {
        return mapStreetService.getGeoserverConfig();
    }

    /**
     * 获取道路地图
     *
     * @return
     */
    @GetMapping("/get/geoserver/street/map")
    public String getGeoserverStreetMap() {
        return mapStreetService.getGeoserverStreetMap();
    }

    /**
     * 根据点获取道路信息
     *
     * @return
     */
    @GetMapping("/get/street/by/point")
    public String getStreetByPoint(String pointStr) {
        return mapStreetService.getStreetByPoint(pointStr);
    }

    /**
     * 更新道路检测状态
     *
     * @return
     */
    @GetMapping("/update/street/status")
    public AjaxResult updateStreetStatus() {
        return mapStreetService.updateStreetStatus();
    }

    /**
     * kafka 下发道路信息
     *
     * @param uuid
     * @return
     */
    @GetMapping("/send/kafka")
    public AjaxResult sendKafka(String uuid) {
        return mapStreetService.sendKafka(uuid);
    }

    /**
     * kafka 下发道路信息
     *
     * @param status
     * @return
     */
    @GetMapping("/send/kafka/check/status")
    public AjaxResult sendKafkaCheckStatus(String status) {
        return mapStreetService.sendKafkaCheckStatus(status);
    }

    /**
     * 保存道路额外属性信息
     *
     * @param mapStreetExt
     * @return
     */
    @PostMapping("/ext/save")
    public AjaxResult extSave(@RequestBody MapStreetExt mapStreetExt) {
        return mapStreetService.streetExtSave(mapStreetExt);
    }

    /**
     * 获取道路额外属性信息
     *
     * @param uuid
     * @return
     */
    @GetMapping("/ext/get")
    public AjaxResult extGet(String uuid) {
        return mapStreetService.streetExtGet(uuid);
    }

    /**
     * 获取道路检测结果详细信息
     *
     * @param mapStreetResultQuery
     * @return
     */
    @PostMapping("/result")
    public TableDataInfo resultList(@RequestBody MapStreetResultQuery mapStreetResultQuery) {
        return mapStreetService.resultList(mapStreetResultQuery);
    }

    /**
     * 获取道路检测结果列表
     *
     * @return
     */
    @PostMapping("/result/list/all")
    public AjaxResult resultListAll() {
        return mapStreetService.resultListAll();
    }

    /**
     * 获取有备注的道路，标识不能进行检测的道路
     *
     * @return
     */
    @GetMapping("/get/can/not/check")
    public AjaxResult getCanNotCheck() {
        return mapStreetService.getCanNotCheck();
    }
}

