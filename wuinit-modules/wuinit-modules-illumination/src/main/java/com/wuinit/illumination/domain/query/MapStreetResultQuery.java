package com.wuinit.illumination.domain.query;

import lombok.Data;

@Data
public class MapStreetResultQuery extends PageQuery {
    /**
     * 检测状态
     */
    private String status;
}
