package com.wuinit.illumination.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "geoserver")
public class GeoserverConfig {
    private String baseMapToken;
    private String baseDarkMapUrl;
    private String baseWhiteMapUrl;
    private String baseMapSearchUrl;
    private String wfsUrl;
    private String streetUrl;
    private String editStreetUrl;
}
