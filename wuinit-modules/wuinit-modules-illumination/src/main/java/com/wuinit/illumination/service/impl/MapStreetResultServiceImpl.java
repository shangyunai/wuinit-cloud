package com.wuinit.illumination.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuinit.common.datasource.annotation.Slave;
import com.wuinit.illumination.domain.entity.MapStreetResult;
import com.wuinit.illumination.mapper.MapStreetResultMapper;
import com.wuinit.illumination.service.MapStreetResultService;
import org.springframework.stereotype.Service;

@Service
public class MapStreetResultServiceImpl extends ServiceImpl<MapStreetResultMapper, MapStreetResult> implements MapStreetResultService {

    @Slave
    @Override
    public MapStreetResult getOneSlave(QueryWrapper<MapStreetResult> queryWrapper) {
        return getOne(queryWrapper, false);
    }

    @Slave
    @Override
    public boolean saveSlave(MapStreetResult mapStreetResult) {
        return save(mapStreetResult);
    }
}
