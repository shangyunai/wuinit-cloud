package com.wuinit.illumination.service;

import com.wuinit.common.core.web.domain.AjaxResult;

public interface ShowResultService {

    AjaxResult getStreetTree(String status);

    AjaxResult getRegionStreetRate();

    AjaxResult getStreetLightRateByType(String streetType);
}
