package com.wuinit.illumination.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wuinit.illumination.domain.entity.MapStreetResult;

public interface MapStreetResultService extends IService<MapStreetResult> {
    MapStreetResult getOneSlave(QueryWrapper<MapStreetResult> queryWrapper);

    boolean saveSlave(MapStreetResult mapStreetResult);
}
