package com.wuinit.illumination.utils;

import com.wuinit.illumination.domain.entity.MapPoint;
import lombok.Data;
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

import java.util.ArrayList;
import java.util.List;

public class LineInterpolatorUtil {
    @Data
    public static class LatLng {
        double latitude;
        double longitude;

        public LatLng(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    public static List<LatLng> interpolatePoints(List<LatLng> originalPoints, double interval) {
        if (originalPoints.size() < 2) {
            throw new IllegalArgumentException("Interpolation requires at least two points");
        }

        double[] distances = calculateDistances(originalPoints);
        double totalDistance = distances[distances.length - 1];

        List<LatLng> interpolatedPoints = new ArrayList<>();

        for (double distance = 0; distance <= totalDistance; distance += interval) {
            LatLng interpolatedLatLng = interpolateLatLng(originalPoints, distances, distance);
            interpolatedPoints.add(interpolatedLatLng);
        }

        return interpolatedPoints;
    }

    private static LatLng interpolateLatLng(List<LatLng> originalPoints, double[] distances, double targetDistance) {
        SplineInterpolator interpolator = new SplineInterpolator();
        PolynomialSplineFunction latFunction = interpolator.interpolate(distances, getLatitudes(originalPoints));
        PolynomialSplineFunction lonFunction = interpolator.interpolate(distances, getLongitudes(originalPoints));

        double lat = latFunction.value(targetDistance);
        double lon = lonFunction.value(targetDistance);

        return new LatLng(lat, lon);
    }

    private static double[] calculateDistances(List<LatLng> points) {
        double[] distances = new double[points.size()];
        distances[0] = 0;

        for (int i = 1; i < points.size(); i++) {
            distances[i] = distances[i - 1] + haversine(points.get(i - 1), points.get(i));
        }

        // Make sure distances are strictly increasing
        for (int i = 1; i < distances.length; i++) {
            if (distances[i] <= distances[i - 1]) {
                distances[i] = distances[i - 1] + 0.000001; // Add a small increment to ensure strict increase
            }
        }

        return distances;
    }

    private static double[] getLatitudes(List<LatLng> points) {
        double[] latitudes = new double[points.size()];
        for (int i = 0; i < points.size(); i++) {
            latitudes[i] = points.get(i).latitude;
        }
        return latitudes;
    }

    private static double[] getLongitudes(List<LatLng> points) {
        double[] longitudes = new double[points.size()];
        for (int i = 0; i < points.size(); i++) {
            longitudes[i] = points.get(i).longitude;
        }
        return longitudes;
    }

    private static double haversine(LatLng coord1, LatLng coord2) {
        final int R = 6371000; // 地球半径，单位米
        double lat1 = Math.toRadians(coord1.latitude);
        double lon1 = Math.toRadians(coord1.longitude);
        double lat2 = Math.toRadians(coord2.latitude);
        double lon2 = Math.toRadians(coord2.longitude);

        double dLat = lat2 - lat1;
        double dLon = lon2 - lon1;

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1) * Math.cos(lat2) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return R * c;
    }
    /**
     * 获取一个点在一组经纬度中最近的点
     *
     * @param targetPoint
     * @param points
     * @return
     */
    public static MapPoint findNearestPoint(LatLng targetPoint, List<MapPoint> points) {
        if (points == null || points.isEmpty()) {
            return null; // 或者抛出异常，表示输入点集为空
        }
        MapPoint nearestPoint = points.get(0);
        LatLng nearestLatLng = new LatLng(nearestPoint.getLat().doubleValue(), nearestPoint.getLon().doubleValue());
        double minDistance = haversine(targetPoint, nearestLatLng);

        for (MapPoint point : points) {
            LatLng pointLatLng = new LatLng(point.getLat().doubleValue(), point.getLon().doubleValue());
            double distance = haversine(targetPoint, pointLatLng);
            if (distance < minDistance) {
                minDistance = distance;
                nearestPoint = point;
            }
        }

        return nearestPoint;
    }
}
