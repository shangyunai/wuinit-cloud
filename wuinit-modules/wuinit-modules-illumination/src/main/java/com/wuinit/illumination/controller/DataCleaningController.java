package com.wuinit.illumination.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuinit.common.core.utils.DateUtils;
import com.wuinit.common.core.web.controller.BaseController;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.illumination.datacleaning.DataCleaningUtil;
import com.wuinit.illumination.domain.entity.MapPoint;
import com.wuinit.illumination.domain.entity.MapStreet;
import com.wuinit.illumination.domain.entity.MapStreetResult;
import com.wuinit.illumination.domain.query.DataCleanNowByUuidQuery;
import com.wuinit.illumination.service.MapPointService;
import com.wuinit.illumination.service.MapStreetService;
import com.wuinit.illumination.utils.LineInterpolatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * 数据清洗Controller
 *
 * @author wuinit
 * @date 2023-09-30
 */
@RestController
@RequestMapping("/dataclean")
public class DataCleaningController extends BaseController {
    @Autowired
    private DataCleaningUtil dataCleaningUtil;

    /**
     * 补全交叉点
     */
    @GetMapping("/complete/cross/line/point")
    public AjaxResult completeCrossLinePoints() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                dataCleaningUtil.completeCrossLinePoints();
            }
        }).start();
        return AjaxResult.success();
    }

    /**
     * 补全道路缺失的道路框外点
     */
    @GetMapping("/fix/street/line/point")
    public AjaxResult fixStreetLinePoint() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                dataCleaningUtil.fixStreetLinePoint();
            }
        }).start();
        return AjaxResult.success();
    }

    /**
     * 重新计算点位所属道路信息
     */
    @GetMapping("/recalculate/point")
    public AjaxResult recalculatePoints() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                dataCleaningUtil.recalculatePoints();
            }
        }).start();
        return AjaxResult.success();
    }

    /**
     * 判断点合格性
     */
    @GetMapping("/check/point/qualification")
    public AjaxResult checkPointQualification() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                dataCleaningUtil.checkPointQualification();
            }
        }).start();
        return AjaxResult.success();
    }

    /**
     * 判断道路合格性
     */
    @GetMapping("/check/street/qualification")
    public AjaxResult checkStreetQualification() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                dataCleaningUtil.checkStreetQualification();
            }
        }).start();
        return AjaxResult.success();
    }

    @Autowired
    private MapPointService mapPointService;
    @Autowired
    private MapStreetService mapStreetService;

    /**
     * 实时查询数据结构
     */
    @PostMapping("/now/by/uuid")
    public AjaxResult pointByTime(@RequestBody DataCleanNowByUuidQuery dataCleanNowByUuidQuery) {
        String uuid = dataCleanNowByUuidQuery.getUuid();
        MapPoint mapPoint = new MapPoint();
        mapPoint.setStreetUuid(uuid);
        List<List<MapPoint>> pointsList = mapPointService.getPointByUuid(mapPoint);
        List<MapPoint> list = new ArrayList<>();
        if (pointsList.size() > 0) {
            list.addAll(pointsList.get(0));
        }
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        mapStreetQueryWrapper.eq("uuid", uuid);
        MapStreet mapStreet = mapStreetService.getOne(mapStreetQueryWrapper, false);
        MapStreetResult mapStreetResult = new MapStreetResult();
        List<MapPoint> markPoints = new ArrayList<>();
        if (mapStreet != null && list.size() > 0) {
            //计算平均距离点
            List<LineInterpolatorUtil.LatLng> latLngs = new ArrayList<>();
            String streetType = mapStreet.getStreetType();
            //平均照度
            List<Double> lightAList = new ArrayList<>();
            //平均照度均匀度
            List<Double> lightUList = new ArrayList<>();
            //平均亮度
            List<Double> brightAList = new ArrayList<>();
            //平均亮度均匀度
            List<Double> brightUList = new ArrayList<>();
            //平均车速
            List<Double> speedAList = new ArrayList<>();
            //照度达标数量
            List<String> qualificationOk = new ArrayList<>();
            for (MapPoint point : list) {
                LineInterpolatorUtil.LatLng latLng = new LineInterpolatorUtil.LatLng(point.getLat().doubleValue(), point.getLon().doubleValue());
                latLngs.add(latLng);
                BigDecimal lightAverage = point.getLightAverage();
                switch (streetType) {
                    case "主干路":
                    case "快速路":
                        if (lightAverage.compareTo(new BigDecimal(30)) >= 0) {
                            //合格
                            point.setCompliance("合格");
                        } else {
                            //偏低
                            point.setCompliance("偏低");
                        }
                        break;
                    case "次干路":
                        if (lightAverage.compareTo(new BigDecimal(20)) >= 0) {
                            //合格
                            point.setCompliance("合格");
                        } else {
                            //偏低
                            point.setCompliance("偏低");
                        }
                        break;
                    case "支路":
                        if (lightAverage.compareTo(new BigDecimal(10)) >= 0) {
                            //合格
                            point.setCompliance("合格");
                        } else {
                            //偏低
                            point.setCompliance("偏低");
                        }
                        break;
                }
                lightAList.add(point.getLightAverage().doubleValue());
                lightUList.add(point.getLightuniformity().doubleValue());
                brightAList.add(point.getAveragebrightness().doubleValue());
                brightUList.add(point.getBrightnessuniformity().doubleValue());
                speedAList.add(point.getSpeed().doubleValue());
                if ("合格".equals(point.getCompliance())) {
                    qualificationOk.add(point.getCompliance());
                }
            }
            //得到平均数 照度
            double lightAverage = lightAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
            //最大值 照度
            double lightMax = lightAList.stream().mapToDouble(Double::valueOf).max().getAsDouble();
            //最小值 照度
            double lightMin = lightAList.stream().mapToDouble(Double::valueOf).min().getAsDouble();
            //得到平均数 照度均匀度
            double lightUAverage = lightUList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
            //得到平均数 照度
            double brightAverage = brightAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
            //最大值 照度
            double brightMax = brightAList.stream().mapToDouble(Double::valueOf).max().getAsDouble();
            //最小值 照度
            double brightMin = brightAList.stream().mapToDouble(Double::valueOf).min().getAsDouble();
            //得到平均数 照度均匀度
            double brightUAverage = brightUList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
            //得到平均数 照度均匀度
            double speedAverage = speedAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
            //合格率占比
            BigDecimal rate = new BigDecimal(qualificationOk.size()).divide(new BigDecimal(list.size()), 4, RoundingMode.DOWN).setScale(4, RoundingMode.DOWN);
            mapStreetResult.setTime(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, new Date(list.get(0).getTime() * 1000)));
            //todo 设置检测人员
            mapStreetResult.setPerson("王*楚");
            mapStreetResult.setCheckTime(1);
            mapStreetResult.setUuid(uuid);
            mapStreetResult.setLightAverage(new BigDecimal(lightAverage).setScale(2, RoundingMode.DOWN));
            mapStreetResult.setMaxLight(new BigDecimal(lightMax).setScale(2, RoundingMode.DOWN));
            mapStreetResult.setMinLight(new BigDecimal(lightMin).setScale(2, RoundingMode.DOWN));
            mapStreetResult.setLightUniformity(new BigDecimal(lightUAverage).setScale(2, RoundingMode.DOWN));
            mapStreetResult.setAverageBrightness(new BigDecimal(brightAverage).setScale(2, RoundingMode.DOWN));
            mapStreetResult.setMaxBright(new BigDecimal(brightMax).setScale(2, RoundingMode.DOWN));
            mapStreetResult.setMinBright(new BigDecimal(brightMin).setScale(2, RoundingMode.DOWN));
            mapStreetResult.setBrightnessUniformity(new BigDecimal(brightUAverage).setScale(2, RoundingMode.DOWN));
            mapStreetResult.setAverageSpeed(new BigDecimal(speedAverage).setScale(2, RoundingMode.DOWN));
            mapStreetResult.setRate(rate);
            mapStreetResult.setStartLon(list.get(0).getLon());
            mapStreetResult.setStartLat(list.get(0).getLat());
            mapStreetResult.setEndLon(list.get(list.size() - 1).getLon());
            mapStreetResult.setEndLat(list.get(list.size() - 1).getLat());
            mapStreetResult.setName(mapStreet.getStreetName());
            BigDecimal lightAverageResult = mapStreetResult.getLightAverage();
            switch (streetType) {
                case "主干路":
                case "快速路":
                    if (lightAverageResult.compareTo(new BigDecimal(30)) >= 0) {
                        //合格
                        mapStreetResult.setLightResult("合格");
                    } else {
                        //偏低
                        mapStreetResult.setLightResult("偏低");
                    }
                    break;
                case "次干路":
                    if (lightAverageResult.compareTo(new BigDecimal(20)) >= 0) {
                        //合格
                        mapStreetResult.setLightResult("合格");
                    } else {
                        //偏低
                        mapStreetResult.setLightResult("偏低");
                    }
                    break;
                case "支路":
                    if (lightAverageResult.compareTo(new BigDecimal(10)) >= 0) {
                        //合格
                        mapStreetResult.setLightResult("合格");
                    } else {
                        //偏低
                        mapStreetResult.setLightResult("偏低");
                    }
                    break;
            }
            //todo 验证平均间隔画点
            //计算出平均间隔点,平均间隔8m
            List<LineInterpolatorUtil.LatLng> latLngsResult = LineInterpolatorUtil.interpolatePoints(latLngs, 10);
            latLngsResult.stream().forEach(item -> {
                MapPoint nearestPoint = LineInterpolatorUtil.findNearestPoint(item, list);
                nearestPoint.setLon(new BigDecimal(item.getLongitude()));
                nearestPoint.setLat(new BigDecimal(item.getLatitude()));
                markPoints.add(nearestPoint);
            });

        }
        Map<String, Object> data = new HashMap<>();
        data.put("mapStreet", mapStreet);
        data.put("mapStreetResult", mapStreetResult);
        data.put("points", list);
        data.put("markPoints", markPoints);
        return AjaxResult.success(data);
    }
}

