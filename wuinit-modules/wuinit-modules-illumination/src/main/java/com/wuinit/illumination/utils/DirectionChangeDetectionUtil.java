package com.wuinit.illumination.utils;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

public class DirectionChangeDetectionUtil {
    @Data
    public static class LatLng {
        double latitude;
        double longitude;

        public LatLng(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    public static void detectDirectionChanges(List<LatLng> points, double threshold) {
        for (int i = 2; i < points.size(); i++) {
            double angle1 = calculateBearing(points.get(i - 2), points.get(i - 1));
            double angle2 = calculateBearing(points.get(i - 1), points.get(i));

            double angleChange = calculateAngleChange(angle1, angle2);

            if (angleChange > threshold) {
                System.out.println("Direction change detected at point " + (i - 1));
            }
        }
    }

    private static double calculateBearing(LatLng coord1, LatLng coord2) {
        double lat1 = Math.toRadians(coord1.latitude);
        double lon1 = Math.toRadians(coord1.longitude);
        double lat2 = Math.toRadians(coord2.latitude);
        double lon2 = Math.toRadians(coord2.longitude);

        double dLon = lon2 - lon1;

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);

        return Math.atan2(y, x);
    }

    private static double calculateAngleChange(double angle1, double angle2) {
        double angleChange = Math.abs(angle1 - angle2);

        if (angleChange > Math.PI) {
            angleChange = 2 * Math.PI - angleChange;
        }

        return angleChange;
    }

    public static void main(String[] args) {
        // 例子
        List<LatLng> curvePoints = new ArrayList<>();
        curvePoints.add(new LatLng(40.7128, -74.0060));
        curvePoints.add(new LatLng(34.0522, -118.2437));
        curvePoints.add(new LatLng(41.8781, -87.6298));
        // 添加更多点...

        double threshold = Math.toRadians(30); // 阈值，用弧度表示

        detectDirectionChanges(curvePoints, threshold);
    }
}
