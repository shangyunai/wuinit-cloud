package com.wuinit.illumination.controller;

import com.wuinit.common.core.web.controller.BaseController;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.illumination.datacleaning.DataCleaningUtil;
import com.wuinit.illumination.service.ShowResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.health.HealthProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 清洗后数据展示界面Controller
 *
 * @author wuinit
 * @date 2023-09-30
 */
@RestController
@RequestMapping("/show/result")
public class ShowResultController extends BaseController {
    @Autowired
    private ShowResultService showResultService;

    /**
     * 获取道路列表树
     *
     * @return
     */
    @GetMapping("/get/street/tree")
    public AjaxResult getStreetTree(String status) {
        return showResultService.getStreetTree(status);
    }

    /**
     * 获取各区域道路合格率占比
     *
     * @return
     */
    @GetMapping("/get/region/street/rate")
    public AjaxResult getRegionStreetRate() {
        return showResultService.getRegionStreetRate();
    }
    /**
     * 获取各区域道路合格率占比
     *
     * @return
     */
    @GetMapping("/get/street/light/rate/by/type")
    public AjaxResult getStreetLightRateByType(String streetType) {
        return showResultService.getStreetLightRateByType(streetType);
    }
}

