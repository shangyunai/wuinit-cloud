package com.wuinit.illumination.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wuinit.common.core.web.controller.BaseController;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.common.core.web.page.PageDomain;
import com.wuinit.common.core.web.page.TableDataInfo;
import com.wuinit.common.core.web.page.TableSupport;
import com.wuinit.illumination.domain.entity.MapPoint;
import com.wuinit.illumination.domain.form.PointBlindingImageForm;
import com.wuinit.illumination.kafka.KafkaMessage;
import com.wuinit.illumination.service.MapPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 地图点Controller
 *
 * @author wuinit
 * @date 2023-09-30
 */
@RestController
@RequestMapping("/point")
public class MapPointController extends BaseController {
    @Autowired
    private MapPointService mapPointService;

    /**
     * 查询地图所有点集合
     */
    @GetMapping("/all/check")
    public AjaxResult allCheck(MapPoint mapPoint) {
        List<List<BigDecimal>> list = mapPointService.allCheck(mapPoint);
        return AjaxResult.success(list);
    }

    /**
     * 分页查询点集合
     *
     * @param mapPoint
     * @return
     */
    @GetMapping("/list")
    public TableDataInfo list(MapPoint mapPoint) {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        QueryWrapper<MapPoint> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("time");
        Page<MapPoint> page = new Page<>(pageNum, pageSize);
        page = mapPointService.page(page, queryWrapper);
        List<MapPoint> list = page.getRecords();
        return getDataTable(list);
    }

    /**
     * 根据uuid查询点集合
     */
    @PostMapping("/by/uuid")
    public AjaxResult getByPointUuid(@RequestBody MapPoint mapPoint) {
        List<List<MapPoint>> list = mapPointService.getPointByUuid(mapPoint);
        return AjaxResult.success(list);
    }

    /**
     * 点绑定图片
     */
    @PostMapping("/blinding/image")
    public AjaxResult blindingImage(@RequestBody PointBlindingImageForm pointBlindingImageForm) {
        return mapPointService.blindingImage(pointBlindingImageForm);
    }

    /**
     * 补传点
     */
    @PostMapping("/supplement")
    public AjaxResult supplement(@RequestBody KafkaMessage kafkaMessage) {
        return mapPointService.supplement(kafkaMessage);
    }

    /**
     * 获取点绑定图片连接
     */
    @PostMapping("/get/image/url")
    public AjaxResult getImageUrlByTime(@RequestBody MapPoint mapPoint) {
        return mapPointService.getImageUrlByTime(mapPoint);
    }

    /**
     * 获取点绑定图片连接列表
     */
    @PostMapping("/get/image/url/list")
    public AjaxResult getImagesUrlByUuid(@RequestBody MapPoint mapPoint) {
        return mapPointService.getImagesUrlByUuid(mapPoint);
    }
}

