package com.wuinit.illumination.datacleaning;

import com.alibaba.nacos.shaded.com.google.gson.Gson;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuinit.common.core.utils.DateUtils;
import com.wuinit.common.core.utils.StringUtils;
import com.wuinit.illumination.api.MapApi;
import com.wuinit.illumination.config.GeoserverConfig;
import com.wuinit.illumination.domain.entity.MapPoint;
import com.wuinit.illumination.domain.entity.MapStreet;
import com.wuinit.illumination.domain.entity.MapStreetExt;
import com.wuinit.illumination.domain.entity.MapStreetResult;
import com.wuinit.illumination.domain.result.MapFeatureResult;
import com.wuinit.illumination.kafka.KafkaProducer;
import com.wuinit.illumination.service.MapPointService;
import com.wuinit.illumination.service.MapStreetExtService;
import com.wuinit.illumination.service.MapStreetResultService;
import com.wuinit.illumination.service.MapStreetService;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.IntSummaryStatistics;
import java.util.List;

/**
 * 数据清洗工具
 */
@Slf4j
@Component
public class DataCleaningUtil {
    @Autowired
    private MapPointService mapPointService;
    @Autowired
    private MapStreetService mapStreetService;
    @Autowired
    private MapStreetResultService mapStreetResultService;
    @Autowired
    private GeoserverConfig geoserverConfig;
    @Autowired
    private RestTemplate restTemplate;
    @Resource(name = "threadPoolProcessorTaskExecutor")
    private ThreadPoolTaskExecutor executor;

    /**
     * 补全交叉点
     */
    public void completeCrossLinePoints() {
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        mapPointQueryWrapper.ne("speed", 0);
        //查询所有已有道路属性的点
        List<MapPoint> list = mapPointService.list(mapPointQueryWrapper);
        log.info("查询到已有道路属性的点位数量：[{}]", list.size());
        list.stream().forEach(pointItem -> {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    List<String> uuids = new ArrayList<>();
                    String point = "POINT(" + pointItem.getLon() + " " + pointItem.getLat() + ")";
                    //相交
                    String streetByPoint = MapApi.getInstance().getStreetByPoint(point, geoserverConfig, restTemplate);
                    if (streetByPoint != null) {
                        MapFeatureResult mapFeatureResult = new Gson().fromJson(streetByPoint, MapFeatureResult.class);
                        if (mapFeatureResult != null) {
                            List<MapFeatureResult.FeaturesDTO> features = mapFeatureResult.getFeatures();
                            if (!Collections.isEmpty(features)) {
                                features.stream().forEach(feature -> {
                                    MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                    if (properties != null) {
                                        String uuid = properties.getUuid();
                                        if (StringUtils.isNotEmpty(uuid)) {
                                            uuids.add(uuid);
                                        }
                                    }
                                });
                            } else {
                                //包含
                                String streetByPointContains = MapApi.getInstance().getStreetByPointContains(point, geoserverConfig, restTemplate);
                                if (streetByPointContains != null) {
                                    MapFeatureResult mapFeatureResultContains = new Gson().fromJson(streetByPointContains, MapFeatureResult.class);
                                    if (mapFeatureResultContains != null) {
                                        List<MapFeatureResult.FeaturesDTO> featuresContains = mapFeatureResultContains.getFeatures();
                                        if (!Collections.isEmpty(featuresContains)) {
                                            featuresContains.stream().forEach(feature -> {
                                                MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                                if (properties != null) {
                                                    String uuid = properties.getUuid();
                                                    if (StringUtils.isNotEmpty(uuid)) {
                                                        uuids.add(uuid);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //只保存有道路属性的点位
                    if (!Collections.isEmpty(uuids)) {
                        uuids.stream().forEach(uuid -> {
                            Integer isDataOk = pointItem.getIsDataOk();
                            if (isDataOk == null) {
                                pointItem.setIsDataOk(1);
                            }
                            pointItem.setStreetUuid(uuid);
                            mapPointService.saveSlave(pointItem);
                            log.info("==========save point slave ok ========");
                        });
                    }
                }
            });

        });
        log.info("========finish completeCrossLinePoints=======");
    }

    /**
     * 重新计算点位所属道路
     */
    public void recalculatePoints() {
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        mapPointQueryWrapper.ne("speed", 0);
        mapPointQueryWrapper.isNull("street_uuid");
        mapPointQueryWrapper.eq("isDataOk", 1);
        //查询所有有效点位，但是未归属道路的点位，重新归属道路
        List<MapPoint> list = mapPointService.list(mapPointQueryWrapper);
        log.info("查询有效点位数量：[{}]", list.size());
        list.stream().forEach(pointItem -> {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    String point = "POINT(" + pointItem.getLon() + " " + pointItem.getLat() + ")";
                    //相交
                    String streetByPoint = MapApi.getInstance().getStreetByPoint(point, geoserverConfig, restTemplate);
                    if (streetByPoint != null) {
                        MapFeatureResult mapFeatureResult = new Gson().fromJson(streetByPoint, MapFeatureResult.class);
                        if (mapFeatureResult != null) {
                            List<MapFeatureResult.FeaturesDTO> features = mapFeatureResult.getFeatures();
                            if (!Collections.isEmpty(features)) {
                                features.stream().forEach(feature -> {
                                    MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                    if (properties != null) {
                                        String uuid = properties.getUuid();
                                        //设置点位所属道路uuid，更新点位信息
                                        pointItem.setStreetUuid(uuid);
                                        mapPointService.updateById(pointItem);
                                    }
                                });
                            } else {
                                //包含
                                String streetByPointContains = MapApi.getInstance().getStreetByPointContains(point, geoserverConfig, restTemplate);
                                if (streetByPointContains != null) {
                                    MapFeatureResult mapFeatureResultContains = new Gson().fromJson(streetByPointContains, MapFeatureResult.class);
                                    if (mapFeatureResultContains != null) {
                                        List<MapFeatureResult.FeaturesDTO> featuresContains = mapFeatureResultContains.getFeatures();
                                        if (!Collections.isEmpty(featuresContains)) {
                                            featuresContains.stream().forEach(feature -> {
                                                MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                                if (properties != null) {
                                                    String uuid = properties.getUuid();
                                                    if (StringUtils.isNotEmpty(uuid)) {
                                                        //设置点位所属道路uuid，更新点位信息
                                                        pointItem.setStreetUuid(uuid);
                                                        mapPointService.updateById(pointItem);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });

        });
        log.info("========finish recalculatePoints=======");
    }

    /**
     * 补全道路缺失的道路框外点
     */
    public void fixStreetLinePoint() {
        List<MapStreetResult> mapStreetResults = mapStreetResultService.list();
        for (MapStreetResult street : mapStreetResults) {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    String name = street.getName();
                    String uuid = street.getUuid();
                    log.info("========{}:{}========", name, uuid);
                    //获取道路所有点
                    QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
                    mapPointQueryWrapper.eq("street_uuid", uuid);
                    mapPointQueryWrapper.orderByAsc("time");
                    List<MapPoint> mapPoints = mapPointService.list(mapPointQueryWrapper);
                    //获取整条道路的检测时间范围
                    if (!Collections.isEmpty(mapPoints)) {
                        long startTime = mapPoints.get(0).getTime();
                        long endTime = mapPoints.get(mapPoints.size() - 1).getTime();
                        QueryWrapper<MapPoint> mapPointQueryWrapperFix = new QueryWrapper<>();
                        mapPointQueryWrapperFix.ge("time", startTime);
                        mapPointQueryWrapperFix.le("time", endTime);
                        //mapPointQueryWrapperFix.isNull("street_uuid");
                        mapPointQueryWrapperFix.eq("isDataOk", 1);
                        List<MapPoint> needFixPoints = mapPointService.list(mapPointQueryWrapperFix);
                        if (!Collections.isEmpty(needFixPoints)) {
                            needFixPoints.stream().forEach(point -> {
                                point.setStreetUuid(uuid);
                            });
                            mapPointService.updateBatchById(needFixPoints);
                        }
                    }
                }
            });
        }
        log.info("========finish fixStreetLinePoint=======");
    }

    /**
     * 判断点合格性
     */
    public void checkPointQualification() {
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        mapPointQueryWrapper.isNull("Compliance");
        mapPointQueryWrapper.isNotNull("street_uuid");
        mapPointQueryWrapper.eq("isDataOk", 1);
        List<MapPoint> mapPoints = mapPointService.list(mapPointQueryWrapper);
        mapPoints.stream().forEach(point -> {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    String streetUuid = point.getStreetUuid();
                    if (StringUtils.isNotEmpty(streetUuid)) {
                        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
                        mapStreetQueryWrapper.eq("uuid", streetUuid);
                        MapStreet mapStreet = mapStreetService.getOne(mapStreetQueryWrapper, false);
                        if (mapStreet != null) {
                            String streetType = mapStreet.getStreetType();
                            BigDecimal lightAverage = point.getLightAverage();
                            switch (streetType) {
                                case "主干路":
                                case "快速路":
                                    if (lightAverage.compareTo(new BigDecimal(30)) >= 0) {
                                        //合格
                                        point.setCompliance("合格");
                                    } else {
                                        //偏低
                                        point.setCompliance("偏低");
                                    }
                                    break;
                                case "次干路":
                                    if (lightAverage.compareTo(new BigDecimal(20)) >= 0) {
                                        //合格
                                        point.setCompliance("合格");
                                    } else {
                                        //偏低
                                        point.setCompliance("偏低");
                                    }
                                    break;
                                case "支路":
                                    if (lightAverage.compareTo(new BigDecimal(10)) >= 0) {
                                        //合格
                                        point.setCompliance("合格");
                                    } else {
                                        //偏低
                                        point.setCompliance("偏低");
                                    }
                                    break;
                            }
                            mapPointService.updateById(point);
                            log.info("=======update point qualification:{}======", point.getCompliance());
                        }
                    }
                }
            });

        });
        log.info("========finish checkPointQualification=======");
    }

    private static final int LINE_LIMIT_POINT = 5;
    private static final int LINE_LIMIT_POINT_ONE = 100;

    /**
     * 判断道路合格性
     */
    public void checkStreetQualification() {
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        mapStreetQueryWrapper.ne("region", "一期800条道路");
        List<MapStreet> streets = mapStreetService.list(mapStreetQueryWrapper);
        streets.stream().forEach(mapStreet -> {
            //查询道路点位
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        String uuid = mapStreet.getUuid();
                        String region = mapStreet.getRegion();
                        //判断之前是否已有该条路检测结果
                        QueryWrapper<MapStreetResult> mapStreetResultQueryWrapper = new QueryWrapper<>();
                        mapStreetResultQueryWrapper.eq("uuid", uuid);
                        mapStreetResultQueryWrapper.eq("check_time", 1);
                        MapStreetResult one = mapStreetResultService.getOne(mapStreetResultQueryWrapper, false);
                        if (one == null) {
                            QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
                            mapPointQueryWrapper.eq("street_uuid", uuid);
                            mapPointQueryWrapper.eq("isDataOk", 1);
                            mapPointQueryWrapper.orderByDesc("time");
                            List<MapPoint> mapPoints = mapPointService.list(mapPointQueryWrapper);
                            if ("一期800条道路".equals(region) && !Collections.isEmpty(mapPoints) && mapPoints.size() >= LINE_LIMIT_POINT_ONE) {
                                //平均照度
                                List<Double> lightAList = new ArrayList<>();
                                //平均照度均匀度
                                List<Double> lightUList = new ArrayList<>();
                                //平均亮度
                                List<Double> brightAList = new ArrayList<>();
                                //平均亮度均匀度
                                List<Double> brightUList = new ArrayList<>();
                                //平均车速
                                List<Double> speedAList = new ArrayList<>();
                                //照度达标数量
                                List<String> qualificationOk = new ArrayList<>();
                                mapPoints.stream().forEach(mapPoint -> {
                                    lightAList.add(mapPoint.getLightAverage().doubleValue());
                                    lightUList.add(mapPoint.getLightuniformity().doubleValue());
                                    brightAList.add(mapPoint.getAveragebrightness().doubleValue());
                                    brightUList.add(mapPoint.getBrightnessuniformity().doubleValue());
                                    speedAList.add(mapPoint.getSpeed().doubleValue());
                                    if ("合格".equals(mapPoint.getCompliance())) {
                                        qualificationOk.add(mapPoint.getCompliance());
                                    }
                                });
                                //得到平均数 照度
                                double lightAverage = lightAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //最大值 照度
                                double lightMax = lightAList.stream().mapToDouble(Double::valueOf).max().getAsDouble();
                                //最小值 照度
                                double lightMin = lightAList.stream().mapToDouble(Double::valueOf).min().getAsDouble();
                                //得到平均数 照度均匀度
                                double lightUAverage = lightUList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //得到平均数 照度
                                double brightAverage = brightAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //最大值 照度
                                double brightMax = brightAList.stream().mapToDouble(Double::valueOf).max().getAsDouble();
                                //最小值 照度
                                double brightMin = brightAList.stream().mapToDouble(Double::valueOf).min().getAsDouble();
                                //得到平均数 照度均匀度
                                double brightUAverage = brightUList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //得到平均数 照度均匀度
                                double speedAverage = speedAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //合格率占比
                                BigDecimal rate = new BigDecimal(qualificationOk.size()).divide(new BigDecimal(mapPoints.size()), 4, RoundingMode.DOWN).setScale(4, RoundingMode.DOWN);
                                MapStreetResult mapStreetResult = new MapStreetResult();
                                mapStreetResult.setTime(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, new Date(mapPoints.get(mapPoints.size() - 1).getTime() * 1000)));
                                //todo 设置检测人员
                                mapStreetResult.setPerson("王*楚");
                                mapStreetResult.setCheckTime(1);
                                mapStreetResult.setUuid(uuid);
                                mapStreetResult.setLightAverage(new BigDecimal(lightAverage));
                                mapStreetResult.setMaxLight(new BigDecimal(lightMax));
                                mapStreetResult.setMinLight(new BigDecimal(lightMin));
                                mapStreetResult.setLightUniformity(new BigDecimal(lightUAverage));
                                mapStreetResult.setAverageBrightness(new BigDecimal(brightAverage));
                                mapStreetResult.setMaxBright(new BigDecimal(brightMax));
                                mapStreetResult.setMinBright(new BigDecimal(brightMin));
                                mapStreetResult.setBrightnessUniformity(new BigDecimal(brightUAverage));
                                mapStreetResult.setAverageSpeed(new BigDecimal(speedAverage));
                                mapStreetResult.setRate(rate);
                                mapStreetResult.setStartLon(mapPoints.get(0).getLon());
                                mapStreetResult.setStartLat(mapPoints.get(0).getLat());
                                mapStreetResult.setEndLon(mapPoints.get(mapPoints.size() - 1).getLon());
                                mapStreetResult.setEndLat(mapPoints.get(mapPoints.size() - 1).getLat());
                                mapStreetResult.setName(mapStreet.getRegion());
                                String streetType = mapStreet.getStreetType();
                                BigDecimal lightAverageResult = mapStreetResult.getLightAverage();
                                switch (streetType) {
                                    case "主干路":
                                    case "快速路":
                                        if (lightAverageResult.compareTo(new BigDecimal(30)) >= 0) {
                                            //合格
                                            mapStreetResult.setLightResult("合格");
                                        } else {
                                            //偏低
                                            mapStreetResult.setLightResult("偏低");
                                        }
                                        break;
                                    case "次干路":
                                        if (lightAverageResult.compareTo(new BigDecimal(20)) >= 0) {
                                            //合格
                                            mapStreetResult.setLightResult("合格");
                                        } else {
                                            //偏低
                                            mapStreetResult.setLightResult("偏低");
                                        }
                                        break;
                                    case "支路":
                                        if (lightAverageResult.compareTo(new BigDecimal(10)) >= 0) {
                                            //合格
                                            mapStreetResult.setLightResult("合格");
                                        } else {
                                            //偏低
                                            mapStreetResult.setLightResult("偏低");
                                        }
                                        break;
                                }
                                QueryWrapper<MapStreet> mapStreetQueryWrapper1 = new QueryWrapper<>();
                                mapStreetQueryWrapper1.ne("region", "一期800条道路");
                                mapStreetQueryWrapper1.eq("street_name", mapStreet.getStreetName());
                                MapStreet one1 = mapStreetService.getOne(mapStreetQueryWrapper1, false);
                                if (one1 == null) {
                                    mapStreetResultService.save(mapStreetResult);
                                    log.info("============== 一期800条道路 新增一条道路检测结果==============");
                                }

                            } else if (!"一期800条道路".equals(region)) {
                                if (!Collections.isEmpty(mapPoints) && mapPoints.size() >= LINE_LIMIT_POINT) {
                                    //平均照度
                                    List<Double> lightAList = new ArrayList<>();
                                    //平均照度均匀度
                                    List<Double> lightUList = new ArrayList<>();
                                    //平均亮度
                                    List<Double> brightAList = new ArrayList<>();
                                    //平均亮度均匀度
                                    List<Double> brightUList = new ArrayList<>();
                                    //平均车速
                                    List<Double> speedAList = new ArrayList<>();
                                    //照度达标数量
                                    List<String> qualificationOk = new ArrayList<>();
                                    mapPoints.stream().forEach(mapPoint -> {
                                        lightAList.add(mapPoint.getLightAverage().doubleValue());
                                        lightUList.add(mapPoint.getLightuniformity().doubleValue());
                                        brightAList.add(mapPoint.getAveragebrightness().doubleValue());
                                        brightUList.add(mapPoint.getBrightnessuniformity().doubleValue());
                                        speedAList.add(mapPoint.getSpeed().doubleValue());
                                        if ("合格".equals(mapPoint.getCompliance())) {
                                            qualificationOk.add(mapPoint.getCompliance());
                                        }
                                    });
                                    //得到平均数 照度
                                    double lightAverage = lightAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                    //最大值 照度
                                    double lightMax = lightAList.stream().mapToDouble(Double::valueOf).max().getAsDouble();
                                    //最小值 照度
                                    double lightMin = lightAList.stream().mapToDouble(Double::valueOf).min().getAsDouble();
                                    //得到平均数 照度均匀度
                                    double lightUAverage = lightUList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                    //得到平均数 照度
                                    double brightAverage = brightAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                    //最大值 照度
                                    double brightMax = brightAList.stream().mapToDouble(Double::valueOf).max().getAsDouble();
                                    //最小值 照度
                                    double brightMin = brightAList.stream().mapToDouble(Double::valueOf).min().getAsDouble();
                                    //得到平均数 照度均匀度
                                    double brightUAverage = brightUList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                    //得到平均数 照度均匀度
                                    double speedAverage = speedAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                    //合格率占比
                                    BigDecimal rate = new BigDecimal(qualificationOk.size()).divide(new BigDecimal(mapPoints.size()), 4, RoundingMode.DOWN).setScale(4, RoundingMode.DOWN);
                                    MapStreetResult mapStreetResult = new MapStreetResult();
                                    mapStreetResult.setTime(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, new Date(mapPoints.get(mapPoints.size() - 1).getTime() * 1000)));
                                    //todo 设置检测人员
                                    mapStreetResult.setPerson("王*楚");
                                    mapStreetResult.setCheckTime(1);
                                    mapStreetResult.setUuid(uuid);
                                    mapStreetResult.setLightAverage(new BigDecimal(lightAverage));
                                    mapStreetResult.setMaxLight(new BigDecimal(lightMax));
                                    mapStreetResult.setMinLight(new BigDecimal(lightMin));
                                    mapStreetResult.setLightUniformity(new BigDecimal(lightUAverage));
                                    mapStreetResult.setAverageBrightness(new BigDecimal(brightAverage));
                                    mapStreetResult.setMaxBright(new BigDecimal(brightMax));
                                    mapStreetResult.setMinBright(new BigDecimal(brightMin));
                                    mapStreetResult.setBrightnessUniformity(new BigDecimal(brightUAverage));
                                    mapStreetResult.setAverageSpeed(new BigDecimal(speedAverage));
                                    mapStreetResult.setRate(rate);
                                    mapStreetResult.setStartLon(mapPoints.get(0).getLon());
                                    mapStreetResult.setStartLat(mapPoints.get(0).getLat());
                                    mapStreetResult.setEndLon(mapPoints.get(mapPoints.size() - 1).getLon());
                                    mapStreetResult.setEndLat(mapPoints.get(mapPoints.size() - 1).getLat());
                                    mapStreetResult.setName(mapStreet.getStreetName());
                                    String streetType = mapStreet.getStreetType();
                                    BigDecimal lightAverageResult = mapStreetResult.getLightAverage();
                                    switch (streetType) {
                                        case "主干路":
                                        case "快速路":
                                            if (lightAverageResult.compareTo(new BigDecimal(30)) >= 0) {
                                                //合格
                                                mapStreetResult.setLightResult("合格");
                                            } else {
                                                //偏低
                                                mapStreetResult.setLightResult("偏低");
                                            }
                                            break;
                                        case "次干路":
                                            if (lightAverageResult.compareTo(new BigDecimal(20)) >= 0) {
                                                //合格
                                                mapStreetResult.setLightResult("合格");
                                            } else {
                                                //偏低
                                                mapStreetResult.setLightResult("偏低");
                                            }
                                            break;
                                        case "支路":
                                            if (lightAverageResult.compareTo(new BigDecimal(10)) >= 0) {
                                                //合格
                                                mapStreetResult.setLightResult("合格");
                                            } else {
                                                //偏低
                                                mapStreetResult.setLightResult("偏低");
                                            }
                                            break;
                                    }
                                    mapStreetResultService.save(mapStreetResult);
                                    log.info("==============新增一条道路检测结果==============");

                                }
                            }
                        } else {

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.info("======error===={}", e.toString());
                    }

                }
            });
        });
        log.info("========finish checkStreetQualification=======");
    }

    @Autowired
    private MapStreetExtService mapStreetExtService;

    /**
     * 道路额外属性初始化
     */
    public void initStreetExt() {
        List<MapStreet> list = mapStreetService.list();
        list.stream().forEach(mapStreet -> {
            QueryWrapper<MapStreetExt> mapStreetExtQueryWrapper = new QueryWrapper<>();
            mapStreetExtQueryWrapper.eq("uuid", mapStreet.getUuid());
            String streetType = mapStreet.getStreetType();
            MapStreetExt one = mapStreetExtService.getOne(mapStreetExtQueryWrapper, false);
            if (one == null) {
                MapStreetExt mapStreetExt = new MapStreetExt();
                mapStreetExt.setUuid(mapStreet.getUuid());
                mapStreetExt.setArrangement("两边对称");
                mapStreetExt.setInstallationHeight("12m");
                mapStreetExt.setMaterials("沥青路面");
                mapStreetExt.setLightType("LED");
                mapStreetExt.setLightPower("150W");
                mapStreetExt.setCorrectionCoefficient("0.93");
                mapStreetExt.setPoleType("铸铁镀锌");
                mapStreetExt.setRoadCoefficient("0.066");
                switch (streetType) {
                    case "主干路":
                    case "快速路":
                        mapStreetExt.setLanesNum("8");
                        mapStreetExt.setPoleSpacing("40m");
                        break;
                    case "次干路":
                        mapStreetExt.setLanesNum("4");
                        mapStreetExt.setPoleSpacing("30m");
                        break;
                    case "支路":
                        mapStreetExt.setLanesNum("2");
                        mapStreetExt.setPoleSpacing("30m");
                        break;
                }
                mapStreetExtService.save(mapStreetExt);
                log.info("======新增一条道路额外属性====");
            }

        });

    }

    /**
     * 测试kafka 上传点位信息
     */
    @Autowired
    private KafkaProducer kafkaProducer;

    public void kafkaUploadPoint() {
        String message = "{\n" + "  \"Region\": null,\n" + "  \"Roadname\": null,\n" + "  \"Roadlevel\": null,\n" + "  \"lightdistribution\": null,\n" + "  \"lighttype\": null,\n" + "  \"lightAverage\": \"0.11\",\n" + "  \"lightuniformity\": \"0.64\",\n" + "  \"Averagebrightness\": \"0.0\",\n" + "  \"Brightnessuniformity\": \"nan\",\n" + "  \"max\": \"0.22\",\n" + "  \"min\": \"0.07\",\n" + "  \"Longitudinal\": \"0.32\",\n" + "  \"lat\": \"30.722050\",\n" + "  \"lon\": \"103.986464\",\n" + "  \"time\": \"1701014376\",\n" + "  \"speed\": \"39.86\",\n" + "  \"Compliance\": null,\n" + "  \"result\": null,\n" + "  \"dataCuuid\": null,\n" + "  \"isDataOk\": true\n" + "}";
        kafkaProducer.sendMessage(message);

    }

    /**
     * 过滤掉有备注不能检测的道路
     */
    public void changeStreetStatus() {
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        mapStreetQueryWrapper.ne("street_decs", "");
        List<MapStreet> streetList = mapStreetService.list(mapStreetQueryWrapper);


    }

}
