package com.wuinit.illumination.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuinit.illumination.domain.entity.MapPointImage;
import com.wuinit.illumination.mapper.MapPointImageMapper;
import com.wuinit.illumination.service.MapPointImageService;
import org.springframework.stereotype.Service;

@Service
public class MapPointImageServiceImpl extends ServiceImpl<MapPointImageMapper, MapPointImage> implements MapPointImageService {



}
