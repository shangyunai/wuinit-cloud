package com.wuinit.illumination.service.impl;

import com.alibaba.nacos.shaded.com.google.gson.Gson;
import com.baomidou.dynamic.datasource.annotation.Slave;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuinit.common.core.constant.Constants;
import com.wuinit.common.core.domain.R;
import com.wuinit.common.core.utils.StringUtils;
import com.wuinit.common.core.utils.file.MimeTypeUtils;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.illumination.api.MapApi;
import com.wuinit.illumination.config.GeoserverConfig;
import com.wuinit.illumination.domain.entity.MapPoint;
import com.wuinit.illumination.domain.entity.MapPointImage;
import com.wuinit.illumination.domain.form.PointBlindingImageForm;
import com.wuinit.illumination.domain.result.MapFeatureResult;
import com.wuinit.illumination.kafka.KafkaMessage;
import com.wuinit.illumination.mapper.MapPointMapper;
import com.wuinit.illumination.service.MapPointImageService;
import com.wuinit.illumination.service.MapPointService;
import com.wuinit.illumination.utils.DistanceCalculatorUtil;
import com.wuinit.illumination.utils.FileUtil;
import com.wuinit.illumination.utils.LineInterpolatorUtil;
import com.wuinit.system.api.RemoteFileService;
import com.wuinit.system.api.domain.SysFile;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Slf4j
@Service
public class MapPointServiceImpl extends ServiceImpl<MapPointMapper, MapPoint> implements MapPointService {
    @Autowired
    private MapPointImageService mapPointImageService;
    @Resource
    private RemoteFileService remoteFileService;
    @Autowired
    private GeoserverConfig geoserverConfig;
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<List<BigDecimal>> allCheck(MapPoint mapPoint) {
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        if (mapPoint.getTime() != null) {
            mapPointQueryWrapper.ge("time", mapPoint.getTime());
        }
        mapPointQueryWrapper.ge("lon", mapPoint.getLon());
        mapPointQueryWrapper.orderByAsc("time");
        mapPointQueryWrapper.ne("time", 0);
        List<MapPoint> list = list(mapPointQueryWrapper);
        List<List<BigDecimal>> linePointDatas = new ArrayList<>();
        if (!Collections.isEmpty(list)) {
            list.stream().forEach(item -> {
                ArrayList<BigDecimal> point = new ArrayList<>();
                point.add(item.getLon());
                point.add(item.getLat());
                linePointDatas.add(point);
            });
        }
        return linePointDatas;
    }

    /**
     * 道路采集点时间最长间隔 70s
     */
    private static long TIME_INTERVAL_100 = 100;
    /**
     * 每条道路最少点位不得少于5个点
     */
    private static int LINE_LIMIT_POINT = 5;
    /**
     * 每条道路两点之间的最短距离
     */
    private static int LINE_LIMIT_DISTANCE = 40;

    @Override
    public List<List<MapPoint>> getPointByUuid(MapPoint mapPoint) {
        List<List<MapPoint>> pointLinesVo = new ArrayList<>();
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        mapPointQueryWrapper.eq("street_uuid", mapPoint.getStreetUuid());
        mapPointQueryWrapper.eq("isDataOk", 1);
        mapPointQueryWrapper.ne("speed", 0);
        mapPointQueryWrapper.orderByAsc("time");
        List<MapPoint> list = list(mapPointQueryWrapper);
        if (!Collections.isEmpty(list)) {
            List<List<MapPoint>> pointLines = new ArrayList<>();
            List<MapPoint> mapPoints = new ArrayList<>();
            pointLines.add(mapPoints);
            MapPoint beforePoint = null;
            for (MapPoint point : list) {
                if (beforePoint != null) {
                    long interval = point.getTime() - beforePoint.getTime();
                    BigDecimal distance = DistanceCalculatorUtil.calculateDistance(point.getLat().doubleValue(), point.getLon().doubleValue(), beforePoint.getLat().doubleValue(), beforePoint.getLon().doubleValue());
                    //todo 取消两点之前的时间间隔限制
                    if (distance.intValue() > LINE_LIMIT_DISTANCE) {
                        //另外一次采集
                        List<MapPoint> nextLine = new ArrayList<>();
                        nextLine.add(point);
                        pointLines.add(nextLine);
                    } else {
                        pointLines.get(pointLines.size() - 1).add(point);
                    }
                } else {
                    pointLines.get(pointLines.size() - 1).add(point);
                }
                beforePoint = point;
            }
            pointLines.stream().forEach(pointLine -> {
                if (pointLine.size() >= LINE_LIMIT_POINT) {
                    pointLinesVo.add(pointLine);
                }
            });
            pointLinesVo.sort(new Comparator<List<MapPoint>>() {
                @Override
                public int compare(List<MapPoint> o1, List<MapPoint> o2) {
                    Integer size1 = o1.size();
                    Integer size2 = o2.size();
                    return size2.compareTo(size1);
                }
            });
        }
        return pointLinesVo;
    }

    /**
     * 检测点绑定图片
     *
     * @param pointBlindingImageForm
     * @return
     */
    @Override
    public AjaxResult blindingImage(PointBlindingImageForm pointBlindingImageForm) {
        //判断是否已经添加过
        QueryWrapper<MapPointImage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("time", pointBlindingImageForm.getTime());
        MapPointImage one = mapPointImageService.getOne(queryWrapper, false);
        if (one == null) {
            MultipartFile multipartFile = FileUtil.byteArrayToMultipartFile(Base64Utils.decodeFromString(pointBlindingImageForm.getImageBase64()), String.valueOf(System.currentTimeMillis()), MimeTypeUtils.IMAGE_JPG);
            R<SysFile> upload = remoteFileService.upload(multipartFile);
            if (upload != null) {
                int code = upload.getCode();
                if (code == Constants.SUCCESS) {
                    SysFile sysFile = upload.getData();
                    MapPointImage mapPointImage = new MapPointImage();
                    mapPointImage.setTime(pointBlindingImageForm.getTime());
                    mapPointImage.setUrl(sysFile.getUrl());
                    boolean save = mapPointImageService.save(mapPointImage);
                    if (save) {
                        return AjaxResult.success();
                    }
                }
            }
        } else {
            return AjaxResult.error("图片已经上传过了");
        }
        return AjaxResult.error("上传失败");
    }

    @Slave
    @Override
    public boolean saveSlave(MapPoint mapPoint) {
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        String streetUuid = mapPoint.getStreetUuid();
        if (StringUtils.isNotEmpty(streetUuid)) {
            mapPointQueryWrapper.eq("street_uuid", streetUuid);
        }
        Long time = mapPoint.getTime();
        mapPointQueryWrapper.eq("time", time);
        BigDecimal lon = mapPoint.getLon();
        mapPointQueryWrapper.eq("lon", lon);
        BigDecimal lat = mapPoint.getLat();
        mapPointQueryWrapper.eq("lat", lat);
        MapPoint one = getOne(mapPointQueryWrapper, false);
        if (one == null) {
            return save(mapPoint);
        }
        return false;
    }

    @Slave
    @Override
    public List<MapPoint> listSlave(QueryWrapper<MapPoint> queryWrapper) {
        return list(queryWrapper);
    }

    @Slave
    @Override
    public boolean updateByIdSlave(MapPoint mapPoint) {
        return updateById(mapPoint);
    }

    @Override
    public AjaxResult supplement(KafkaMessage kafkaMessage) {
        try {
            String lon = kafkaMessage.getLon();
            String lat = kafkaMessage.getLat();
            if (StringUtils.isNotEmpty(lon) && StringUtils.isNotEmpty(lat)) {
                //查询属于那条街道
                List<String> uuids = new ArrayList<>();
                String point = "POINT(" + lon.trim() + " " + lat.trim() + ")";
                //相交
                String streetByPoint = MapApi.getInstance().getStreetByPoint(point, geoserverConfig, restTemplate);
                if (streetByPoint != null) {
                    MapFeatureResult mapFeatureResult = new Gson().fromJson(streetByPoint, MapFeatureResult.class);
                    if (mapFeatureResult != null) {
                        List<MapFeatureResult.FeaturesDTO> features = mapFeatureResult.getFeatures();
                        if (!Collections.isEmpty(features)) {
                            features.stream().forEach(feature -> {
                                MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                if (properties != null) {
                                    String uuid = properties.getUuid();
                                    if (StringUtils.isNotEmpty(uuid)) {
                                        uuids.add(uuid);
                                    }
                                }
                            });
                        } else {
                            //包含
                            String streetByPointContains = MapApi.getInstance().getStreetByPointContains(point, geoserverConfig, restTemplate);
                            if (streetByPointContains != null) {
                                MapFeatureResult mapFeatureResultContains = new Gson().fromJson(streetByPointContains, MapFeatureResult.class);
                                if (mapFeatureResultContains != null) {
                                    List<MapFeatureResult.FeaturesDTO> featuresContains = mapFeatureResultContains.getFeatures();
                                    if (!Collections.isEmpty(featuresContains)) {
                                        featuresContains.stream().forEach(feature -> {
                                            MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                            if (properties != null) {
                                                String uuid = properties.getUuid();
                                                if (StringUtils.isNotEmpty(uuid)) {
                                                    uuids.add(uuid);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
                MapPoint mapPoint = new MapPoint();
                mapPoint.setLon(new BigDecimal(kafkaMessage.getLon().trim()));
                mapPoint.setLat(new BigDecimal(kafkaMessage.getLat().trim()));
                String time = kafkaMessage.getTime();
                if (StringUtils.isNotEmpty(time)) {
                    mapPoint.setTime(Long.valueOf(time.trim()));
                }
                String region = kafkaMessage.getRegion();
                if (StringUtils.isNotEmpty(region)) {
                    mapPoint.setRegion(region.trim());
                }
                String roadname = kafkaMessage.getRoadname();
                if (StringUtils.isNotEmpty(roadname)) {
                    mapPoint.setRoadname(roadname.trim());
                }
                String roadlevel = kafkaMessage.getRoadlevel();
                if (StringUtils.isNotEmpty(roadlevel)) {
                    mapPoint.setRoadlevel(roadlevel.trim());
                }
                String lightdistribution = kafkaMessage.getLightdistribution();
                if (StringUtils.isNotEmpty(lightdistribution)) {
                    mapPoint.setLightdistribution(lightdistribution.trim());
                }
                String lighttype = kafkaMessage.getLighttype();
                if (StringUtils.isNotEmpty(lighttype)) {
                    mapPoint.setLighttype(lighttype.trim());
                }
                boolean dataOk = kafkaMessage.isDataOk();
                if (dataOk) {
                    mapPoint.setIsDataOk(1);
                } else {
                    mapPoint.setIsDataOk(0);
                }
                String dataCuuid = kafkaMessage.getDataCuuid();
                if (StringUtils.isNotEmpty(dataCuuid)) {
                    mapPoint.setDataCuuid(dataCuuid.trim());
                }
                mapPoint.setLightAverage(new BigDecimal(kafkaMessage.getLightAverage().trim()));
                mapPoint.setLightuniformity(new BigDecimal(kafkaMessage.getLightuniformity().trim()));
                mapPoint.setAveragebrightness(new BigDecimal(kafkaMessage.getAveragebrightness().trim()));
                if ("nan".equals(kafkaMessage.getBrightnessuniformity().trim())) {
                    mapPoint.setBrightnessuniformity(new BigDecimal(0));
                } else {
                    mapPoint.setBrightnessuniformity(new BigDecimal(kafkaMessage.getBrightnessuniformity().trim()));
                }
                mapPoint.setMax(new BigDecimal(kafkaMessage.getMax().trim()));
                mapPoint.setMin(new BigDecimal(kafkaMessage.getMin().trim()));
                mapPoint.setLongitudinal(new BigDecimal(kafkaMessage.getLongitudinal().trim()));
                mapPoint.setSpeed(new BigDecimal(kafkaMessage.getSpeed().trim()));
                String compliance = kafkaMessage.getCompliance();
                if (StringUtils.isNotEmpty(compliance)) {
                    mapPoint.setCompliance(compliance.trim());
                }
                String result = kafkaMessage.getResult();
                if (StringUtils.isNotEmpty(result)) {
                    mapPoint.setResult(result.trim());
                }
                //判断重复点位不入库
                QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
                mapPointQueryWrapper.eq("lon", mapPoint.getLon());
                mapPointQueryWrapper.eq("lat", mapPoint.getLat());
                mapPointQueryWrapper.eq("time", mapPoint.getTime());
                MapPoint mapPointOne = getOne(mapPointQueryWrapper, false);
                if (mapPointOne == null) {
                    if (!Collections.isEmpty(uuids)) {
//                        uuids.stream().forEach(uuid -> {
//                            mapPoint.setStreetUuid(uuid);
//                            save(mapPoint);
//                            log.info("------ insert mappoint ok -------");
//                        });
                        mapPoint.setStreetUuid(uuids.get(0));
                        save(mapPoint);
                        log.info("------ insert mappoint ok -------");
                    } else {
                        save(mapPoint);
                        log.info("------ insert mappoint ok -------");
                    }
                }else {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    @Override
    public AjaxResult getImageUrlByTime(MapPoint mapPoint) {
        Long time = mapPoint.getTime();
        QueryWrapper<MapPointImage> mapPointImageQueryWrapper = new QueryWrapper<>();
        mapPointImageQueryWrapper.eq("time", time);
        MapPointImage one = mapPointImageService.getOne(mapPointImageQueryWrapper, false);
        if (one != null) {
            return AjaxResult.success(one);
        }
        return AjaxResult.success();
    }

    @Override
    public AjaxResult getImagesUrlByUuid(MapPoint mapPoint) {
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        mapPointQueryWrapper.eq("street_uuid", mapPoint.getStreetUuid());
        mapPointQueryWrapper.eq("isDataOk", 1);
        List<MapPoint> list = list(mapPointQueryWrapper);
        List<Long> times = new ArrayList<>();
        list.stream().forEach(point -> {
            times.add(point.getTime());
        });
        if (!Collections.isEmpty(times)) {
            QueryWrapper<MapPointImage> queryWrapper = new QueryWrapper<>();
            queryWrapper.in("time", times);
            List<MapPointImage> list1 = mapPointImageService.list(queryWrapper);
            return AjaxResult.success(list1);
        }
        return AjaxResult.success();
    }


}
