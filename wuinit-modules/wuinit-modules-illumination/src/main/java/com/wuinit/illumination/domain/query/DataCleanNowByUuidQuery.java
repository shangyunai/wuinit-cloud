package com.wuinit.illumination.domain.query;

import lombok.Data;

@Data
public class DataCleanNowByUuidQuery extends PageQuery {
    /**
     *
     */
    private String uuid;
    /**
     *
     */
    private Long time;
}
