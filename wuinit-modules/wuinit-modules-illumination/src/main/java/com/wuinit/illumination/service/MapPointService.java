package com.wuinit.illumination.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.illumination.domain.entity.MapPoint;
import com.wuinit.illumination.domain.form.PointBlindingImageForm;
import com.wuinit.illumination.kafka.KafkaMessage;

import java.math.BigDecimal;
import java.util.List;

public interface MapPointService extends IService<MapPoint> {
    List<List<BigDecimal>> allCheck(MapPoint mapPoint);

    List<List<MapPoint>> getPointByUuid(MapPoint mapPoint);

    AjaxResult blindingImage(PointBlindingImageForm pointBlindingImageForm);

    boolean saveSlave(MapPoint mapPoint);

    List<MapPoint> listSlave(QueryWrapper<MapPoint> queryWrapper);

    boolean updateByIdSlave(MapPoint mapPoint);

    AjaxResult supplement(KafkaMessage kafkaMessage);

    AjaxResult getImageUrlByTime(MapPoint mapPoint);

    AjaxResult getImagesUrlByUuid(MapPoint mapPoint);
}
