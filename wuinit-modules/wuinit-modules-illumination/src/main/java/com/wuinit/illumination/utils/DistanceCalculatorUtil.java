package com.wuinit.illumination.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DistanceCalculatorUtil {
    private static final double EARTH_RADIUS = 6371000; // 地球半径，单位米

    /**
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return 距离单位米
     */
    public static BigDecimal calculateDistance(double lat1, double lon1, double lat2, double lon2) {
        // 将经纬度转换为弧度
        double radLat1 = Math.toRadians(lat1);
        double radLon1 = Math.toRadians(lon1);
        double radLat2 = Math.toRadians(lat2);
        double radLon2 = Math.toRadians(lon2);

        // 计算经纬度之间的差距
        double deltaLat = radLat2 - radLat1;
        double deltaLon = radLon2 - radLon1;

        // 应用Haversine公式计算距离
        double a = Math.pow(Math.sin(deltaLat / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(deltaLon / 2), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = EARTH_RADIUS * c;

        return new BigDecimal(distance).setScale(2, RoundingMode.DOWN);
    }
}