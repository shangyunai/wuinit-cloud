package com.wuinit.illumination.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wuinit.illumination.domain.entity.MapStreetExt;

public interface MapStreetExtService extends IService<MapStreetExt> {

}
