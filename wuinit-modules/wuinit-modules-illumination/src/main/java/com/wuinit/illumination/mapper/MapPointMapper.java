package com.wuinit.illumination.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuinit.illumination.domain.entity.MapPoint;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MapPointMapper extends BaseMapper<MapPoint> {

}
