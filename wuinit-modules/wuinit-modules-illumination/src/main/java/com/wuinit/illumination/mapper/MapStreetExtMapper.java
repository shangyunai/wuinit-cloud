package com.wuinit.illumination.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuinit.illumination.domain.entity.MapStreetExt;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MapStreetExtMapper extends BaseMapper<MapStreetExt> {

}
