package com.wuinit.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import com.wuinit.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 文件服务
 *
 * @author wuinit
 */
@EnableCustomSwagger2
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class WuinitFileApplication {
    public static void main(String[] args) {
        SpringApplication.run(WuinitFileApplication.class, args);
        System.out.println("======= wuinit file start =======");
    }


}
