package com.illumination;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


@Component
@Order(value = 2)
public class IlluminationApplicationRunner implements org.springframework.boot.ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(IlluminationApplicationRunner.class);


    @Override
    public void run(ApplicationArguments args) {

    }


}
