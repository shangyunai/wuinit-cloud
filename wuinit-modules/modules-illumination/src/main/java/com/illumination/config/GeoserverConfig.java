package com.illumination.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "geoserver")
public class GeoserverConfig {
    /**
     * 成都 天地图 token
     */
    private String baseMapToken;
    /**
     * 成都 天地图 黑色主题
     */
    private String baseDarkMapUrl;
    /**
     * 成都 天地图 白色主题
     */
    private String baseWhiteMapUrl;
    /**
     * 成都 地图 道路检索矢量图层
     */
    private String baseMapSearchUrl;
    /**
     * geoserver wfs 矢量图层编辑
     */
    private String wfsUrl;
    /**
     * 道路矢量图层地址
     */
    private String streetUrl;
    private String editStreetUrl;
}
