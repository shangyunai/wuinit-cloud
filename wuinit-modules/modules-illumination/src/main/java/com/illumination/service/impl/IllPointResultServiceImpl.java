package com.illumination.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.illumination.domain.po.IllPointResult;
import com.illumination.mapper.IllPointResultMapper;
import com.illumination.service.IllPointResultService;
import org.springframework.stereotype.Service;

@Service
public class IllPointResultServiceImpl extends ServiceImpl<IllPointResultMapper, IllPointResult> implements IllPointResultService {


}
