package com.illumination.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.illumination.domain.po.IllRoadExt;

public interface IllRoadExtService extends IService<IllRoadExt> {

}
