package com.illumination.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.illumination.domain.po.IllRoad;
import com.illumination.domain.query.RoadTreeQuery;
import com.wuinit.common.core.web.domain.AjaxResult;

public interface IllRoadService extends IService<IllRoad> {
    AjaxResult getTree(RoadTreeQuery roadTreeQuery);
}
