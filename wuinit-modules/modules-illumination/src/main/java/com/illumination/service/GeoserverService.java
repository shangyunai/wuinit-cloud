package com.illumination.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.illumination.domain.form.GeoserverRoadForm;
import com.illumination.domain.po.IllRoad;
import com.illumination.domain.query.RoadTreeQuery;
import com.wuinit.common.core.web.domain.AjaxResult;

public interface GeoserverService {
    AjaxResult geoserverWfs(GeoserverRoadForm geoserverRoadForm);
}
