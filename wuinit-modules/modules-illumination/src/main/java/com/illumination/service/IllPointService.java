package com.illumination.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.illumination.domain.po.IllPoint;

public interface IllPointService extends IService<IllPoint> {

}
