package com.illumination.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.illumination.domain.po.IllRoadExt;
import com.illumination.mapper.IllRoadExtMapper;
import com.illumination.service.IllRoadExtService;
import org.springframework.stereotype.Service;

@Service
public class IllRoadExtServiceImpl extends ServiceImpl<IllRoadExtMapper, IllRoadExt> implements IllRoadExtService {


}
