package com.illumination.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.illumination.domain.po.IllPoint;
import com.illumination.mapper.IllPointMapper;
import com.illumination.service.IllPointService;
import org.springframework.stereotype.Service;

@Service
public class IllPointServiceImpl extends ServiceImpl<IllPointMapper, IllPoint> implements IllPointService {


}
