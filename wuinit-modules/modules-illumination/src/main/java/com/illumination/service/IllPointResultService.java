package com.illumination.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.illumination.domain.po.IllPointResult;

public interface IllPointResultService extends IService<IllPointResult> {

}
