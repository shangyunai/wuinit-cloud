package com.illumination.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.illumination.domain.po.IllRoad;
import com.illumination.domain.query.RoadTreeQuery;
import com.illumination.mapper.IllRoadMapper;
import com.illumination.service.IllRoadService;
import com.wuinit.common.core.web.domain.AjaxResult;
import org.springframework.stereotype.Service;

@Service
public class IllRoadServiceImpl extends ServiceImpl<IllRoadMapper, IllRoad> implements IllRoadService {


    @Override
    public AjaxResult getTree(RoadTreeQuery roadTreeQuery) {
        return null;
    }
}
