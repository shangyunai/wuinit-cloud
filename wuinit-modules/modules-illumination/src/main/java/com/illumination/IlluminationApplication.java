package com.illumination;

import com.wuinit.common.security.annotation.EnableCustomConfig;
import com.wuinit.common.security.annotation.EnableRyFeignClients;
import com.wuinit.common.swagger.annotation.EnableCustomSwagger2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * 照明项目模块
 *
 * @author wuinit
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
@Slf4j
public class IlluminationApplication {
    public static void main(String[] args) {
        SpringApplication.run(IlluminationApplication.class, args);
        log.info("=======  illumination start =======");
    }

    @Bean
    public RestTemplate restTemplate() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(5000);
        factory.setReadTimeout(60000);
        return new RestTemplate(factory);
    }
}
