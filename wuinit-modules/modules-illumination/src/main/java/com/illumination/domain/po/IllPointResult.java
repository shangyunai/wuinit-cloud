package com.illumination.domain.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 监测点有效数据对象 ill_point_result
 *
 * @author wuinit
 * @date 2023-12-03
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ill_point_result")
public class IllPointResult implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 经度
     */
    private BigDecimal lon;

    /**
     * 纬度
     */
    private BigDecimal lat;

    /**
     * 道路uuid
     */
    private String streetUuid;

    /**
     * 监测点时间戳
     */
    private Long time;


    /**
     * 平均照度
     */
    private BigDecimal lightAverage;

    /**
     * 照度均匀度
     */
    private BigDecimal lightUniformity;

    /**
     * 平均亮度
     */
    private BigDecimal brightnessAverage;

    /**
     * 亮度均匀度
     */
    private BigDecimal brightnessUniformity;

    /**
     * 照度最大值
     */
    private BigDecimal lightMax;

    /**
     * 照度最小值
     */
    private BigDecimal lightMin;

    /**
     * 纵向均匀度
     */
    private BigDecimal longitudinal;

    /**
     * 平均速度
     */
    private BigDecimal speed;

    /**
     * 是否是有效数据 0 无效 1 有效
     */
    private Integer isDataOk;

    /**
     * 当前道路监测编号
     */
    private String roadMonitorNumber;

    /**
     * 监测结果 1 正常 2 偏低
     */
    private Integer result;
}
