package com.illumination.domain.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuinit.common.core.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 道路对象 ill_road
 *
 * @author wuinit
 * @date 2023-12-03
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ill_road")
public class IllRoad implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 应用ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 道路唯一标识
     */
    private String uuid;
    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String region;

    /**
     * 道路名称
     */
    private String name;

    /**
     * 道路等级 todo 待划分道路等级
     */
    private Integer level;

    /**
     * 道路类型 1.主干路 2.次干路 3.支路 4.快速路
     */
    private String type;

    /**
     * 道路长度
     */
    private BigDecimal distance;

    /**
     * 道路起点
     */
    private String startStr;

    /**
     * 道路终点
     */
    private String endStr;

    /**
     * 管理单位
     */
    private String managementUnit;
}
