package com.illumination.domain.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 道路属性扩展表 ill_road_ext
 *
 * @author wuinit
 * @date 2023-12-03
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ill_road_ext")
public class IllRoadExt implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 道路唯一标识uuid
     */
    private String uuid;
    /**
     * 路面材料
     */
    private String materials;
    /**
     * 车道数量
     */
    private String lanesNum;
    /**
     * 光源类型
     */
    private String lightType;
    /**
     * 光源功率
     */
    private String lightPower;
    /**
     * 灯杆类型
     */
    private String poleType;
    /**
     * 排列方式
     */
    private String arrangement;
    /**
     * 安装高度
     */
    private String installationHeight;
    /**
     * 灯杆间距
     */
    private String poleSpacing;
    /**
     * 路面系数
     */
    private String roadCoefficient;
    /**
     * 校正系数
     */
    private String correctionCoefficient;
}
