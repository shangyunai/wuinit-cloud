package com.illumination.domain.query;

import lombok.Data;

@Data
public class RoadTreeQuery {
    private String region;
    private Integer status;
    private String name;
}
