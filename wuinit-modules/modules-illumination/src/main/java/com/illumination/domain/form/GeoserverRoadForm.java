package com.illumination.domain.form;

import lombok.Data;

@Data
public class GeoserverRoadForm {

    /**
     * 操作类型 add delete update
     */
    private String optionType;

    /**
     * geoserver xml
     */
    private String textXml;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String region;

    /**
     * 道路名称
     */
    private String name;

    /**
     * 道路等级
     */
    private String level;

    /**
     * 道路地图显示状态
     */
    private String decs;

    /**
     * 道路类型
     */
    private String type;

    /**
     * 道路唯一标识
     */
    private String uuid;

}
