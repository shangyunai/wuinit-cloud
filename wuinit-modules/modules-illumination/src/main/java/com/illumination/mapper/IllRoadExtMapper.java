package com.illumination.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.illumination.domain.po.IllRoadExt;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IllRoadExtMapper extends BaseMapper<IllRoadExt> {

}
