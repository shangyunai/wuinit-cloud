package com.illumination.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.illumination.domain.po.IllPointResult;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IllPointResultMapper extends BaseMapper<IllPointResult> {

}
