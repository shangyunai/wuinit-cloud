package com.illumination.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.illumination.domain.po.IllPoint;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IllPointMapper extends BaseMapper<IllPoint> {

}
