package com.illumination.controller;

import com.illumination.domain.query.RoadTreeQuery;
import com.illumination.service.IllRoadService;
import com.wuinit.common.core.web.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 道路Controller
 *
 * @author wuinit
 * @date 2023-12-03
 */
@RestController
@RequestMapping("/road")
public class IllRoadController {
    @Autowired
    private IllRoadService illRoadService;

    /**
     * 获取道路列表树
     *
     * @return
     */
    @PostMapping("/get/tree")
    public AjaxResult getTree(@RequestBody RoadTreeQuery roadTreeQuery) {
        return illRoadService.getTree(roadTreeQuery);
    }
}

