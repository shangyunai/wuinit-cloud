package com.wuinit.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.wuinit.common.security.annotation.EnableCustomConfig;
import com.wuinit.common.security.annotation.EnableRyFeignClients;
import com.wuinit.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 系统模块
 *
 * @author wuinit
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class WuinitSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(WuinitSystemApplication.class, args);
        System.out.println("======= wuinit system start =======");
    }
}
