package com.wuinit.workflow.utils;

import org.camunda.bpm.engine.impl.cfg.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author:wuinit
 * @Description:
 * @Date:Created in 2022/10/17 13:01
 */
@Component
public class IdWorkerIdGenerator implements IdGenerator {

    @Autowired
    private IdWorker idWorker;

    @Override
    public String getNextId() {
        return idWorker.nextId() + "";
    }
}
