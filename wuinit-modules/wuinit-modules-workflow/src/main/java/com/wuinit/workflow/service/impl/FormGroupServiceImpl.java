package com.wuinit.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuinit.workflow.domain.entity.FormGroups;
import com.wuinit.workflow.mapper.FormGroupsMapper;
import com.wuinit.workflow.service.FormGroupService;
import org.springframework.stereotype.Service;

/**
 * @author : wuinit
 * @version : 1.0
 */
@Service
public class FormGroupServiceImpl extends ServiceImpl<FormGroupsMapper, FormGroups> implements FormGroupService {


}
