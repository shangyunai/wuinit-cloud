package com.wuinit.workflow.domain.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author wuinit
 * @create 2022-10-16 9:42
 */
@Data
public class OptionVO {
    private String comments;
    private String userId;
    private String userName;
    private Date createTime;
}
