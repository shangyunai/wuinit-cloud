package com.wuinit.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuinit.workflow.domain.entity.TemplateGroup;
import com.wuinit.workflow.domain.entity.TemplateGroupBo;
import com.wuinit.workflow.mapper.TemplateGroupMapper;
import com.wuinit.workflow.service.TemplateGroupService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author : wuinit
 * @version : 1.0
 */
@Service
public class TemplateGroupServiceImpl extends ServiceImpl<TemplateGroupMapper, TemplateGroup> implements TemplateGroupService {

    @Override
    public List<TemplateGroupBo> getTemplateGroups() {
        return baseMapper.getAllFormAndGroups();
    }
}
