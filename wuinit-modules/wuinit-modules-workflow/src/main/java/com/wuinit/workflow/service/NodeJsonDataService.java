package com.wuinit.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wuinit.workflow.domain.entity.NodeJsonData;

/**
 * @author : wuinit
 * @version : 1.0
 */
public interface NodeJsonDataService extends IService<NodeJsonData> {


}
