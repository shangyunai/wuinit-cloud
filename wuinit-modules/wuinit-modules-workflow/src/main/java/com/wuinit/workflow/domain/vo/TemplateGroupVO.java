package com.wuinit.workflow.domain.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author :wuinit
 * @date : 2020/9/21
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TemplateGroupVO {

    private Integer id;

    private String name;

    private List<Template> items;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Template {

        private String formId;

        private Integer tgId;

        private String formName;

        private String icon;

        private Boolean isStop;

        private String remark;
        private JSONObject logo;

        private String background;

        private String updated;
        private String templateId;
    }


}
