package com.wuinit.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wuinit.workflow.domain.entity.TemplateGroup;
import com.wuinit.workflow.domain.entity.TemplateGroupBo;

import java.util.List;

/**
 * @author : wuinit
 * @version : 1.0
 */
public interface TemplateGroupService extends IService<TemplateGroup> {

    List<TemplateGroupBo> getTemplateGroups();
}
