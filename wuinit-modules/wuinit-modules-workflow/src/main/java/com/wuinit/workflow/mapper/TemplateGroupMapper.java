package com.wuinit.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuinit.workflow.domain.entity.TemplateGroup;
import com.wuinit.workflow.domain.entity.TemplateGroupBo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author : wuinit
 * @date : 2020/9/21
 */
@Mapper
public interface TemplateGroupMapper extends BaseMapper<TemplateGroup> {

    /**
     * 查询所有表单及组
     *
     * @return
     */
    @Select("SELECT fg.group_id, tg.id, fg.group_name, pt.template_id, pt.remark, pt.is_stop, pt.updated, pt.template_name, " +
            "pt.icon, pt.background FROM process_templates pt LEFT JOIN template_group tg ON pt.template_id = tg.template_id\n" +
            "RIGHT JOIN form_groups fg ON tg.group_id = fg.group_id ORDER BY fg.sort_num ASC, tg.sort_num ASC")
    List<TemplateGroupBo> getAllFormAndGroups();
}
