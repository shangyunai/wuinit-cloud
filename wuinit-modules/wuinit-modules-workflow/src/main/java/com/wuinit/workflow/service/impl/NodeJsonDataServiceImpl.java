package com.wuinit.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuinit.workflow.domain.entity.NodeJsonData;
import com.wuinit.workflow.mapper.NodeJsonDataMapper;
import com.wuinit.workflow.service.NodeJsonDataService;
import org.springframework.stereotype.Service;

/**
 * @author : wuinit
 * @version : 1.0
 */
@Service
public class NodeJsonDataServiceImpl extends ServiceImpl<NodeJsonDataMapper, NodeJsonData> implements NodeJsonDataService {

}
