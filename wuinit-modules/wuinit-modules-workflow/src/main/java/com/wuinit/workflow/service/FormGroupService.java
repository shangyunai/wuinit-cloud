package com.wuinit.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wuinit.workflow.domain.entity.FormGroups;

/**
 * @author : wuinit
 * @version : 1.0
 */
public interface FormGroupService extends IService<FormGroups> {


}
