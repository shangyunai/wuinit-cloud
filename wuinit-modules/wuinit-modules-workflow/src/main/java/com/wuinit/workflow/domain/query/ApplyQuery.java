package com.wuinit.workflow.domain.query;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author wuinit
 * @create 2022-10-14 23:47
 */
@Data
@ApiModel("我发起流程")
public class ApplyQuery extends  PageQuery{
}
