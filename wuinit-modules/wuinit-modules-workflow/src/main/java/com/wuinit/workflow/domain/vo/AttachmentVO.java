package com.wuinit.workflow.domain.vo;

import lombok.Data;

/**
 * @author wuinit
 * @create 2022-10-15 17:04
 */
@Data
public class AttachmentVO {
    private String id;
    private String name;
    private String url;
}
