package com.wuinit.workflow.domain.form;

import lombok.Data;

/**
 * @author wuinit 文件附件
 * @create 2022-10-15 17:04
 */
@Data
public class AttachmentForm {
    private String id;
    private String name;
    private String url;
}
