package com.wuinit.workflow.domain.json;

import lombok.Data;

/**
 * @Author:wuinit
 * @Description:
 * @Date:Created in 2022/10/9 16:26
 */
@Data
public class FormOperates {
    private String id;
    private String title;
    private Boolean required;
    private String perm;
}
