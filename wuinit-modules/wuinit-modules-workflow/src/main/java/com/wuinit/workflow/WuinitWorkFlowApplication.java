package com.wuinit.workflow;

import com.wuinit.common.security.annotation.EnableCustomConfig;
import com.wuinit.common.security.annotation.EnableRyFeignClients;
import com.wuinit.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 工作流模块
 *
 * @author wuinit
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class WuinitWorkFlowApplication {

    public static void main(String[] args) {
        SpringApplication.run(WuinitWorkFlowApplication.class, args);
        System.out.println("======= wuinit work flow start =======");
    }


}
