package com.wuinit.workflow.domain.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wuinit
 * @create 2022-10-15 10:33
 */
@Data
@ApiModel("分页")
public class PageQuery {
    @ApiModelProperty(value = "第几页")
    private Integer pageNo;
    @ApiModelProperty(value = "多少条")
    private Integer pageSize;
}
