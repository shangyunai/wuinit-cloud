package com.wuinit.workflow.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuinit.workflow.domain.entity.NodeJsonData;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author : wuinit
 * @version : 1.0
 */
@Mapper
public interface NodeJsonDataMapper extends BaseMapper<NodeJsonData> {
}
