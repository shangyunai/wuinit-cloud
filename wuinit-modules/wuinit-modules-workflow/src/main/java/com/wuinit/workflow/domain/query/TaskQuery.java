package com.wuinit.workflow.domain.query;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author wuinit
 * @create 2022-10-14 23:47
 */
@Data
@ApiModel("待办 需要返回给前端的VO")
public class TaskQuery extends PageQuery {

}
