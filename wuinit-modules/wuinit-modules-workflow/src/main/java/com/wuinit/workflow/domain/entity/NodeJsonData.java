package com.wuinit.workflow.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "node_json_data")
public class NodeJsonData {
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private String id;
    private String nodeJsonData;
    private String processDefinitionId;
}
