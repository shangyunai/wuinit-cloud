package com.wuinit.workflow.domain.form;

import com.alibaba.fastjson.JSONObject;
import com.wuinit.system.api.domain.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author wuinit
 * @create 2022-10-15 16:27
 */
@Data
@ApiModel("各个按钮 处理数据需要传递的参数")
public class HandleDataForm {
    @ApiModelProperty("任务id")
    private String taskId;
    @ApiModelProperty("流程实例id")
    private String processInstanceId;
    @ApiModelProperty("表单数据")
    private JSONObject formData;
    @ApiModelProperty("附件")
    private List attachments;
    @ApiModelProperty("意见")
    private String comments;
    @ApiModelProperty("签名信息")
    private String signInfo;
    @ApiModelProperty("转办用户信息")
    private SysUser transferUserInfo;
    @ApiModelProperty("加签用户信息")
    private SysUser multiAddUserInfo;
    @ApiModelProperty("退回节点id")
    private String rollbackId;
    @ApiModelProperty("委派的人")
    private SysUser delegateUserInfo;
}
