package com.wuinit.workflow.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.wuinit.common.core.domain.R;
import com.wuinit.workflow.domain.entity.*;
import com.wuinit.workflow.domain.form.ProcessTemplateForm;
import com.wuinit.workflow.domain.json.ChildNode;
import com.wuinit.workflow.domain.json.SettingsInfo;
import com.wuinit.workflow.domain.vo.TemplateGroupVO;
import com.wuinit.workflow.exception.WorkFlowException;
import com.wuinit.workflow.mapper.ProcessTemplatesMapper;
import com.wuinit.workflow.service.FormGroupService;
import com.wuinit.workflow.service.NodeJsonDataService;
import com.wuinit.workflow.service.ProcessTemplateService;
import com.wuinit.workflow.service.TemplateGroupService;
import com.wuinit.workflow.utils.IdWorker;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.builder.AbstractFlowNodeBuilder;
import org.camunda.bpm.model.bpmn.builder.EndEventBuilder;
import org.camunda.bpm.model.bpmn.builder.ProcessBuilder;
import org.camunda.bpm.model.bpmn.builder.StartEventBuilder;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import static com.wuinit.workflow.constants.WorkFlowConstants.*;
import static com.wuinit.workflow.utils.BpmnModelUtils.connect;
import static com.wuinit.workflow.utils.BpmnModelUtils.createStartEvent;
import static com.wuinit.workflow.utils.BpmnModelUtils.create;

/**
 * @author : wuinit
 * @version : 1.0
 * @decs 流程模板实现类
 */
@Service
public class ProcessTemplateServiceImpl extends ServiceImpl<ProcessTemplatesMapper, ProcessTemplates> implements ProcessTemplateService {

    @Autowired
    private IdWorker idWorker;
    @Autowired
    private TemplateGroupService templateGroupService;
    @Autowired
    private FormGroupService formGroupService;
    @Autowired
    private NodeJsonDataService nodeJsonDataService;

    @Resource
    private RepositoryService repositoryService;

    @Override
    public R save(ProcessTemplateForm processTemplateForm) {
        /*审批流程 json*/
        String processJson = processTemplateForm.getProcess();
        ChildNode childNode = JSONObject.parseObject(processJson, new TypeReference<ChildNode>() {
        });
        String settingsJson = processTemplateForm.getSettings();
        SettingsInfo settingsInfo = JSONObject.parseObject(settingsJson, new TypeReference<SettingsInfo>() {
        });
        String remark = processTemplateForm.getRemark();
        String formItems = processTemplateForm.getFormItems();
        String formName = processTemplateForm.getFormName();
        String logo = processTemplateForm.getLogo();
        Integer groupId = processTemplateForm.getGroupId();
        String templateId = idWorker.nextId() + "";

        ProcessTemplates processTemplates = ProcessTemplates.builder().build();
        processTemplates.setTemplateId(templateId);
        processTemplates.setTemplateName(formName);
        processTemplates.setGroupId(groupId);
        processTemplates.setFormItems(formItems);
        processTemplates.setProcess(processJson);
        processTemplates.setIcon(logo);
        processTemplates.setBackground(logo);
        processTemplates.setNotify(settingsInfo.getNotify().toJSONString());
        String adminInfo = JSONObject.toJSONString(settingsInfo.getAdmin());
        processTemplates.setSettings(settingsJson);
        processTemplates.setWhoCommit(adminInfo);
        processTemplates.setWhoEdit(adminInfo);
        processTemplates.setWhoExport(adminInfo);
        processTemplates.setRemark(processTemplateForm.getRemark());
        processTemplates.setIsStop(false);
        Date date = new Date();
        processTemplates.setCreated(date);
        processTemplates.setUpdated(date);
        save(processTemplates);

        TemplateGroup templateGroup = new TemplateGroup();
        templateGroup.setTemplateId(processTemplates.getTemplateId());
        templateGroup.setGroupId(groupId);
        templateGroup.setSortNum(0);
        templateGroup.setCreated(date);
        templateGroupService.save(templateGroup);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("processJson", processJson);
        jsonObject.put("formJson", formItems);
        BpmnModelInstance bpmnModel = assemBpmnModel(jsonObject, childNode, remark, formName, groupId, templateId);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Bpmn.writeModelToStream(outputStream, bpmnModel);
        byte[] bytes = outputStream.toByteArray();
        System.err.println(new String(bytes));
        Deployment deployment = repositoryService.createDeployment().addModelInstance(processTemplates.getFormName() + ".bpmn", bpmnModel).name(processTemplates.getFormName())
                //      .category(template.getGroupId() + "")
                .deploy();
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId()).singleResult();
        NodeJsonData nodeJsonData = new NodeJsonData();
        nodeJsonData.setNodeJsonData(processTemplates.getProcess());
        nodeJsonData.setProcessDefinitionId(processDefinition.getId());
        nodeJsonDataService.save(nodeJsonData);
        return R.ok();
    }

    @Override
    public R getGroups() {
        List<TemplateGroupBo> allformAndGroups = templateGroupService.getTemplateGroups();
        Map<Integer, List<TemplateGroupBo>> coverMap = new LinkedHashMap<>();
        allformAndGroups.forEach(fg -> {
            List<TemplateGroupBo> bos = coverMap.get(fg.getGroupId());
            if (CollectionUtil.isEmpty(bos)) {
                List<TemplateGroupBo> list = new ArrayList<>();
                list.add(fg);
                coverMap.put(fg.getGroupId(), list);
            } else {
                bos.add(fg);
            }
        });
        List<TemplateGroupVO> results = new ArrayList<>();
        coverMap.forEach((key, val) -> {
            List<TemplateGroupVO.Template> templates = new ArrayList<>();
            val.forEach(v -> {
                if (ObjectUtil.isNotNull(v.getTemplateId())) {
                    templates.add(TemplateGroupVO.Template.builder().formId(v.getTemplateId()).tgId(v.getId()).remark(v.getRemark()).formName(v.getTemplateName()).icon(v.getIcon()).isStop(v.getIsStop()).updated(DateFormatUtils.format(v.getUpdated(), "yyyy年MM月dd日 HH时:mm分:ss秒")).background(v.getBackground()).templateId(v.getTemplateId()).logo(JSONObject.parseObject(v.getBackground(), new TypeReference<JSONObject>() {
                    })).build());
                }
            });
            results.add(TemplateGroupVO.builder().id(key).name(val.get(0).getGroupName()).items(templates).build());
        });
        return R.ok(results);
    }

    @Override
    public R groupsSort(List<TemplateGroupVO> groups) {
        int i = 0, j = 0;
        try {
            for (TemplateGroupVO group : groups) {
                formGroupService.updateById(FormGroups.builder().groupId(group.getId()).sortNum(group.getId().equals(0) ? 999999 : i + 2).build());
                for (TemplateGroupVO.Template item : group.getItems()) {
                    templateGroupService.updateById(TemplateGroup.builder().id(item.getTgId()).groupId(group.getId()).templateId(item.getFormId()).sortNum(j + 1).build());
                    j++;
                }
                i++;
                j = 0;
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return R.fail("排序异常: " + e.getMessage());
        }
        return R.ok("排序成功");
    }

    @Override
    public R updateGroupName(Integer id, String name) {
        formGroupService.updateById(FormGroups.builder().groupId(id).groupName(name.trim()).build());
        return R.ok("修改成功");
    }

    @Override
    public R createGroup(String name) {
        LambdaQueryWrapper<FormGroups> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(FormGroups::getGroupName, name);
        if (formGroupService.count(lambdaQueryWrapper) > 0) {
            return R.fail("分组名称 [" + name + "] 已存在");
        }
        Date date = new Date();
        FormGroups formGroups = FormGroups.builder().groupName(name).sortNum(1).created(date).updated(date).build();
        formGroupService.save(formGroups);
        return R.ok("添加分组 " + name + " 成功");
    }

    @Override
    public R deleteGroup(Integer id) {
        LambdaUpdateWrapper<TemplateGroup> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.set(TemplateGroup::getGroupId, 1);
        lambdaUpdateWrapper.eq(TemplateGroup::getGroupId, id);
        templateGroupService.update(lambdaUpdateWrapper);
        formGroupService.removeById(id);
        return R.ok("删除分组成功");
    }

    @Override
    public R getProcessTemplateById(String templateId) {
        ProcessTemplates processTemplates = getById(templateId);
        processTemplates.setLogo(processTemplates.getIcon());
        processTemplates.setFormId(processTemplates.getTemplateId());
        processTemplates.setFormName(processTemplates.getTemplateName());
        return R.ok(processTemplates);
    }

    @Override
    public R updateProcessTemplate(String templateId, String type, Integer groupId) {
        boolean isStop = "stop".equals(type);


        ProcessTemplates build = ProcessTemplates.builder().templateId(templateId).isStop(isStop)
                .build();
        if ("using".equals(type) || isStop) {
            updateById(
                    ProcessTemplates.builder().templateId(templateId).isStop(isStop).build());
            LambdaUpdateWrapper<TemplateGroup> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
            lambdaUpdateWrapper.eq(TemplateGroup::getTemplateId, templateId);
            lambdaUpdateWrapper.set(TemplateGroup::getGroupId, isStop ? 0 : 1);
            templateGroupService
                    .update(lambdaUpdateWrapper);
        } else if ("delete".equals(type)) {
            removeById(templateId);
            LambdaQueryWrapper<TemplateGroup> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(TemplateGroup::getTemplateId, templateId);
            templateGroupService.remove(lambdaQueryWrapper);
        } else if ("move".equals(type)) {
            if (ObjectUtil.isNull(groupId)) {
                return R.fail("分组ID必传");
            }

            LambdaUpdateWrapper<TemplateGroup> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
            lambdaUpdateWrapper.eq(TemplateGroup::getTemplateId, templateId);
            lambdaUpdateWrapper.set(TemplateGroup::getGroupId, groupId);
            templateGroupService.update(lambdaUpdateWrapper);
        } else {
            return R.fail("不支持的操作");
        }
        return R.ok("操作成功");
    }


    private BpmnModelInstance assemBpmnModel(JSONObject jsonObject, ChildNode childNode, String remark, String formName, Integer groupId, String templateId) {
        ProcessBuilder process = Bpmn.createExecutableProcess();
        List<SequenceFlow> sequenceFlows = Lists.newArrayList();
        Map<String, ChildNode> childNodeMap = new HashMap<>();
        process.id(PROCESS_PREFIX + templateId);
        process.name(formName);
        process.documentation(remark);
        StartEventBuilder startEvent = createStartEvent(process);
        String lastNode = null;
        try {
            lastNode = create(startEvent, START_EVENT_ID, childNode, sequenceFlows, childNodeMap);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            throw new WorkFlowException("操作失败");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new WorkFlowException("操作失败");
        }
        AbstractFlowNodeBuilder<?, ?> fromBuilder = moveToNode(startEvent, lastNode);
        EndEventBuilder endEventBuilder = fromBuilder.endEvent(END_EVENT_ID).camundaExecutionListenerDelegateExpression(ExecutionListener.EVENTNAME_END, "${processListener}");
        connect(fromBuilder, endEventBuilder, lastNode, END_EVENT_ID, sequenceFlows, childNodeMap);
        return process.done();
    }

    private static AbstractFlowNodeBuilder<?, ?> moveToNode(AbstractFlowNodeBuilder<?, ?> startEventBuilder, String identifier) {
        return startEventBuilder.moveToNode(identifier);
    }
}
