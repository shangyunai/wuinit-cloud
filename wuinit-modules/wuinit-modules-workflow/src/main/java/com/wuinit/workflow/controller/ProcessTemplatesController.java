package com.wuinit.workflow.controller;

import com.wuinit.common.core.domain.R;
import com.wuinit.workflow.domain.form.ProcessTemplateForm;
import com.wuinit.workflow.domain.vo.TemplateGroupVO;
import com.wuinit.workflow.service.ProcessTemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author : wuinit
 * @decs ： 流程模板 controller
 * @date : 2020/9/17
 */
@RestController
@RequestMapping("process/template")
@Api(tags = {"流程模板相关接口"})
public class ProcessTemplatesController {

    @Autowired
    private ProcessTemplateService processTemplateService;

    /**
     * 创建流程模板
     *
     * @param processTemplateForm
     * @return
     */
    @ApiOperation("创建流程模板")
    @PostMapping("/save")
    public R save(@RequestBody ProcessTemplateForm processTemplateForm) {
        return processTemplateService.save(processTemplateForm);
    }

    /**
     * 查询所有模板分组
     *
     * @return
     */
    @ApiOperation("查询所有模板分组")
    @GetMapping("/groups")
    public R getGroups() {
        return processTemplateService.getGroups();
    }

    /**
     * 表单分组排序
     *
     * @param groups 分组数据
     * @return 排序结果
     */
    @PutMapping("/groups/sort")
    public R groupsSort(@RequestBody List<TemplateGroupVO> groups) {
        return processTemplateService.groupsSort(groups);
    }

    /**
     * 修改分组
     *
     * @param id   分组ID
     * @param name 分组名
     * @return 修改结果
     */
    @PutMapping("/group")
    public R updateGroupName(@RequestParam Integer id, @RequestParam String name) {
        return processTemplateService.updateGroupName(id, name);
    }


    /**
     * 新增分组
     *
     * @param name 分组名
     * @return 添加结果
     */
    @PostMapping("/group")
    public R createGroup(@RequestParam String name) {
        return processTemplateService.createGroup(name);
    }

    /**
     * 删除分组
     *
     * @param id
     * @return
     */
    @DeleteMapping("/group")
    public R deleteGroup(@RequestParam Integer id) {
        return processTemplateService.deleteGroup(id);
    }

    /**
     * 查询流程模板数据
     *
     * @param templateId 模板id
     * @return 模板详情数据
     */
    @GetMapping("/detail/{formId}")
    public Object getProcessTemplateById(@PathVariable("formId") String templateId) {
        return processTemplateService.getProcessTemplateById(templateId);
    }

    /**
     * 编辑模板
     *
     * @param templateId 摸板ID
     * @param type       类型 stop using delete
     * @return 操作结果
     */
    @PutMapping("/update")
    public R updateProcessTemplate(@RequestParam String templateId,
                                        @RequestParam String type,
                                        @RequestParam(required = false) Integer groupId) {
        return processTemplateService.updateProcessTemplate(templateId, type, groupId);
    }

}
