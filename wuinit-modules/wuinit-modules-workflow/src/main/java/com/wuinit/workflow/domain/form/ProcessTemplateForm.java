package com.wuinit.workflow.domain.form;

import lombok.Data;

/**
 * @Author:wuinit
 * @Description: 创建流程引擎模板表达
 * @Date:Created in 2022/10/9 15:51
 */
@Data
public class ProcessTemplateForm {
    private String formId;
    private String formItems;
    private String formName;
    //表单分类id
    private Integer groupId;
    private String logo;
    private String process;
    private String remark;
    private String settings;
}
