package com.wuinit.workflow.domain.form;

import com.alibaba.fastjson.JSONObject;
import com.wuinit.system.api.domain.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author wuinit
 * @create 2022-10-14 23:27
 */
@Data
@ApiModel("启动流程实例需要传入的参数")
public class StartProcessInstanceForm {
    @ApiModelProperty("流程定义id")
    private String processDefinitionId;
    @ApiModelProperty("表单数据")
    private JSONObject formData;
    @ApiModelProperty("发起人自选的信息, key 是nodeId, value是用户信息")
    private Map<String, List<SysUser>> processUsers;
    @ApiModelProperty("当前人用户信息,(因为本项目没有做登录功能,所以就是直接传递用户信息就行 简单起见)")
    private SysUser startUserInfo;
}
