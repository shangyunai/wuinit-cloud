package com.wuinit.workflow.utils;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wuinit.system.api.domain.SysUser;
import com.wuinit.workflow.constants.enums.AssigneeTypeEnums;
import com.wuinit.workflow.domain.entity.NodeJsonData;
import com.wuinit.workflow.domain.json.ChildNode;
import com.wuinit.workflow.exception.WorkFlowException;
import com.wuinit.workflow.service.NodeJsonDataService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.wuinit.workflow.domain.json.Properties;

import javax.annotation.Resource;
import java.util.*;

import static com.wuinit.workflow.constants.CommonConstants.START_USER_INFO;
import static com.wuinit.workflow.constants.WorkFlowConstants.*;
import static com.wuinit.workflow.utils.BpmnModelUtils.getChildNode;

@Component
@Slf4j
public class CamundaFlowUtils {

    @Resource
    private RuntimeService runtimeService;
    @Resource
    private HistoryService historyService;
    @Autowired
    private NodeJsonDataService nodeJsonDataService;

    public List<String> calculateTaskCandidateUsers(DelegateExecution execution) {
        if (StringUtils.isBlank(execution.getCurrentActivityId())) {
            Map<String, Object> variables = execution.getVariables();
            Set<String> strings = variables.keySet();
            String variableName = "";
            for (String string : strings) {
                if (string.endsWith("AssigneeList")) {
                    variableName = string;
                }
            }
            List list = MapUtil.get(variables, variableName, List.class);
            return list;
        }
        List<String> assigneeList = new ArrayList<>();
        LambdaQueryWrapper<NodeJsonData> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(NodeJsonData::getProcessDefinitionId, execution.getProcessDefinitionId());
        NodeJsonData nodeJsonData = nodeJsonDataService.getOne(lambdaQueryWrapper);
        String currentActivityId = execution.getCurrentActivityId();
        if (StringUtils.endsWith(execution.getCurrentActivityId(), MULTI_BODY)) {
            currentActivityId = currentActivityId.replace(MULTI_BODY, "");
        }

        ChildNode childNode = JSONObject.parseObject(nodeJsonData.getNodeJsonData(), new TypeReference<ChildNode>() {
        });
        ChildNode currentNode = getChildNode(childNode, currentActivityId);
        if (currentNode == null) {
            throw new WorkFlowException("查找审批人失败,请联系管理员重试");
        }
        Properties props = currentNode.getProps();
        String assignedType = props.getAssignedType();
        Map<String, Object> nobody = props.getNobody();
        String variable = currentActivityId + "AssigneeList";
        if (AssigneeTypeEnums.ASSIGN_USER.getTypeName().equals(assignedType)) {
            List<SysUser> assignedUser = props.getAssignedUser();
            for (SysUser userInfo : assignedUser) {
                assigneeList.add(String.valueOf(userInfo.getUserId()));
            }
        } else if (AssigneeTypeEnums.SELF_SELECT.getTypeName().equals(assignedType)) {
            List<String> assigneeUsers = (List<String>) execution.getVariable(currentActivityId);
            if (assigneeUsers != null) {
                assigneeList.addAll(assigneeUsers);
            }
        } else if (AssigneeTypeEnums.LEADER_TOP.getTypeName().equals(assignedType)) {
            //来自于users表的admin列
            throw new WorkFlowException("此项目没有RBAC功能,所以没法做这个功能,可以看一下我写的Ruoyi-Vue-Camunda的那个版本,里面有复杂的找人代码实现");
        } else if (AssigneeTypeEnums.LEADER.getTypeName().equals(assignedType)) {
            //向上找就行了
            throw new WorkFlowException("此项目没有RBAC功能,所以没法做这个功能,可以看一下我写的Ruoyi-Vue-Camunda的那个版本,里面有复杂的找人代码实现");
        } else if (AssigneeTypeEnums.ROLE.getTypeName().equals(assignedType)) {
            //向上找就行了
            throw new WorkFlowException("此项目没有RBAC功能,所以没法做这个功能,可以看一下我写的Ruoyi-Vue-Camunda的那个版本,里面有复杂的找人代码实现");
        } else if (AssigneeTypeEnums.SELF.getTypeName().equals(assignedType)) {
            String startUserJson = (String) execution.getVariable(START_USER_INFO);
            SysUser userInfo = JSONObject.parseObject(startUserJson, new TypeReference<SysUser>() {
            });
            assigneeList.add(String.valueOf(userInfo.getUserId()));
        } else if (AssigneeTypeEnums.FORM_USER.getTypeName().equals(assignedType)) {
            String formUser = props.getFormUser();
            List<JSONObject> assigneeUsers = (List<JSONObject>) execution.getVariable(formUser);
            if (assigneeUsers != null) {
                for (JSONObject assigneeUser : assigneeUsers) {
                    assigneeList.add(assigneeUser.getString("id"));
                }
            }

        }

        if (CollUtil.isEmpty(assigneeList)) {
            String handler = MapUtil.getStr(nobody, "handler");
            if ("TO_PASS".equals(handler)) {
                assigneeList.add(DEFAULT_NULL_ASSIGNEE);
                execution.setVariable(variable, assigneeList);
            } else if ("TO_REFUSE".equals(handler)) {
                execution.setVariable("autoRefuse", Boolean.TRUE);
                assigneeList.add(DEFAULT_NULL_ASSIGNEE);
                execution.setVariable(variable, assigneeList);
            } else if ("TO_ADMIN".equals(handler)) {
                assigneeList.add(DEFAULT_ADMIN_ASSIGNEE);
                execution.setVariable(variable, assigneeList);
            } else if ("TO_USER".equals(handler)) {
                Object assignedUserObj = nobody.get("assignedUser");
                if (assignedUserObj != null) {
                    List<JSONObject> assignedUser = (List<JSONObject>) assignedUserObj;
                    if (assignedUser.size() > 0) {
                        for (JSONObject object : assignedUser) {
                            assigneeList.add(object.getString("id"));
                        }
                        execution.setVariable(variable, assigneeList);
                    } else {
                        assigneeList.add(DEFAULT_NULL_ASSIGNEE);
                        execution.setVariable(variable, assigneeList);
                    }

                }

            } else {
                throw new WorkFlowException("找不到审批人,请检查配置!!!");
            }
        } else {
            execution.setVariable(variable, assigneeList);
        }


        execution.setVariableLocal(currentActivityId + "AssigneeList", assigneeList);
        return assigneeList;
    }


}
