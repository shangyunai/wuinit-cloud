package com.wuinit.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuinit.workflow.domain.entity.FormGroups;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author : wuinit
 * @date : 2020/9/21
 */
@Mapper
public interface FormGroupsMapper extends BaseMapper<FormGroups> {
}
