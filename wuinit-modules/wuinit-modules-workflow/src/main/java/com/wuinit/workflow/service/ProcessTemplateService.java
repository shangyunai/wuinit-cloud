package com.wuinit.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wuinit.common.core.domain.R;
import com.wuinit.workflow.domain.entity.ProcessTemplates;
import com.wuinit.workflow.domain.form.ProcessTemplateForm;
import com.wuinit.workflow.domain.vo.TemplateGroupVO;

import java.util.List;

/**
 * @author : wuinit
 * @version : 1.0
 */
public interface ProcessTemplateService extends IService<ProcessTemplates> {

    R save(ProcessTemplateForm processTemplateForm);

    R getGroups();

    R groupsSort(List<TemplateGroupVO> groups);

    R updateGroupName(Integer id, String name);

    R createGroup(String name);

    R deleteGroup(Integer id);

    R getProcessTemplateById(String templateId);

    R updateProcessTemplate(String templateId, String type, Integer groupId);

}
