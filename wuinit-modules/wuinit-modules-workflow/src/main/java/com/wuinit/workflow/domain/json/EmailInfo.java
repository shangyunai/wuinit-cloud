package com.wuinit.workflow.domain.json;

import lombok.Data;

import java.util.List;

/**
 * @Author:wuinit
 * @Description: 邮箱
 * @Date:Created in 2022/10/9 20:08
 */
@Data
public class EmailInfo {
    private String subject;
    private List<String> to;
    private String content;
}
