package com.wuinit.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.wuinit.common.security.annotation.EnableCustomConfig;
import com.wuinit.common.security.annotation.EnableRyFeignClients;
import com.wuinit.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 定时任务
 * 
 * @author wuinit
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients   
@SpringBootApplication
public class WuinitJobApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(WuinitJobApplication.class, args);
        System.out.println("======= wuinit job start =======");
    }
}
