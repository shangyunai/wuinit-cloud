package com.wuinit.illumination.domain.vo;

import com.wuinit.illumination.domain.entity.MapPoint;
import com.wuinit.illumination.domain.entity.MapStreet;
import com.wuinit.illumination.domain.entity.MapStreetExt;
import com.wuinit.illumination.domain.entity.MapStreetResult;
import lombok.Data;

import java.util.List;

@Data
public class MapStreetResultVo {
    private MapStreet mapStreet;
    private MapStreetExt mapStreetExt;
    private MapStreetResult mapStreetResult;
    private List<MapPoint> mapPoints;
}
