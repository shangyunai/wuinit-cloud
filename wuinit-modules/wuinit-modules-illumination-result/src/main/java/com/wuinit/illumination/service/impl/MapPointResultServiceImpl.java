package com.wuinit.illumination.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuinit.illumination.domain.entity.MapPointImage;
import com.wuinit.illumination.domain.entity.MapPointResult;
import com.wuinit.illumination.mapper.MapPointImageMapper;
import com.wuinit.illumination.mapper.MapPointResultMapper;
import com.wuinit.illumination.service.MapPointImageService;
import com.wuinit.illumination.service.MapPointResultService;
import org.springframework.stereotype.Service;

@Service
public class MapPointResultServiceImpl extends ServiceImpl<MapPointResultMapper, MapPointResult> implements MapPointResultService {


}
