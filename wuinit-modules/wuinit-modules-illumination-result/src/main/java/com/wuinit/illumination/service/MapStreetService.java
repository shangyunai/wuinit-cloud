package com.wuinit.illumination.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.common.core.web.page.TableDataInfo;
import com.wuinit.illumination.domain.entity.MapStreet;
import com.wuinit.illumination.domain.entity.MapStreetExt;
import com.wuinit.illumination.domain.form.MapStreetForm;
import com.wuinit.illumination.domain.query.MapStreetResultQuery;
import org.aspectj.weaver.loadtime.Aj;

public interface MapStreetService extends IService<MapStreet> {

    TableDataInfo list(MapStreet mapStreet, int pageNum, int pageSize);

    AjaxResult getInfo(Long id);

    AjaxResult insert(MapStreet mapStreet);

    AjaxResult update(MapStreet mapStreet);

    AjaxResult delete(Long[] id);

    AjaxResult geoserverWfs(MapStreetForm mapStreetForm);

    AjaxResult getUUID();

    AjaxResult getStreetTree(String status);

    AjaxResult getGeoserverConfig();

    String getGeoserverStreetMap();

    String getStreetByPoint(String pointStr);

    AjaxResult updateStreetStatus();

    AjaxResult sendKafka(String uuid);

    AjaxResult sendKafkaCheckStatus(String status);

    AjaxResult streetExtSave(MapStreetExt mapStreetExt);

    AjaxResult streetExtGet(String uuid);

    TableDataInfo resultList(MapStreetResultQuery mapStreetResultQuery);
    AjaxResult resultListAll();
    AjaxResult getCanNotCheck();

}
