package com.wuinit.illumination.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/kafka")
public class KafkaProducer {
    @Resource
    private KafkaTemplate<String, Object> kafkaTemplate;
    @Value("${kafka.topic.point}")
    private String topicPoint;

    @PostMapping("/send/message")
    public void sendMessage(@RequestBody String message) {
        kafkaTemplate.send(topicPoint, message);
    }
}
