package com.wuinit.illumination.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 地图点位检测时间抓拍 map_point_image
 *
 * @author wuinit
 * @date 2023-10-17
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "map_point_image")
public class MapPointImage implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 设备id
     */
    private String deviceId;
    /**
     * 抓拍时间戳
     */
    private Long time;
    /**
     * 抓拍图片地址
     */
    private String url;
}
