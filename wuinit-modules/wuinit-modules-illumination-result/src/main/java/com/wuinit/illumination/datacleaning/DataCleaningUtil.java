package com.wuinit.illumination.datacleaning;

import com.alibaba.nacos.shaded.com.google.gson.Gson;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuinit.common.core.constant.Constants;
import com.wuinit.common.core.domain.R;
import com.wuinit.common.core.utils.DateUtils;
import com.wuinit.common.core.utils.StringUtils;
import com.wuinit.common.core.utils.file.MimeTypeUtils;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.illumination.api.MapApi;
import com.wuinit.illumination.config.GeoserverConfig;
import com.wuinit.illumination.domain.entity.*;
import com.wuinit.illumination.domain.form.PointBlindingImageForm;
import com.wuinit.illumination.domain.result.MapFeatureResult;
import com.wuinit.illumination.domain.result.RoadVectorData;
import com.wuinit.illumination.geoserver.GeoserverUtils;
import com.wuinit.illumination.kafka.KafkaMessage;
import com.wuinit.illumination.kafka.KafkaProducer;
import com.wuinit.illumination.service.*;
import com.wuinit.illumination.utils.FileUtil;
import com.wuinit.system.api.RemoteFileService;
import com.wuinit.system.api.domain.SysFile;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * 数据清洗工具
 */
@Slf4j
@Component
public class DataCleaningUtil {
    @Autowired
    private MapPointService mapPointService;
    @Autowired
    private MapPointResultService mapPointResultService;
    @Autowired
    private MapStreetService mapStreetService;
    @Autowired
    private MapStreetResultService mapStreetResultService;
    @Autowired
    private GeoserverConfig geoserverConfig;
    @Autowired
    private RestTemplate restTemplate;
    @Resource(name = "threadPoolProcessorTaskExecutor")
    private ThreadPoolTaskExecutor executor;

    /**
     * 补全交叉点
     */
    public void completeCrossLinePoints() {
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        mapPointQueryWrapper.ne("speed", 0);
        //查询所有已有道路属性的点
        List<MapPoint> list = mapPointService.list(mapPointQueryWrapper);
        log.info("查询到已有道路属性的点位数量：[{}]", list.size());
        list.stream().forEach(pointItem -> {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    List<String> uuids = new ArrayList<>();
                    String point = "POINT(" + pointItem.getLon() + " " + pointItem.getLat() + ")";
                    //相交
                    String streetByPoint = MapApi.getInstance().getStreetByPoint(point, geoserverConfig, restTemplate);
                    if (streetByPoint != null) {
                        MapFeatureResult mapFeatureResult = new Gson().fromJson(streetByPoint, MapFeatureResult.class);
                        if (mapFeatureResult != null) {
                            List<MapFeatureResult.FeaturesDTO> features = mapFeatureResult.getFeatures();
                            if (!Collections.isEmpty(features)) {
                                features.stream().forEach(feature -> {
                                    MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                    if (properties != null) {
                                        String uuid = properties.getUuid();
                                        if (StringUtils.isNotEmpty(uuid)) {
                                            uuids.add(uuid);
                                        }
                                    }
                                });
                            } else {
                                //包含
                                String streetByPointContains = MapApi.getInstance().getStreetByPointContains(point, geoserverConfig, restTemplate);
                                if (streetByPointContains != null) {
                                    MapFeatureResult mapFeatureResultContains = new Gson().fromJson(streetByPointContains, MapFeatureResult.class);
                                    if (mapFeatureResultContains != null) {
                                        List<MapFeatureResult.FeaturesDTO> featuresContains = mapFeatureResultContains.getFeatures();
                                        if (!Collections.isEmpty(featuresContains)) {
                                            featuresContains.stream().forEach(feature -> {
                                                MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                                if (properties != null) {
                                                    String uuid = properties.getUuid();
                                                    if (StringUtils.isNotEmpty(uuid)) {
                                                        uuids.add(uuid);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //只保存有道路属性的点位
                    if (!Collections.isEmpty(uuids)) {
                        uuids.stream().forEach(uuid -> {
                            Integer isDataOk = pointItem.getIsDataOk();
                            if (isDataOk == null) {
                                pointItem.setIsDataOk(1);
                            }
                            pointItem.setStreetUuid(uuid);
                            mapPointService.saveSlave(pointItem);
                            log.info("==========save point slave ok ========");
                        });
                    }
                }
            });

        });
        log.info("========finish completeCrossLinePoints=======");
    }

    /**
     * 重新计算点位所属道路
     */
    public void recalculatePoints() {
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        mapPointQueryWrapper.ne("speed", 0);
        mapPointQueryWrapper.isNull("street_uuid");
        mapPointQueryWrapper.eq("isDataOk", 1);
        //查询所有有效点位，但是未归属道路的点位，重新归属道路
        List<MapPoint> list = mapPointService.list(mapPointQueryWrapper);
        log.info("查询有效点位数量：[{}]", list.size());
        list.stream().forEach(pointItem -> {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    String point = "POINT(" + pointItem.getLon() + " " + pointItem.getLat() + ")";
                    //相交
                    String streetByPoint = MapApi.getInstance().getStreetByPoint(point, geoserverConfig, restTemplate);
                    if (streetByPoint != null) {
                        MapFeatureResult mapFeatureResult = new Gson().fromJson(streetByPoint, MapFeatureResult.class);
                        if (mapFeatureResult != null) {
                            List<MapFeatureResult.FeaturesDTO> features = mapFeatureResult.getFeatures();
                            if (!Collections.isEmpty(features)) {
                                features.stream().forEach(feature -> {
                                    MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                    if (properties != null) {
                                        String uuid = properties.getUuid();
                                        //设置点位所属道路uuid，更新点位信息
                                        pointItem.setStreetUuid(uuid);
                                        mapPointService.updateById(pointItem);
                                    }
                                });
                            } else {
                                //包含
                                String streetByPointContains = MapApi.getInstance().getStreetByPointContains(point, geoserverConfig, restTemplate);
                                if (streetByPointContains != null) {
                                    MapFeatureResult mapFeatureResultContains = new Gson().fromJson(streetByPointContains, MapFeatureResult.class);
                                    if (mapFeatureResultContains != null) {
                                        List<MapFeatureResult.FeaturesDTO> featuresContains = mapFeatureResultContains.getFeatures();
                                        if (!Collections.isEmpty(featuresContains)) {
                                            featuresContains.stream().forEach(feature -> {
                                                MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                                if (properties != null) {
                                                    String uuid = properties.getUuid();
                                                    if (StringUtils.isNotEmpty(uuid)) {
                                                        //设置点位所属道路uuid，更新点位信息
                                                        pointItem.setStreetUuid(uuid);
                                                        mapPointService.updateById(pointItem);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });

        });
        log.info("========finish recalculatePoints=======");
    }

    /**
     * 补全道路缺失的道路框外点
     */
    public void fixStreetLinePoint() {
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        mapPointQueryWrapper.isNull("street_uuid");
        mapPointQueryWrapper.eq("isDataOk", 1);
        //查询是有效数据但是没有赋予道路属性的监测点
        List<MapPoint> mapPoints = mapPointService.list(mapPointQueryWrapper);
        for (MapPoint mapPoint : mapPoints) {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    String roadCodeP = mapPoint.getRoadMCode();
                    QueryWrapper<MapPointResult> mapPointResultQueryWrapper = new QueryWrapper<>();
                    mapPointResultQueryWrapper.select("count(1) as c", "street_uuid").groupBy("street_uuid").orderByDesc("c").eq("roadMCode", roadCodeP);
                    List<Map<String, Object>> maps = mapPointResultService.getBaseMapper().selectMaps(mapPointResultQueryWrapper);
                    if (!Collections.isEmpty(maps)) {
                        Map<String, Object> stringObjectMap = maps.get(0);
                        String streetUUid = (String) stringObjectMap.get("street_uuid");
                        //判断合格性
                        checkPointQualification(mapPoint, streetUUid);
                        mapPoint.setStreetUuid(streetUUid);
                        //更新原始数据表
                        mapPointService.updateById(mapPoint);
                        MapPointResult mapPointResult = new MapPointResult();
                        BeanUtils.copyProperties(mapPoint, mapPointResult);
                        mapPointResult.setId(null);
                        mapPointResult.setStreetUuid(streetUUid);
                        //存入有效结果数据表
                        mapPointResultService.save(mapPointResult);
                    }
                }
            });
        }
        log.info("========finish fixStreetLinePoint=======");
    }

    /**
     * 判断单点的合格性
     */
    public MapPoint checkPointQualification(MapPoint mapPoint, String uuid) {
        //有效数据，判断合格性
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        mapStreetQueryWrapper.eq("uuid", uuid);
        MapStreet mapStreet = mapStreetService.getOne(mapStreetQueryWrapper, false);
        if (mapStreet != null) {
            String streetType = mapStreet.getStreetType();
            BigDecimal lightAverage = mapPoint.getLightAverage();
            switch (streetType) {
                case "主干路":
                case "快速路":
                    if (lightAverage.compareTo(new BigDecimal(30)) >= 0) {
                        //合格
                        mapPoint.setCompliance("合格");
                    } else {
                        //偏低
                        mapPoint.setCompliance("偏低");
                    }
                    break;
                case "次干路":
                    if (lightAverage.compareTo(new BigDecimal(20)) >= 0) {
                        //合格
                        mapPoint.setCompliance("合格");
                    } else {
                        //偏低
                        mapPoint.setCompliance("偏低");
                    }
                    break;
                case "支路":
                    if (lightAverage.compareTo(new BigDecimal(10)) >= 0) {
                        //合格
                        mapPoint.setCompliance("合格");
                    } else {
                        //偏低
                        mapPoint.setCompliance("偏低");
                    }
                    break;
            }
        }
        return mapPoint;
    }

    /**
     * 判断点合格性
     */
    public void checkPointQualification() {
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        mapPointQueryWrapper.isNull("Compliance");
        mapPointQueryWrapper.isNotNull("street_uuid");
        mapPointQueryWrapper.eq("isDataOk", 1);
        List<MapPoint> mapPoints = mapPointService.list(mapPointQueryWrapper);
        mapPoints.stream().forEach(point -> {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    String streetUuid = point.getStreetUuid();
                    if (StringUtils.isNotEmpty(streetUuid)) {
                        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
                        mapStreetQueryWrapper.eq("uuid", streetUuid);
                        MapStreet mapStreet = mapStreetService.getOne(mapStreetQueryWrapper, false);
                        if (mapStreet != null) {
                            String streetType = mapStreet.getStreetType();
                            BigDecimal lightAverage = point.getLightAverage();
                            switch (streetType) {
                                case "主干路":
                                case "快速路":
                                    if (lightAverage.compareTo(new BigDecimal(30)) >= 0) {
                                        //合格
                                        point.setCompliance("合格");
                                    } else {
                                        //偏低
                                        point.setCompliance("偏低");
                                    }
                                    break;
                                case "次干路":
                                    if (lightAverage.compareTo(new BigDecimal(20)) >= 0) {
                                        //合格
                                        point.setCompliance("合格");
                                    } else {
                                        //偏低
                                        point.setCompliance("偏低");
                                    }
                                    break;
                                case "支路":
                                    if (lightAverage.compareTo(new BigDecimal(10)) >= 0) {
                                        //合格
                                        point.setCompliance("合格");
                                    } else {
                                        //偏低
                                        point.setCompliance("偏低");
                                    }
                                    break;
                            }
                            mapPointService.updateById(point);
                            log.info("=======update point qualification:{}======", point.getCompliance());
                        }
                    }
                }
            });

        });
        log.info("========finish checkPointQualification=======");
    }

    private static final int LINE_LIMIT_POINT = 5;
    private static final int LINE_LIMIT_POINT_ONE = 100;

    /**
     * 判断道路合格性
     */
    public void checkStreetQualification() {
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        mapStreetQueryWrapper.ne("region", "一期800条道路");
        List<MapStreet> streets = mapStreetService.list(mapStreetQueryWrapper);
        streets.stream().forEach(mapStreet -> {
            //查询道路点位
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        String uuid = mapStreet.getUuid();
                        String region = mapStreet.getRegion();
                        //判断之前是否已有该条路检测结果
                        QueryWrapper<MapStreetResult> mapStreetResultQueryWrapper = new QueryWrapper<>();
                        mapStreetResultQueryWrapper.eq("uuid", uuid);
                        mapStreetResultQueryWrapper.eq("check_time", 1);
                        MapStreetResult one = mapStreetResultService.getOne(mapStreetResultQueryWrapper, false);
                        if (one == null) {
                            QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
                            mapPointQueryWrapper.eq("street_uuid", uuid);
                            mapPointQueryWrapper.eq("isDataOk", 1);
                            mapPointQueryWrapper.orderByDesc("time");
                            List<MapPoint> mapPoints = mapPointService.list(mapPointQueryWrapper);
                            if ("一期800条道路".equals(region) && !Collections.isEmpty(mapPoints) && mapPoints.size() >= LINE_LIMIT_POINT_ONE) {
                                //平均照度
                                List<Double> lightAList = new ArrayList<>();
                                //平均照度均匀度
                                List<Double> lightUList = new ArrayList<>();
                                //平均亮度
                                List<Double> brightAList = new ArrayList<>();
                                //平均亮度均匀度
                                List<Double> brightUList = new ArrayList<>();
                                //平均车速
                                List<Double> speedAList = new ArrayList<>();
                                //照度达标数量
                                List<String> qualificationOk = new ArrayList<>();
                                mapPoints.stream().forEach(mapPoint -> {
                                    lightAList.add(mapPoint.getLightAverage().doubleValue());
                                    lightUList.add(mapPoint.getLightuniformity().doubleValue());
                                    brightAList.add(mapPoint.getAveragebrightness().doubleValue());
                                    brightUList.add(mapPoint.getBrightnessuniformity().doubleValue());
                                    speedAList.add(mapPoint.getSpeed().doubleValue());
                                    if ("合格".equals(mapPoint.getCompliance())) {
                                        qualificationOk.add(mapPoint.getCompliance());
                                    }
                                });
                                //得到平均数 照度
                                double lightAverage = lightAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //最大值 照度
                                double lightMax = lightAList.stream().mapToDouble(Double::valueOf).max().getAsDouble();
                                //最小值 照度
                                double lightMin = lightAList.stream().mapToDouble(Double::valueOf).min().getAsDouble();
                                //得到平均数 照度均匀度
                                double lightUAverage = lightUList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //得到平均数 照度
                                double brightAverage = brightAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //最大值 照度
                                double brightMax = brightAList.stream().mapToDouble(Double::valueOf).max().getAsDouble();
                                //最小值 照度
                                double brightMin = brightAList.stream().mapToDouble(Double::valueOf).min().getAsDouble();
                                //得到平均数 照度均匀度
                                double brightUAverage = brightUList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //得到平均数 照度均匀度
                                double speedAverage = speedAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //合格率占比
                                BigDecimal rate = new BigDecimal(qualificationOk.size()).divide(new BigDecimal(mapPoints.size()), 4, RoundingMode.DOWN).setScale(4, RoundingMode.DOWN);
                                MapStreetResult mapStreetResult = new MapStreetResult();
                                mapStreetResult.setTime(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, new Date(mapPoints.get(mapPoints.size() - 1).getTime() * 1000)));
                                //todo 设置检测人员
                                mapStreetResult.setPerson("王*楚");
                                mapStreetResult.setCheckTime(1);
                                mapStreetResult.setUuid(uuid);
                                mapStreetResult.setLightAverage(new BigDecimal(lightAverage));
                                mapStreetResult.setMaxLight(new BigDecimal(lightMax));
                                mapStreetResult.setMinLight(new BigDecimal(lightMin));
                                mapStreetResult.setLightUniformity(new BigDecimal(lightUAverage));
                                mapStreetResult.setAverageBrightness(new BigDecimal(brightAverage));
                                mapStreetResult.setMaxBright(new BigDecimal(brightMax));
                                mapStreetResult.setMinBright(new BigDecimal(brightMin));
                                mapStreetResult.setBrightnessUniformity(new BigDecimal(brightUAverage));
                                mapStreetResult.setAverageSpeed(new BigDecimal(speedAverage));
                                mapStreetResult.setRate(rate);
                                mapStreetResult.setStartLon(mapPoints.get(0).getLon());
                                mapStreetResult.setStartLat(mapPoints.get(0).getLat());
                                mapStreetResult.setEndLon(mapPoints.get(mapPoints.size() - 1).getLon());
                                mapStreetResult.setEndLat(mapPoints.get(mapPoints.size() - 1).getLat());
                                mapStreetResult.setName(mapStreet.getRegion());
                                String streetType = mapStreet.getStreetType();
                                BigDecimal lightAverageResult = mapStreetResult.getLightAverage();
                                switch (streetType) {
                                    case "主干路":
                                    case "快速路":
                                        if (lightAverageResult.compareTo(new BigDecimal(30)) >= 0) {
                                            //合格
                                            mapStreetResult.setLightResult("合格");
                                        } else {
                                            //偏低
                                            mapStreetResult.setLightResult("偏低");
                                        }
                                        break;
                                    case "次干路":
                                        if (lightAverageResult.compareTo(new BigDecimal(20)) >= 0) {
                                            //合格
                                            mapStreetResult.setLightResult("合格");
                                        } else {
                                            //偏低
                                            mapStreetResult.setLightResult("偏低");
                                        }
                                        break;
                                    case "支路":
                                        if (lightAverageResult.compareTo(new BigDecimal(10)) >= 0) {
                                            //合格
                                            mapStreetResult.setLightResult("合格");
                                        } else {
                                            //偏低
                                            mapStreetResult.setLightResult("偏低");
                                        }
                                        break;
                                }
                                QueryWrapper<MapStreet> mapStreetQueryWrapper1 = new QueryWrapper<>();
                                mapStreetQueryWrapper1.ne("region", "一期800条道路");
                                mapStreetQueryWrapper1.eq("street_name", mapStreet.getStreetName());
                                MapStreet one1 = mapStreetService.getOne(mapStreetQueryWrapper1, false);
                                if (one1 == null) {
                                    mapStreetResultService.save(mapStreetResult);
                                    log.info("============== 一期800条道路 新增一条道路检测结果==============");
                                }

                            } else if (!"一期800条道路".equals(region)) {
                                if (!Collections.isEmpty(mapPoints) && mapPoints.size() >= LINE_LIMIT_POINT) {
                                    //平均照度
                                    List<Double> lightAList = new ArrayList<>();
                                    //平均照度均匀度
                                    List<Double> lightUList = new ArrayList<>();
                                    //平均亮度
                                    List<Double> brightAList = new ArrayList<>();
                                    //平均亮度均匀度
                                    List<Double> brightUList = new ArrayList<>();
                                    //平均车速
                                    List<Double> speedAList = new ArrayList<>();
                                    //照度达标数量
                                    List<String> qualificationOk = new ArrayList<>();
                                    mapPoints.stream().forEach(mapPoint -> {
                                        lightAList.add(mapPoint.getLightAverage().doubleValue());
                                        lightUList.add(mapPoint.getLightuniformity().doubleValue());
                                        brightAList.add(mapPoint.getAveragebrightness().doubleValue());
                                        brightUList.add(mapPoint.getBrightnessuniformity().doubleValue());
                                        speedAList.add(mapPoint.getSpeed().doubleValue());
                                        if ("合格".equals(mapPoint.getCompliance())) {
                                            qualificationOk.add(mapPoint.getCompliance());
                                        }
                                    });
                                    //得到平均数 照度
                                    double lightAverage = lightAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                    //最大值 照度
                                    double lightMax = lightAList.stream().mapToDouble(Double::valueOf).max().getAsDouble();
                                    //最小值 照度
                                    double lightMin = lightAList.stream().mapToDouble(Double::valueOf).min().getAsDouble();
                                    //得到平均数 照度均匀度
                                    double lightUAverage = lightUList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                    //得到平均数 照度
                                    double brightAverage = brightAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                    //最大值 照度
                                    double brightMax = brightAList.stream().mapToDouble(Double::valueOf).max().getAsDouble();
                                    //最小值 照度
                                    double brightMin = brightAList.stream().mapToDouble(Double::valueOf).min().getAsDouble();
                                    //得到平均数 照度均匀度
                                    double brightUAverage = brightUList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                    //得到平均数 照度均匀度
                                    double speedAverage = speedAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                    //合格率占比
                                    BigDecimal rate = new BigDecimal(qualificationOk.size()).divide(new BigDecimal(mapPoints.size()), 4, RoundingMode.DOWN).setScale(4, RoundingMode.DOWN);
                                    MapStreetResult mapStreetResult = new MapStreetResult();
                                    mapStreetResult.setTime(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, new Date(mapPoints.get(mapPoints.size() - 1).getTime() * 1000)));
                                    //todo 设置检测人员
                                    mapStreetResult.setPerson("王*楚");
                                    mapStreetResult.setCheckTime(1);
                                    mapStreetResult.setUuid(uuid);
                                    mapStreetResult.setLightAverage(new BigDecimal(lightAverage));
                                    mapStreetResult.setMaxLight(new BigDecimal(lightMax));
                                    mapStreetResult.setMinLight(new BigDecimal(lightMin));
                                    mapStreetResult.setLightUniformity(new BigDecimal(lightUAverage));
                                    mapStreetResult.setAverageBrightness(new BigDecimal(brightAverage));
                                    mapStreetResult.setMaxBright(new BigDecimal(brightMax));
                                    mapStreetResult.setMinBright(new BigDecimal(brightMin));
                                    mapStreetResult.setBrightnessUniformity(new BigDecimal(brightUAverage));
                                    mapStreetResult.setAverageSpeed(new BigDecimal(speedAverage));
                                    mapStreetResult.setRate(rate);
                                    mapStreetResult.setStartLon(mapPoints.get(0).getLon());
                                    mapStreetResult.setStartLat(mapPoints.get(0).getLat());
                                    mapStreetResult.setEndLon(mapPoints.get(mapPoints.size() - 1).getLon());
                                    mapStreetResult.setEndLat(mapPoints.get(mapPoints.size() - 1).getLat());
                                    mapStreetResult.setName(mapStreet.getStreetName());
                                    String streetType = mapStreet.getStreetType();
                                    BigDecimal lightAverageResult = mapStreetResult.getLightAverage();
                                    switch (streetType) {
                                        case "主干路":
                                        case "快速路":
                                            if (lightAverageResult.compareTo(new BigDecimal(30)) >= 0) {
                                                //合格
                                                mapStreetResult.setLightResult("合格");
                                            } else {
                                                //偏低
                                                mapStreetResult.setLightResult("偏低");
                                            }
                                            break;
                                        case "次干路":
                                            if (lightAverageResult.compareTo(new BigDecimal(20)) >= 0) {
                                                //合格
                                                mapStreetResult.setLightResult("合格");
                                            } else {
                                                //偏低
                                                mapStreetResult.setLightResult("偏低");
                                            }
                                            break;
                                        case "支路":
                                            if (lightAverageResult.compareTo(new BigDecimal(10)) >= 0) {
                                                //合格
                                                mapStreetResult.setLightResult("合格");
                                            } else {
                                                //偏低
                                                mapStreetResult.setLightResult("偏低");
                                            }
                                            break;
                                    }
                                    mapStreetResultService.save(mapStreetResult);
                                    log.info("==============新增一条道路检测结果==============");

                                }
                            }
                        } else {

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.info("======error===={}", e.toString());
                    }

                }
            });
        });
        log.info("========finish checkStreetQualification=======");
    }

    @Autowired
    private MapStreetExtService mapStreetExtService;

    /**
     * 道路额外属性初始化
     */
    public void initStreetExt() {
        List<MapStreet> list = mapStreetService.list();
        list.stream().forEach(mapStreet -> {
            QueryWrapper<MapStreetExt> mapStreetExtQueryWrapper = new QueryWrapper<>();
            mapStreetExtQueryWrapper.eq("uuid", mapStreet.getUuid());
            String streetType = mapStreet.getStreetType();
            MapStreetExt one = mapStreetExtService.getOne(mapStreetExtQueryWrapper, false);
            if (one == null) {
                MapStreetExt mapStreetExt = new MapStreetExt();
                mapStreetExt.setUuid(mapStreet.getUuid());
                mapStreetExt.setArrangement("两边对称");
                mapStreetExt.setInstallationHeight("12m");
                mapStreetExt.setMaterials("沥青路面");
                mapStreetExt.setLightType("LED");
                mapStreetExt.setLightPower("150W");
                mapStreetExt.setCorrectionCoefficient("0.93");
                mapStreetExt.setPoleType("铸铁镀锌");
                mapStreetExt.setRoadCoefficient("0.066");
                switch (streetType) {
                    case "主干路":
                    case "快速路":
                        mapStreetExt.setLanesNum("8");
                        mapStreetExt.setPoleSpacing("40m");
                        break;
                    case "次干路":
                        mapStreetExt.setLanesNum("4");
                        mapStreetExt.setPoleSpacing("30m");
                        break;
                    case "支路":
                        mapStreetExt.setLanesNum("2");
                        mapStreetExt.setPoleSpacing("30m");
                        break;
                }
                mapStreetExtService.save(mapStreetExt);
                log.info("======新增一条道路额外属性====");
            }

        });

    }

    /**
     * 测试kafka 上传点位信息
     */
    @Autowired
    private KafkaProducer kafkaProducer;

    public void kafkaUploadPoint() {
        String message = "{\n" + "  \"Region\": null,\n" + "  \"Roadname\": null,\n" + "  \"Roadlevel\": null,\n" + "  \"lightdistribution\": null,\n" + "  \"lighttype\": null,\n" + "  \"lightAverage\": \"0.11\",\n" + "  \"lightuniformity\": \"0.64\",\n" + "  \"Averagebrightness\": \"0.0\",\n" + "  \"Brightnessuniformity\": \"nan\",\n" + "  \"max\": \"0.22\",\n" + "  \"min\": \"0.07\",\n" + "  \"Longitudinal\": \"0.32\",\n" + "  \"lat\": \"30.722050\",\n" + "  \"lon\": \"103.986464\",\n" + "  \"time\": \"1701014376\",\n" + "  \"speed\": \"39.86\",\n" + "  \"Compliance\": null,\n" + "  \"result\": null,\n" + "  \"dataCuuid\": null,\n" + "  \"isDataOk\": true\n" + "}";
        kafkaProducer.sendMessage(message);

    }

    /**
     * 过滤掉有备注不能检测的道路
     */
    public void changeStreetStatus() {
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        mapStreetQueryWrapper.ne("street_decs", "");
        List<MapStreet> streetList = mapStreetService.list(mapStreetQueryWrapper);
    }

    /**
     * 解析离线点位文件，补全采集数据信息
     */
    public void offlineAnalysisPointData() {
        String dataPath = "C:\\Users\\wujing\\Desktop\\data";
        List<File> childDirectories = new ArrayList<>();
        FileUtil.getFilesDirectoryPath(dataPath, childDirectories);
        childDirectories.stream().forEach(file -> {
            String name = file.getName();
            log.info("data:{}/{}", name, file.getPath());
            File fileByName = FileUtil.getFileByName(file.getPath(), "data.txt");
            try {
                String contentData = FileUtil.readFileToString(fileByName.getPath());
                log.info("contentData:{}", contentData);
                String[] datas = contentData.split("\\n");
                Arrays.stream(datas).forEach(data -> {
                    KafkaMessage kafkaMessage = new Gson().fromJson(data, KafkaMessage.class);
                    kafkaMessage.setRoadMCode(name);
                    kafkaProducer.sendMessage(new Gson().toJson(kafkaMessage));
                    //计算
                    log.info("------");
                });
            } catch (IOException e) {
                e.printStackTrace();
                log.error("文件读取失败");
            }
        });
    }

    /**
     * 更新监测点道路监测编号信息
     */
    public void updatePointRoadCode() {
        String dataPath = "D:\\第二次测试\\1129";
        List<File> childDirectories = new ArrayList<>();
        FileUtil.getFilesDirectoryPath(dataPath, childDirectories);
        List<KafkaMessage> kafkaMessages = new ArrayList<>();
        childDirectories.stream().forEach(file -> {
            String name = file.getName();
            log.info("data:{}/{}", name, file.getPath());
            File fileByName = FileUtil.getFileByName(file.getPath(), "data.txt");
            try {
                String contentData = FileUtil.readFileToString(fileByName.getPath());
                //log.info("contentData:{}", contentData);
                String[] datas = contentData.split("\\n");
                Arrays.stream(datas).forEach(data -> {
                    KafkaMessage kafkaMessage = new Gson().fromJson(data, KafkaMessage.class);
                    kafkaMessage.setRoadMCode(name);
                    kafkaMessages.add(kafkaMessage);
                });
            } catch (IOException e) {
                e.printStackTrace();
                log.error("文件读取失败");
            }
        });
        List<Future> futures = new ArrayList<>();
        //遍历查询更新点位道路监测编号信息
        kafkaMessages.stream().forEach(kafkaMessage -> {
            Future<?> submit = executor.submit(new Runnable() {
                @Override
                public void run() {
                    supplement(kafkaMessage);
                }
            });
            futures.add(submit);
        });
        int allCount = futures.size();
        int progress = 0;
        for (Future f : futures) {
            try {
                f.get();
                progress++;
                log.info("=========进度：{}/{}", progress, allCount);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 有效监测点，分表存储
     */
    public void dealMapPointResultData() {
        //查询有效监测点，且具有道路属性的
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        mapPointQueryWrapper.eq("isDataOk", 1);
        mapPointQueryWrapper.isNotNull("street_uuid");
        List<MapPoint> mapPoints = mapPointService.list(mapPointQueryWrapper);
        List<Future> futures = new ArrayList<>();
        mapPoints.stream().forEach(mapPoint -> {
            Future<?> submit = executor.submit(new Runnable() {
                @Override
                public void run() {
                    //判断监测点，是否已入库结果表
                    QueryWrapper<MapPointResult> mapPointResultQueryWrapper = new QueryWrapper<>();
                    mapPointResultQueryWrapper.eq("lat", mapPoint.getLat());
                    mapPointResultQueryWrapper.eq("lon", mapPoint.getLon());
                    mapPointResultQueryWrapper.eq("time", mapPoint.getTime());
                    MapPointResult one = mapPointResultService.getOne(mapPointResultQueryWrapper, false);
                    if (one == null) {
                        //未入库，新增数据入库结果表
                        MapPointResult mapPointResult = new MapPointResult();
                        BeanUtils.copyProperties(mapPoint, mapPointResult);
                        mapPointResultService.save(mapPointResult);
                    }
                }
            });
            futures.add(submit);
        });
        int allCount = futures.size();
        int progress = 0;
        for (Future f : futures) {
            try {
                f.get();
                progress++;
                log.info("=========进度：{}/{}", progress, allCount);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Autowired
    private MapPointImageService mapPointImageService;
    @Resource
    private RemoteFileService remoteFileService;

    /**
     * 离线上传监测点位图片
     */
    public void offlineUploadPointImage() {
        List<Future> futures = new ArrayList<>();
        String dataPath = "C:\\Users\\wujing\\Desktop\\changephoto";
        List<File> childDirectories = new ArrayList<>();
        FileUtil.getFilesDirectoryPath(dataPath, childDirectories);
        childDirectories.stream().forEach(file -> {
            String name = file.getName();
            log.info("data:{}/{}", name, file.getPath());
            List<File> files = new ArrayList<>();
            FileUtil.getFilesByLikeName(file.getPath(), ".jpg", files);
            files.stream().forEach(file1 -> {
                String name1 = file1.getName();
                String split = name1.replace(".jpg", "");
                Long time = Long.valueOf(split);
                Future<?> submit = executor.submit(new Runnable() {
                    @Override
                    public void run() {
                        QueryWrapper<MapPointImage> queryWrapper = new QueryWrapper<>();
                        queryWrapper.eq("time", time);
                        MapPointImage one = mapPointImageService.getOne(queryWrapper, false);
                        if (one == null) {
                            MultipartFile multipartFile = FileUtil.getMultipartFile(file1);
                            R<SysFile> upload = remoteFileService.upload(multipartFile);
                            if (upload != null) {
                                int code = upload.getCode();
                                if (code == Constants.SUCCESS) {
                                    SysFile sysFile = upload.getData();
                                    MapPointImage mapPointImage = new MapPointImage();
                                    mapPointImage.setTime(time);
                                    mapPointImage.setUrl(sysFile.getUrl());
                                    mapPointImageService.save(mapPointImage);
                                }
                            }
                        } else {
                            //更新图片
                            MultipartFile multipartFile = FileUtil.getMultipartFile(file1);
                            R<SysFile> upload = remoteFileService.upload(multipartFile);
                            if (upload != null) {
                                int code = upload.getCode();
                                if (code == Constants.SUCCESS) {
                                    SysFile sysFile = upload.getData();
                                    one.setUrl(sysFile.getUrl());
                                    mapPointImageService.updateById(one);
                                }
                            }
                        }
                    }
                });
                futures.add(submit);
            });
        });
        int allCount = futures.size();
        int progress = 0;
        for (Future f : futures) {
            try {
                f.get();
                progress++;
                log.info("=========进度：{}/{}", progress, allCount);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 补传校验监测点信息
     *
     * @param kafkaMessage
     */
    public void supplement(KafkaMessage kafkaMessage) {
        //判断监测点是否已经入库过
        String latStr = kafkaMessage.getLat().trim();
        String lonStr = kafkaMessage.getLon().trim();
        String timeStr = kafkaMessage.getTime().trim();
        BigDecimal lat = new BigDecimal(latStr);
        BigDecimal lon = new BigDecimal(lonStr);
        Long time = Long.valueOf(timeStr.trim());
        QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
        mapPointQueryWrapper.eq("lat", lat);
        mapPointQueryWrapper.eq("lon", lon);
        mapPointQueryWrapper.eq("time", time);
        MapPoint one = mapPointService.getOne(mapPointQueryWrapper, false);
        if (one == null) {
            //未入库过，新增监测点
            //查询属于那条街道
            List<String> uuids = new ArrayList<>();
            String point = "POINT(" + lonStr + " " + latStr + ")";
            //相交
            String streetByPoint = MapApi.getInstance().getStreetByPoint(point, geoserverConfig, restTemplate);
            if (streetByPoint != null) {
                MapFeatureResult mapFeatureResult = new Gson().fromJson(streetByPoint, MapFeatureResult.class);
                if (mapFeatureResult != null) {
                    List<MapFeatureResult.FeaturesDTO> features = mapFeatureResult.getFeatures();
                    if (!Collections.isEmpty(features)) {
                        features.stream().forEach(feature -> {
                            MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                            if (properties != null) {
                                String uuid = properties.getUuid();
                                if (StringUtils.isNotEmpty(uuid)) {
                                    uuids.add(uuid);
                                }
                            }
                        });
                    } else {
                        //包含
                        String streetByPointContains = MapApi.getInstance().getStreetByPointContains(point, geoserverConfig, restTemplate);
                        if (streetByPointContains != null) {
                            MapFeatureResult mapFeatureResultContains = new Gson().fromJson(streetByPointContains, MapFeatureResult.class);
                            if (mapFeatureResultContains != null) {
                                List<MapFeatureResult.FeaturesDTO> featuresContains = mapFeatureResultContains.getFeatures();
                                if (!Collections.isEmpty(featuresContains)) {
                                    featuresContains.stream().forEach(feature -> {
                                        MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                        if (properties != null) {
                                            String uuid = properties.getUuid();
                                            if (StringUtils.isNotEmpty(uuid)) {
                                                uuids.add(uuid);
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
            }
            MapPoint mapPoint = new MapPoint();
            mapPoint.setLon(new BigDecimal(kafkaMessage.getLon().trim()));
            mapPoint.setLat(new BigDecimal(kafkaMessage.getLat().trim()));
            mapPoint.setTime(time);
            String region = kafkaMessage.getRegion();
            if (StringUtils.isNotEmpty(region)) {
                mapPoint.setRegion(region.trim());
            }
            String roadname = kafkaMessage.getRoadname();
            if (StringUtils.isNotEmpty(roadname)) {
                mapPoint.setRoadname(roadname.trim());
            }
            String roadlevel = kafkaMessage.getRoadlevel();
            if (StringUtils.isNotEmpty(roadlevel)) {
                mapPoint.setRoadlevel(roadlevel.trim());
            }
            String lightdistribution = kafkaMessage.getLightdistribution();
            if (StringUtils.isNotEmpty(lightdistribution)) {
                mapPoint.setLightdistribution(lightdistribution.trim());
            }
            String lighttype = kafkaMessage.getLighttype();
            if (StringUtils.isNotEmpty(lighttype)) {
                mapPoint.setLighttype(lighttype.trim());
            }
            boolean dataOk = kafkaMessage.isDataOk();
            if (dataOk) {
                mapPoint.setIsDataOk(1);
            } else {
                mapPoint.setIsDataOk(0);
            }
            String dataCuuid = kafkaMessage.getDataCuuid();
            if (StringUtils.isNotEmpty(dataCuuid)) {
                mapPoint.setDataCuuid(dataCuuid.trim());
            }
            if (StringUtils.isNotEmpty(kafkaMessage.getLightAverage().trim())) {
                mapPoint.setLightAverage(new BigDecimal(kafkaMessage.getLightAverage().trim()));
            }
            if (StringUtils.isNotEmpty(kafkaMessage.getLightuniformity().trim())) {
                mapPoint.setLightuniformity(new BigDecimal(kafkaMessage.getLightuniformity().trim()));
            }
            mapPoint.setAveragebrightness(new BigDecimal(kafkaMessage.getAveragebrightness().trim()));
            if ("nan".equals(kafkaMessage.getBrightnessuniformity().trim())) {
                mapPoint.setBrightnessuniformity(new BigDecimal(0));
            } else {
                mapPoint.setBrightnessuniformity(new BigDecimal(kafkaMessage.getBrightnessuniformity().trim()));
            }
            mapPoint.setMax(new BigDecimal(kafkaMessage.getMax().trim()));
            mapPoint.setMin(new BigDecimal(kafkaMessage.getMin().trim()));
            mapPoint.setLongitudinal(new BigDecimal(kafkaMessage.getLongitudinal().trim()));
            mapPoint.setSpeed(new BigDecimal(kafkaMessage.getSpeed().trim()));
            String compliance = kafkaMessage.getCompliance();
            if (StringUtils.isNotEmpty(compliance)) {
                mapPoint.setCompliance(compliance.trim());
            }
            String result = kafkaMessage.getResult();
            if (StringUtils.isNotEmpty(result)) {
                mapPoint.setResult(result.trim());
            }
            String roadMCode = kafkaMessage.getRoadMCode();
            if (StringUtils.isNotEmpty(roadMCode)) {
                mapPoint.setRoadMCode(roadMCode);
            }
            if (!Collections.isEmpty(uuids)) {
                uuids.stream().forEach(uuid -> {
                    mapPoint.setStreetUuid(uuid);
                    //判断监测点合格性
                    checkPointQualification(mapPoint, uuid);
                    mapPointService.save(mapPoint);
                    if (mapPoint.getIsDataOk() == 1) {
                        //有效数据保存到有效数据结果表
                        MapPointResult mapPointResult = new MapPointResult();
                        BeanUtils.copyProperties(mapPoint, mapPointResult);
                        mapPointResult.setId(null);
                        mapPointResultService.save(mapPointResult);
                    }
                    log.info("------ insert mappoint ok -------");
                });
            } else {
                //未查询到道路属性且为有效数据监测点
                if (mapPoint.getIsDataOk() == 1) {
                    String roadCodeP = mapPoint.getRoadMCode();
                    QueryWrapper<MapPointResult> mapPointResultQueryWrapper = new QueryWrapper<>();
                    mapPointResultQueryWrapper.select("count(1) as c", "street_uuid").groupBy("street_uuid").orderByDesc("c").eq("roadMCode", roadCodeP);
                    List<Map<String, Object>> maps = mapPointResultService.getBaseMapper().selectMaps(mapPointResultQueryWrapper);
                    if (!Collections.isEmpty(maps)) {
                        Map<String, Object> stringObjectMap = maps.get(0);
                        String streetUUid = (String) stringObjectMap.get("street_uuid");
                        //判断合格性
                        checkPointQualification(mapPoint, streetUUid);
                        mapPoint.setStreetUuid(streetUUid);
                        //保存原始数据表
                        mapPointService.save(mapPoint);
                        MapPointResult mapPointResult = new MapPointResult();
                        BeanUtils.copyProperties(mapPoint, mapPointResult);
                        mapPointResult.setId(null);
                        mapPointResult.setStreetUuid(streetUUid);
                        //存入有效结果数据表
                        mapPointResultService.save(mapPointResult);
                        log.info("------ insert mappoint ok -------");
                    }
                }
            }
        } else {
            //入库过，更新监测点
            String roadMCode = kafkaMessage.getRoadMCode();
            one.setRoadMCode(roadMCode);
            mapPointService.updateById(one);
            log.info("===== update point roadMCode ====== ");
        }
    }


    /**
     * 更新道路监测状态属性
     */
    public void updateRoadFeatureProperty() {
        String streetMap = MapApi.getInstance().getStreetMap(geoserverConfig, restTemplate);
        log.info(streetMap);
        RoadVectorData roadVectorData = new Gson().fromJson(streetMap, RoadVectorData.class);
        List<RoadVectorData.FeaturesDTO> features = roadVectorData.getFeatures();
        List<MapStreetResult> mapStreetResults = mapStreetResultService.list();
        int count = 0;
        if (!Collections.isEmpty(features) && !Collections.isEmpty(mapStreetResults)) {
            //不支持并发
            for (RoadVectorData.FeaturesDTO feature : features) {
                if ("未检测".equals(feature.getProperties().getDecs())) {
                    for (MapStreetResult mapStreetResult : mapStreetResults) {
                        if (mapStreetResult.getUuid().equals(feature.getProperties().getUuid())) {
                            String featuresDTOId = feature.getId();
                            String transactionRequest = GeoserverUtils.buildTransactionWFSUpdateRequest(geoserverConfig.getWorkspace(), geoserverConfig.getLayer(), "decs", mapStreetResult.getLightResult(), featuresDTOId);
                            String wfsResult = MapApi.getInstance().geoserverWFSUpdateProperty(transactionRequest, geoserverConfig, restTemplate);
                            count++;
                            log.info("===== 更新一条路监测状态 ======{}", count);
                        }
                    }
                }
            }
        }
    }

    /**
     * 初始化不能正常检测的道路图层属性
     */
    public void updateRoadFeaturePropertyCanNotCheck() {
        String streetMap = MapApi.getInstance().getStreetMap(geoserverConfig, restTemplate);
        log.info(streetMap);
        RoadVectorData roadVectorData = new Gson().fromJson(streetMap, RoadVectorData.class);
        List<RoadVectorData.FeaturesDTO> features = roadVectorData.getFeatures();
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        mapStreetQueryWrapper.ne("street_decs", "");
        List<MapStreet> mapStreets = mapStreetService.list(mapStreetQueryWrapper);
        int count = 0;
        if (!Collections.isEmpty(features) && !Collections.isEmpty(mapStreets)) {
            //不支持并发
            for (MapStreet mapStreet:mapStreets) {
                for (RoadVectorData.FeaturesDTO feature : features) {
                    if(mapStreet.getUuid().equals(feature.getProperties().getUuid())){
                        String featuresDTOId = feature.getId();
                        String transactionRequest = GeoserverUtils.buildTransactionWFSUpdateRequest(geoserverConfig.getWorkspace(), geoserverConfig.getLayer(), "decs", "不能正常检测", featuresDTOId);
                        String wfsResult = MapApi.getInstance().geoserverWFSUpdateProperty(transactionRequest, geoserverConfig, restTemplate);
                        count++;
                        log.info("===== 更新一条路监测状态 ======{}", count);
                        break;
                    }
                }
            }
            for (RoadVectorData.FeaturesDTO feature : features) {
                if ("未检测".equals(feature.getProperties().getDecs())) {
                    for (MapStreet mapStreetResult : mapStreets) {
                        if (mapStreetResult.getUuid().equals(feature.getProperties().getUuid())) {

                        }
                    }
                }
            }
        }
    }

    /**
     * 测试验证
     */
    public void test() {
        List<MapStreetResult> list = mapStreetResultService.list();
        List<String> uuids = new ArrayList<>();
        list.stream().forEach(item -> {
            uuids.add(item.getUuid());
        });
        uuids.stream().forEach(uuid -> {
            QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
            mapStreetQueryWrapper.eq("uuid", uuid);
            MapStreet one = mapStreetService.getOne(mapStreetQueryWrapper);
            if (one == null) {
                log.info(uuid);
            }
        });
    }

    /**
     * 整合第一次测量数据和第二次测量数据
     * 使用用第二次测量有效数据，剩余的使用第一次测量的数据
     */
    public void integrateTheFirstAndSecondTimeData() {
        List<MapStreetResult> list = mapStreetResultService.list();
        List<String> uuids = new ArrayList<>();
        list.stream().forEach(s -> {
            uuids.add(s.getUuid());
        });
        //查询第二次还未检测的道路数据
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        mapStreetQueryWrapper.notIn("uuid", uuids);
        List<MapStreet> mapStreets = mapStreetService.list(mapStreetQueryWrapper);
        log.info("mapStreet size:{}", mapStreets.size());
        mapStreets.stream().forEach(s -> {
            //根据道路uuid查询第一次有效监测点位数据
            String uuid = s.getUuid();
            QueryWrapper<MapPoint> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("street_uuid", uuid);
            queryWrapper.eq("isDataOk", 1);
            List<MapPoint> mapPoints = mapPointService.listSlave(queryWrapper);
            mapPoints.stream().forEach(p -> {
                p.setRoadMCode("first-" + System.currentTimeMillis() / 1000);
            });
            mapPointService.saveBatch(mapPoints);
        });
    }

    /**
     * 插入第二次测量的有效数据
     */
    public void insertSecondPointData() {
        QueryWrapper<MapPoint> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("roadMCode", "mangce");
        queryWrapper.eq("isDataOk", 1);
        List<MapPoint> mapPoints = mapPointService.listSlave(queryWrapper);
        mapPointService.saveBatch(mapPoints);
    }

    /**
     * 插入一期800条的有效数据
     */
    public void insertYiqi800PointData() {
        QueryWrapper<MapPoint> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("roadMCode", "yiqi800tiao");
        queryWrapper.eq("isDataOk", 1);
        List<MapPoint> mapPoints = mapPointService.listSlave(queryWrapper);
        mapPointService.saveBatch(mapPoints);
    }


    public static void main(String[] args) {
        //offlineUploadPointImage();
    }

}
