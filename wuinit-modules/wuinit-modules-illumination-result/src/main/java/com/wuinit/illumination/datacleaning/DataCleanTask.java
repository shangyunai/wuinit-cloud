package com.wuinit.illumination.datacleaning;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuinit.common.core.utils.DateUtils;
import com.wuinit.illumination.domain.entity.MapPoint;
import com.wuinit.illumination.domain.entity.MapStreet;
import com.wuinit.illumination.domain.entity.MapStreetResult;
import com.wuinit.illumination.service.MapPointService;
import com.wuinit.illumination.service.MapStreetResultService;
import com.wuinit.illumination.service.MapStreetService;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * 定时清洗数据
 */
@Component
@Slf4j
public class DataCleanTask {
    @Autowired
    private DataCleaningUtil dataCleaningUtil;
    @Autowired
    private MapStreetService mapStreetService;
    @Autowired
    private MapPointService mapPointService;
    @Autowired
    private MapStreetResultService mapStreetResultService;
    @Resource(name = "threadPoolProcessorTaskExecutor")
    private ThreadPoolTaskExecutor executor;

    /**
     * 测试
     * #每天凌晨4点执行一次
     */
    @Scheduled(cron = "0 0 4 * * ?")
    //@Scheduled(cron = "0 */1 * * * ?")
    public void runDataClean() {
        log.info("========执行定时清洗数据，计算道路监测结果任务========");
        new Thread(new Runnable() {
            @Override
            public void run() {
                dataClean();
            }
        }).start();
    }

    private void dataClean() {
        //1.判断道路合格性
        checkStreetQualification();
        //2.更新地图道路监测状态
        dataCleaningUtil.updateRoadFeatureProperty();
    }

    private void checkStreetQualification() {
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        //排除一期800条道路
        //mapStreetQueryWrapper.ne("region", "一期800条道路");
        List<MapStreet> streets = mapStreetService.list(mapStreetQueryWrapper);
        List<Future> futures = new ArrayList<>();
        streets.stream().forEach(street -> {
            String uuid = street.getUuid();
            //判断之前是否已有该条路检测结果
            QueryWrapper<MapStreetResult> mapStreetResultQueryWrapper = new QueryWrapper<>();
            mapStreetResultQueryWrapper.eq("uuid", uuid);
            mapStreetResultQueryWrapper.eq("check_time", 1);
            MapStreetResult one = mapStreetResultService.getOne(mapStreetResultQueryWrapper, false);
            if (one == null) {
                //该条道路无检测结果
                Future<?> submit = executor.submit(new Runnable() {
                    @Override
                    public void run() {
                        //计算该条道路是否监测
                        MapPoint mapPointQuery = new MapPoint();
                        mapPointQuery.setStreetUuid(uuid);
                        List<List<MapPoint>> pointByUuid = mapPointService.getPointByUuid(mapPointQuery);
                        if (!Collections.isEmpty(pointByUuid)) {
                            List<MapPoint> mapPoints = pointByUuid.get(0);
                            if (!Collections.isEmpty(mapPoints)) {
                                //平均照度
                                List<Double> lightAList = new ArrayList<>();
                                //平均照度均匀度
                                List<Double> lightUList = new ArrayList<>();
                                //平均亮度
                                List<Double> brightAList = new ArrayList<>();
                                //平均亮度均匀度
                                List<Double> brightUList = new ArrayList<>();
                                //平均车速
                                List<Double> speedAList = new ArrayList<>();
                                //照度达标数量
                                List<String> qualificationOk = new ArrayList<>();
                                mapPoints.stream().forEach(mapPoint -> {
                                    lightAList.add(mapPoint.getLightAverage().doubleValue());
                                    lightUList.add(mapPoint.getLightuniformity().doubleValue());
                                    brightAList.add(mapPoint.getAveragebrightness().doubleValue());
                                    brightUList.add(mapPoint.getBrightnessuniformity().doubleValue());
                                    BigDecimal speed = mapPoint.getSpeed();
                                    if (speed != null) {
                                        speedAList.add(speed.doubleValue());
                                    }
                                    if ("合格".equals(mapPoint.getCompliance())) {
                                        qualificationOk.add(mapPoint.getCompliance());
                                    }
                                });
                                //得到平均数 照度
                                double lightAverage = lightAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //最大值 照度
                                double lightMax = lightAList.stream().mapToDouble(Double::valueOf).max().getAsDouble();
                                //最小值 照度
                                double lightMin = lightAList.stream().mapToDouble(Double::valueOf).min().getAsDouble();
                                //得到平均数 照度均匀度
                                double lightUAverage = lightUList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //得到平均数 照度
                                double brightAverage = brightAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //最大值 照度
                                double brightMax = brightAList.stream().mapToDouble(Double::valueOf).max().getAsDouble();
                                //最小值 照度
                                double brightMin = brightAList.stream().mapToDouble(Double::valueOf).min().getAsDouble();
                                //得到平均数 照度均匀度
                                double brightUAverage = brightUList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //得到平均数 照度均匀度
                                double speedAverage = speedAList.stream().mapToDouble(Double::valueOf).average().getAsDouble();
                                //合格率占比
                                BigDecimal rate = new BigDecimal(qualificationOk.size()).divide(new BigDecimal(mapPoints.size()), 4, RoundingMode.DOWN).setScale(4, RoundingMode.DOWN);
                                MapStreetResult mapStreetResult = new MapStreetResult();
                                mapStreetResult.setTime(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, new Date(mapPoints.get(mapPoints.size() - 1).getTime() * 1000)));
                                //todo 设置检测人员
                                mapStreetResult.setPerson("王*楚");
                                mapStreetResult.setCheckTime(1);
                                mapStreetResult.setUuid(uuid);
                                mapStreetResult.setLightAverage(new BigDecimal(lightAverage));
                                mapStreetResult.setMaxLight(new BigDecimal(lightMax));
                                mapStreetResult.setMinLight(new BigDecimal(lightMin));
                                mapStreetResult.setLightUniformity(new BigDecimal(lightUAverage));
                                mapStreetResult.setAverageBrightness(new BigDecimal(brightAverage));
                                mapStreetResult.setMaxBright(new BigDecimal(brightMax));
                                mapStreetResult.setMinBright(new BigDecimal(brightMin));
                                mapStreetResult.setBrightnessUniformity(new BigDecimal(brightUAverage));
                                mapStreetResult.setAverageSpeed(new BigDecimal(speedAverage));
                                mapStreetResult.setRate(rate);
                                mapStreetResult.setStartLon(mapPoints.get(0).getLon());
                                mapStreetResult.setStartLat(mapPoints.get(0).getLat());
                                mapStreetResult.setEndLon(mapPoints.get(mapPoints.size() - 1).getLon());
                                mapStreetResult.setEndLat(mapPoints.get(mapPoints.size() - 1).getLat());
                                mapStreetResult.setName(street.getStreetName());
                                String streetType = street.getStreetType();
                                BigDecimal lightAverageResult = mapStreetResult.getLightAverage();
                                switch (streetType) {
                                    case "主干路":
                                    case "快速路":
                                        if (lightAverageResult.compareTo(new BigDecimal(30)) >= 0) {
                                            //合格
                                            mapStreetResult.setLightResult("合格");
                                        } else {
                                            //偏低
                                            mapStreetResult.setLightResult("偏低");
                                        }
                                        break;
                                    case "次干路":
                                        if (lightAverageResult.compareTo(new BigDecimal(20)) >= 0) {
                                            //合格
                                            mapStreetResult.setLightResult("合格");
                                        } else {
                                            //偏低
                                            mapStreetResult.setLightResult("偏低");
                                        }
                                        break;
                                    case "支路":
                                        if (lightAverageResult.compareTo(new BigDecimal(10)) >= 0) {
                                            //合格
                                            mapStreetResult.setLightResult("合格");
                                        } else {
                                            //偏低
                                            mapStreetResult.setLightResult("偏低");
                                        }
                                        break;
                                }
                                mapStreetResultService.save(mapStreetResult);
                                log.info("==============新增一条道路检测结果==============");
                            }
                        }
                    }
                });
                futures.add(submit);
            }
        });
        int allCount = futures.size();
        int progress = 0;
        for (Future f : futures) {
            try {
                f.get();
                progress++;
                log.info("=========进度：{}/{}", progress, allCount);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

}
