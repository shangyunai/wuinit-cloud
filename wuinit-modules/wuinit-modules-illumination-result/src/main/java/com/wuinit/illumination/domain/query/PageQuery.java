package com.wuinit.illumination.domain.query;

import lombok.Data;

@Data
public class PageQuery {
    /**
     * 当前记录起始索引
     */
    private Integer pageNum;
    /**
     * 每页显示记录数
     */
    private Integer pageSize;
}
