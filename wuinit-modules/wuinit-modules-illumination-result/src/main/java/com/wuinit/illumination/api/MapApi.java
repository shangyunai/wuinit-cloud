package com.wuinit.illumination.api;

import com.wuinit.illumination.config.GeoserverConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;

public class MapApi {
    private final static Logger logger = LoggerFactory.getLogger(MapApi.class);
    private final static String SERVICE_IP = "http://192.168.1.205:8002";
    private final static String GEOSERVER_WFS = "/geoserver/wfs";

    private static class SingletonClassInstance {
        private static final MapApi instance = new MapApi();
    }

    private MapApi() {

    }

    public static MapApi getInstance() {
        return SingletonClassInstance.instance;
    }

    private HttpHeaders getTextXmlHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-Type", "text/xml;charset=UTF-8;");
        return httpHeaders;
    }

    /**
     * geoserver 矢量图层增、删、改
     *
     * @param textXmlStr
     * @param geoserverConfig
     * @param restTemplate
     * @return
     */
    public String geoserverWFS(String textXmlStr, GeoserverConfig geoserverConfig, RestTemplate restTemplate) {
        try {
            //base64解密
            byte[] decode = Base64.getDecoder().decode(textXmlStr);
            String textXml = new String(decode, "UTF-8");
            HttpEntity<String> httpEntity = new HttpEntity<>(textXml, getTextXmlHeaders());
            String result = restTemplate.postForObject(geoserverConfig.getWfsUrl() + GEOSERVER_WFS, httpEntity, String.class);
            logger.debug("geoserverWFS result: {}", result);
            return result;
        } catch (Exception e) {
            logger.error("geoserverWFS error: {}", e.toString());
            e.printStackTrace();
            return "";
        }

    }

    /**
     * geoserver 矢量图层更新自定义属性
     *
     * @param textXmlStr
     * @param geoserverConfig
     * @param restTemplate
     * @return
     */
    public String geoserverWFSUpdateProperty(String textXmlStr, GeoserverConfig geoserverConfig, RestTemplate restTemplate) {
        try {
            //base64解密
            HttpEntity<String> httpEntity = new HttpEntity<>(textXmlStr, getTextXmlHeaders());
            String result = restTemplate.postForObject(geoserverConfig.getWfsUrl() + GEOSERVER_WFS, httpEntity, String.class);
            logger.debug("geoserverWFSUpdateProperty result: {}", result);
            return result;
        } catch (Exception e) {
            logger.error("geoserverWFSUpdateProperty error: {}", e.toString());
            e.printStackTrace();
            return "";
        }

    }

    /**
     * 获取geoserver 对应图层矢量数据
     *
     * @param geoserverConfig
     * @param restTemplate
     * @return
     */
    public String getStreetMap(GeoserverConfig geoserverConfig, RestTemplate restTemplate) {
        try {
            String result = restTemplate.getForObject(geoserverConfig.getStreetUrl(), String.class);
            logger.debug("getStreetMap result: {}", result);
            return result;
        } catch (Exception e) {
            logger.error("getStreetMap error: {}", e.toString());
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 根据点 查询与其相交的道路
     *
     * @param pointStr
     * @param geoserverConfig
     * @param restTemplate
     * @return
     */
    public String getStreetByPoint(String pointStr, GeoserverConfig geoserverConfig, RestTemplate restTemplate) {
        try {
            String result = restTemplate.getForObject(geoserverConfig.getStreetUrl() + "&cql_filter=INTERSECTS(the_geom,SRID=4490;" + pointStr + ")", String.class);
            logger.debug("getStreetByPoint result: {}", result);
            return result;
        } catch (Exception e) {
            logger.error("getStreetByPoint error: {}", e.toString());
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 根据点 查询包含该点的道路
     *
     * @param pointStr
     * @param geoserverConfig
     * @param restTemplate
     * @return
     */
    public String getStreetByPointContains(String pointStr, GeoserverConfig geoserverConfig, RestTemplate restTemplate) {
        try {
            String result = restTemplate.getForObject(geoserverConfig.getStreetUrl() + "&cql_filter=CONTAINS(the_geom,SRID=4490;" + pointStr + ")", String.class);
            logger.debug("getStreetByPoint result: {}", result);
            return result;
        } catch (Exception e) {
            logger.error("getStreetByPoint error: {}", e.toString());
            e.printStackTrace();
            return "";
        }
    }
}
