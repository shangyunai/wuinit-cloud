package com.wuinit.illumination.domain.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RoadVectorData {

    @JsonProperty("type")
    private String type;
    @JsonProperty("totalFeatures")
    private Integer totalFeatures;
    @JsonProperty("numberMatched")
    private Integer numberMatched;
    @JsonProperty("numberReturned")
    private Integer numberReturned;
    @JsonProperty("timeStamp")
    private String timeStamp;
    @JsonProperty("crs")
    private CrsDTO crs;
    @JsonProperty("features")
    private List<FeaturesDTO> features;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getTotalFeatures() {
        return totalFeatures;
    }

    public void setTotalFeatures(Integer totalFeatures) {
        this.totalFeatures = totalFeatures;
    }

    public Integer getNumberMatched() {
        return numberMatched;
    }

    public void setNumberMatched(Integer numberMatched) {
        this.numberMatched = numberMatched;
    }

    public Integer getNumberReturned() {
        return numberReturned;
    }

    public void setNumberReturned(Integer numberReturned) {
        this.numberReturned = numberReturned;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public CrsDTO getCrs() {
        return crs;
    }

    public void setCrs(CrsDTO crs) {
        this.crs = crs;
    }

    public List<FeaturesDTO> getFeatures() {
        return features;
    }

    public void setFeatures(List<FeaturesDTO> features) {
        this.features = features;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CrsDTO {
        @JsonProperty("type")
        private String type;
        @JsonProperty("properties")
        private PropertiesDTO properties;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public PropertiesDTO getProperties() {
            return properties;
        }

        public void setProperties(PropertiesDTO properties) {
            this.properties = properties;
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class PropertiesDTO {
            @JsonProperty("name")
            private String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class FeaturesDTO {
        @JsonProperty("type")
        private String type;
        @JsonProperty("id")
        private String id;
        @JsonProperty("geometry")
        private GeometryDTO geometry;
        @JsonProperty("geometry_name")
        private String geometryName;
        @JsonProperty("properties")
        private PropertiesDTOX properties;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public GeometryDTO getGeometry() {
            return geometry;
        }

        public void setGeometry(GeometryDTO geometry) {
            this.geometry = geometry;
        }

        public String getGeometryName() {
            return geometryName;
        }

        public void setGeometryName(String geometryName) {
            this.geometryName = geometryName;
        }

        public PropertiesDTOX getProperties() {
            return properties;
        }

        public void setProperties(PropertiesDTOX properties) {
            this.properties = properties;
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class GeometryDTO {
            @JsonProperty("type")
            private String type;
            @JsonProperty("coordinates")
            private List<List<List<List<Double>>>> coordinates;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public List<List<List<List<Double>>>> getCoordinates() {
                return coordinates;
            }

            public void setCoordinates(List<List<List<List<Double>>>> coordinates) {
                this.coordinates = coordinates;
            }
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class PropertiesDTOX {
            @JsonProperty("uuid")
            private String uuid;
            @JsonProperty("province")
            private String province;
            @JsonProperty("city")
            private String city;
            @JsonProperty("region")
            private String region;
            @JsonProperty("name")
            private String name;
            @JsonProperty("level")
            private Object level;
            @JsonProperty("decs")
            private String decs;
            @JsonProperty("type")
            private String type;
            @JsonProperty("center")
            private String center;
            @JsonProperty("start")
            private String start;
            @JsonProperty("end")
            private String end;

            public String getUuid() {
                return uuid;
            }

            public void setUuid(String uuid) {
                this.uuid = uuid;
            }

            public String getProvince() {
                return province;
            }

            public void setProvince(String province) {
                this.province = province;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getRegion() {
                return region;
            }

            public void setRegion(String region) {
                this.region = region;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getLevel() {
                return level;
            }

            public void setLevel(Object level) {
                this.level = level;
            }

            public String getDecs() {
                return decs;
            }

            public void setDecs(String decs) {
                this.decs = decs;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getCenter() {
                return center;
            }

            public void setCenter(String center) {
                this.center = center;
            }

            public String getStart() {
                return start;
            }

            public void setStart(String start) {
                this.start = start;
            }

            public String getEnd() {
                return end;
            }

            public void setEnd(String end) {
                this.end = end;
            }
        }
    }
}
