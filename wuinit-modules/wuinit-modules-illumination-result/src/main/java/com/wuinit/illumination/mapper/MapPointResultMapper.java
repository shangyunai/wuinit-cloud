package com.wuinit.illumination.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuinit.illumination.domain.entity.MapPoint;
import com.wuinit.illumination.domain.entity.MapPointResult;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MapPointResultMapper extends BaseMapper<MapPointResult> {

}
