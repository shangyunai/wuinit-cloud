package com.wuinit.illumination.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuinit.common.core.utils.StringUtils;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.illumination.domain.entity.MapStreet;
import com.wuinit.illumination.domain.entity.MapStreetResult;
import com.wuinit.illumination.domain.vo.MapStreetTreeVo;
import com.wuinit.illumination.domain.vo.RegionStreetRateVo;
import com.wuinit.illumination.domain.vo.StreetLightRateVo;
import com.wuinit.illumination.service.MapStreetResultService;
import com.wuinit.illumination.service.MapStreetService;
import com.wuinit.illumination.service.ShowResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.Future;

@Service
public class ShowResultServiceImpl implements ShowResultService {

    @Autowired
    private MapStreetService mapStreetService;
    @Autowired
    private MapStreetResultService mapStreetResultService;

    @Override
    public AjaxResult getStreetTree(String status) {
        List<MapStreet> mapStreets = new ArrayList<>();
        if ("未检测".equals(status)) {
            List<MapStreetResult> mapStreetResults = mapStreetResultService.list();
            List<String> uuids = new ArrayList<>();
            mapStreetResults.stream().forEach(item -> {
                uuids.add(item.getUuid());
            });
            QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
            if (!CollectionUtils.isEmpty(uuids)) {
                mapStreetQueryWrapper.notIn("uuid", uuids);
            }
            mapStreets = mapStreetService.list(mapStreetQueryWrapper);

        } else if (StringUtils.isNotEmpty(status)) {
            QueryWrapper<MapStreetResult> mapStreetResultQueryWrapper = new QueryWrapper<>();
            mapStreetResultQueryWrapper.eq("light_result", status);
            List<MapStreetResult> mapStreetResults = mapStreetResultService.list(mapStreetResultQueryWrapper);
            List<String> uuids = new ArrayList<>();
            mapStreetResults.stream().forEach(item -> {
                uuids.add(item.getUuid());
            });
            QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
            mapStreetQueryWrapper.in("uuid", uuids);
            if (!CollectionUtils.isEmpty(uuids)) {
                mapStreets = mapStreetService.list(mapStreetQueryWrapper);
            }
        } else {
            mapStreets = mapStreetService.list();
        }
        List<MapStreetTreeVo> mapStreetTreeVos = new ArrayList<>();
        Map<String, List<MapStreetTreeVo.Street>> treeMap = new HashMap<>();
        mapStreets.stream().forEach(item -> {
            String region = item.getRegion();
            if (!treeMap.containsKey(region)) {
                List<MapStreetTreeVo.Street> streets = new ArrayList<>();
                MapStreetTreeVo.Street street = new MapStreetTreeVo.Street();
                street.setName(item.getStreetName());
                street.setType("street");
                street.setMapStreet(item);
                streets.add(street);
                treeMap.put(region, streets);
            } else {
                List<MapStreetTreeVo.Street> streets = treeMap.get(region);
                MapStreetTreeVo.Street street = new MapStreetTreeVo.Street();
                street.setName(item.getStreetName());
                street.setType("street");
                street.setMapStreet(item);
                streets.add(street);
                treeMap.put(region, streets);
            }
        });
        treeMap.keySet().stream().forEach(item -> {
            MapStreetTreeVo mapStreetTreeVo = new MapStreetTreeVo();
            List<MapStreetTreeVo.Street> streets = treeMap.get(item);
            mapStreetTreeVo.setName(item + "(" + streets.size() + ")");
            mapStreetTreeVo.setType("region");
            mapStreetTreeVo.setStreets(streets);
            mapStreetTreeVos.add(mapStreetTreeVo);
        });
        return AjaxResult.success(mapStreetTreeVos);
    }

    @Override
    public AjaxResult getRegionStreetRate() {
        List<MapStreetResult> mapStreetResults = mapStreetResultService.list();
        List<String> uuids = new ArrayList<>();
        mapStreetResults.stream().forEach(item -> {
            uuids.add(item.getUuid());
        });
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        if (!CollectionUtils.isEmpty(uuids)) {
            mapStreetQueryWrapper.in("uuid", uuids);
        }
        List<MapStreet> mapStreets = mapStreetService.list(mapStreetQueryWrapper);
        List<RegionStreetRateVo> regionStreetRateVos = new ArrayList<>();
        RegionStreetRateVo regionStreetRateVo1 = new RegionStreetRateVo();
        regionStreetRateVo1.setRegionName("金牛区");
        RegionStreetRateVo regionStreetRateVo2 = new RegionStreetRateVo();
        regionStreetRateVo2.setRegionName("青羊区");
        RegionStreetRateVo regionStreetRateVo3 = new RegionStreetRateVo();
        regionStreetRateVo3.setRegionName("锦江区");
        RegionStreetRateVo regionStreetRateVo4 = new RegionStreetRateVo();
        regionStreetRateVo4.setRegionName("成华区");
        RegionStreetRateVo regionStreetRateVo5 = new RegionStreetRateVo();
        regionStreetRateVo5.setRegionName("武侯区");
        RegionStreetRateVo regionStreetRateVo6 = new RegionStreetRateVo();
        regionStreetRateVo6.setRegionName("高新区");
        for (MapStreet mapStreet : mapStreets) {
            if ("金牛区".equals(mapStreet.getRegion())) {
                int roadCount = regionStreetRateVo1.getRoadCount();
                roadCount++;
                regionStreetRateVo1.setRoadCount(roadCount);
                int isLightOkCount = regionStreetRateVo1.getIsLightOkCount();
                boolean lightOk = isLightOk(mapStreet.getUuid(), mapStreetResults);
                if (lightOk) {
                    isLightOkCount++;
                }
                regionStreetRateVo1.setIsLightOkCount(isLightOkCount);
            } else if ("青羊区".equals(mapStreet.getRegion())) {
                int roadCount = regionStreetRateVo2.getRoadCount();
                roadCount++;
                regionStreetRateVo2.setRoadCount(roadCount);
                int isLightOkCount = regionStreetRateVo2.getIsLightOkCount();
                boolean lightOk = isLightOk(mapStreet.getUuid(), mapStreetResults);
                if (lightOk) {
                    isLightOkCount++;
                }
                regionStreetRateVo2.setIsLightOkCount(isLightOkCount);
            } else if ("锦江区".equals(mapStreet.getRegion())) {
                int roadCount = regionStreetRateVo3.getRoadCount();
                roadCount++;
                regionStreetRateVo3.setRoadCount(roadCount);
                int isLightOkCount = regionStreetRateVo3.getIsLightOkCount();
                boolean lightOk = isLightOk(mapStreet.getUuid(), mapStreetResults);
                if (lightOk) {
                    isLightOkCount++;
                }
                regionStreetRateVo3.setIsLightOkCount(isLightOkCount);
            } else if ("成华区".equals(mapStreet.getRegion())) {
                int roadCount = regionStreetRateVo4.getRoadCount();
                roadCount++;
                regionStreetRateVo4.setRoadCount(roadCount);
                int isLightOkCount = regionStreetRateVo4.getIsLightOkCount();
                boolean lightOk = isLightOk(mapStreet.getUuid(), mapStreetResults);
                if (lightOk) {
                    isLightOkCount++;
                }
                regionStreetRateVo4.setIsLightOkCount(isLightOkCount);
            } else if ("武侯区".equals(mapStreet.getRegion())) {
                int roadCount = regionStreetRateVo5.getRoadCount();
                roadCount++;
                regionStreetRateVo5.setRoadCount(roadCount);
                int isLightOkCount = regionStreetRateVo5.getIsLightOkCount();
                boolean lightOk = isLightOk(mapStreet.getUuid(), mapStreetResults);
                if (lightOk) {
                    isLightOkCount++;
                }
                regionStreetRateVo5.setIsLightOkCount(isLightOkCount);
            } else if ("高新区".equals(mapStreet.getRegion())) {
                int roadCount = regionStreetRateVo6.getRoadCount();
                roadCount++;
                regionStreetRateVo6.setRoadCount(roadCount);
                int isLightOkCount = regionStreetRateVo6.getIsLightOkCount();
                boolean lightOk = isLightOk(mapStreet.getUuid(), mapStreetResults);
                if (lightOk) {
                    isLightOkCount++;
                }
                regionStreetRateVo6.setIsLightOkCount(isLightOkCount);
            }
        }
        regionStreetRateVos.add(regionStreetRateVo1);
        regionStreetRateVos.add(regionStreetRateVo2);
        regionStreetRateVos.add(regionStreetRateVo3);
        regionStreetRateVos.add(regionStreetRateVo4);
        regionStreetRateVos.add(regionStreetRateVo5);
        regionStreetRateVos.add(regionStreetRateVo6);
        for (RegionStreetRateVo bean : regionStreetRateVos) {
            if (bean.getIsLightOkCount() == 0) {
                bean.setRate(new BigDecimal(0));
            } else {
                bean.setRate(new BigDecimal(bean.getIsLightOkCount()).divide(new BigDecimal(bean.getRoadCount()), 4, BigDecimal.ROUND_HALF_DOWN).setScale(4, RoundingMode.HALF_DOWN).multiply(new BigDecimal(100)));
            }
        }
        return AjaxResult.success(regionStreetRateVos);
    }

    /**
     * 判断道路照度是否合格
     *
     * @param uuid
     * @return
     */
    private boolean isLightOk(String uuid, List<MapStreetResult> mapStreetResults) {
        boolean isLightOk = false;
        for (MapStreetResult mapStreetResult : mapStreetResults) {
            if (uuid.equals(mapStreetResult.getUuid())) {
                if ("合格".equals(mapStreetResult.getLightResult())) {
                    isLightOk = true;
                    break;
                }
            }
        }
        return isLightOk;
    }

    @Override
    public AjaxResult getStreetLightRateByType(String streetType) {
        List<String> lightRateFwNames = new ArrayList<>();
        lightRateFwNames.add("10-20");
        lightRateFwNames.add("20-30");
        lightRateFwNames.add("30-40");
        lightRateFwNames.add("40-50");
        lightRateFwNames.add("50-60");
        lightRateFwNames.add("60>");
        List<StreetLightRateVo> streetLightRateVos = new ArrayList<>();
        for (int i = 0; i < lightRateFwNames.size(); i++) {
            StreetLightRateVo streetLightRateVo = new StreetLightRateVo();
            streetLightRateVo.setName(lightRateFwNames.get(i));
            streetLightRateVos.add(streetLightRateVo);
        }
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        mapStreetQueryWrapper.eq("street_type", streetType);
        List<MapStreet> zglList = mapStreetService.list(mapStreetQueryWrapper);
        List<String> zglUuids = new ArrayList<>();
        zglList.stream().forEach(mapStreet -> {
            zglUuids.add(mapStreet.getUuid());
        });
        QueryWrapper<MapStreetResult> mapStreetResultQueryWrapper = new QueryWrapper<>();
        mapStreetResultQueryWrapper.in("uuid", zglUuids);
        List<MapStreetResult> zglResults = mapStreetResultService.list(mapStreetResultQueryWrapper);
        List<BigDecimal> rates = new ArrayList<>();
        double oneCount = 0;
        double twoCount = 0;
        double threeCount = 0;
        double fourCount = 0;
        double fiveCount = 0;
        double sixCount = 0;
        if (zglResults != null && zglResults.size() > 0) {
            for (MapStreetResult mapStreetResult : zglResults) {
                double lightAverage = mapStreetResult.getLightAverage().doubleValue();
                if (lightAverage >= 10 && lightAverage < 20) {
                    oneCount++;
                } else if (lightAverage >= 20 && lightAverage < 30) {
                    twoCount++;
                } else if (lightAverage >= 30 && lightAverage < 40) {
                    threeCount++;
                } else if (lightAverage >= 40 && lightAverage < 50) {
                    fourCount++;
                } else if (lightAverage >= 50 && lightAverage < 60) {
                    fiveCount++;
                } else if (lightAverage >= 60) {
                    sixCount++;
                }
            }
            rates.add(new BigDecimal(oneCount / zglResults.size() * 100).setScale(2, BigDecimal.ROUND_HALF_DOWN));
            rates.add(new BigDecimal(twoCount / zglResults.size() * 100).setScale(2, BigDecimal.ROUND_HALF_DOWN));
            rates.add(new BigDecimal(threeCount / zglResults.size() * 100).setScale(2, BigDecimal.ROUND_HALF_DOWN));
            rates.add(new BigDecimal(fourCount / zglResults.size() * 100).setScale(2, BigDecimal.ROUND_HALF_DOWN));
            rates.add(new BigDecimal(fiveCount / zglResults.size() * 100).setScale(2, BigDecimal.ROUND_HALF_DOWN));
            rates.add(new BigDecimal(sixCount / zglResults.size() * 100).setScale(2, BigDecimal.ROUND_HALF_DOWN));
        }
        if (streetLightRateVos.size() == rates.size()) {
            for (int i = 0; i < streetLightRateVos.size(); i++) {
                streetLightRateVos.get(i).setValue(rates.get(i));
            }
        } else {
            for (int i = 0; i < streetLightRateVos.size(); i++) {
                streetLightRateVos.get(i).setValue(new BigDecimal(0));
            }
        }
        Map<String, Object> data = new HashMap<>();
        data.put("streetType", streetType);
        data.put("data", streetLightRateVos);
        return AjaxResult.success(data);
    }
}
