package com.wuinit.illumination.geoserver;

public class GeoserverUtils {
    /**
     * 构造更新 geoserver 矢量图层要素属性 xml
     *
     * @param workspace
     * @param layer
     * @param propertyName
     * @param propertyValue
     * @param featureId
     * @return
     */
    public static String buildTransactionWFSUpdateRequest(String workspace, String layer, String propertyName, String propertyValue, String featureId) {
        String transactionRequestTemplate =
                "<wfs:Transaction service=\"WFS\" version=\"1.0.0\"\n" +
                        "  xmlns:%s=\"http://www.openplans.org/%s\"\n" +
                        "  xmlns:ogc=\"http://www.opengis.net/ogc\"\n" +
                        "  xmlns:wfs=\"http://www.opengis.net/wfs\">\n" +
                        "  <wfs:Update typeName=\"%s:%s\">\n" +
                        "    <wfs:Property>\n" +
                        "      <wfs:Name>%s</wfs:Name>\n" +
                        "      <wfs:Value>%s</wfs:Value>\n" +
                        "    </wfs:Property>\n" +
                        "    <ogc:Filter>\n" +
                        "      <ogc:FeatureId fid=\"%s\"/>\n" +
                        "    </ogc:Filter>\n" +
                        "  </wfs:Update>\n" +
                        "</wfs:Transaction>\n";

        // 使用String.format填充模板
        return String.format(transactionRequestTemplate, workspace, workspace, workspace, layer, propertyName, propertyValue, featureId);
    }

}
