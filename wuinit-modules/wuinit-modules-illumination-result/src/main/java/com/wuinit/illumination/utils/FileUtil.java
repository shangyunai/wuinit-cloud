package com.wuinit.illumination.utils;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author 吴靖
 * @desc 文件操作工具类
 * @date 2020/5/27 10:36
 */
public class FileUtil {

    public static final String TYPE_JPG = "jpg";
    public static final String TYPE_GIF = "gif";
    public static final String TYPE_PNG = "png";
    public static final String TYPE_BMP = "bmp";
    public static final String TYPE_UNKNOWN = "unknown";

    /**
     * 文件转byte数组
     *
     * @param file
     * @return
     */
    public static byte[] fileConvertToByteArray(File file) {
        byte[] data = null;
        FileInputStream fis = null;
        ByteArrayOutputStream baos = null;
        try {
            fis = new FileInputStream(file);
            baos = new ByteArrayOutputStream();
            int len;
            byte[] buffer = new byte[1024];
            while ((len = fis.read(buffer)) != -1) {
                baos.write(buffer, 0, len);
            }
            data = baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return data;
    }

    /**
     * multipartFile 转换成 file
     *
     * @param multipartFile
     * @return
     */
    public static File multipartFileToFile(MultipartFile multipartFile) {
        File excelFile = null;
        try {
            String originalFilename = multipartFile.getOriginalFilename();
            String prefix = originalFilename.substring(multipartFile.getOriginalFilename().lastIndexOf("."));
            excelFile = File.createTempFile(String.valueOf(System.currentTimeMillis()), prefix);
            multipartFile.transferTo(excelFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return excelFile;
    }

    /**
     * @param bytes byte数组
     * @return MultipartFile
     * @author gdw
     * @desc byte数组转MultipartFile
     */
    public static MultipartFile byteArrayToMultipartFile(byte[] bytes, String name, String contentType) {
        return new MockMultipartFile(name, null, contentType, bytes);
    }

    /**
     * file 转 MultipartFile
     * @param file
     * @return
     */
    public static MultipartFile getMultipartFile(File file) {
        FileItem item = new DiskFileItemFactory().createItem("file"
                , MediaType.MULTIPART_FORM_DATA_VALUE
                , true
                , file.getName());
        try (InputStream input = new FileInputStream(file);
             OutputStream os = item.getOutputStream()) {
            // 流转移
            IOUtils.copy(input, os);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid file: " + e, e);
        }

        return new CommonsMultipartFile(item);
    }

    /**
     * 删除文件
     *
     * @param files
     */
    public static void deleteFile(File... files) {
        for (File file : files) {
            if (file.exists()) {
                file.delete();
            }
        }
    }

    /**
     * 删除文件或文件夹
     *
     * @param directory
     */
    public static void delAllFile(File directory) {
        if (!directory.isDirectory()) {
            directory.delete();
        } else {
            File[] files = directory.listFiles();
            // 空文件夹
            if (files.length == 0) {
                directory.delete();
                System.out.println("删除" + directory.getAbsolutePath());
                return;
            }
            // 删除子文件夹和子文件
            for (File file : files) {
                if (file.isDirectory()) {
                    delAllFile(file);
                } else {
                    deleteFile(file);
                    System.out.println("删除" + file.getAbsolutePath());
                }
            }
            // 删除文件夹本身
            deleteFile(directory);
            System.out.println("删除" + directory.getAbsolutePath());
        }
    }

    /**
     * base64字符串转文件
     *
     * @param base64
     * @return
     */
    public static File base64ToFile(String base64, String suffix) {
        if (base64 == null || "".equals(base64)) {
            return null;
        }
        byte[] buff = Base64.decode(base64);
        File file = null;
        FileOutputStream fout = null;
        try {
            file = File.createTempFile(String.valueOf(System.currentTimeMillis()), suffix);
            fout = new FileOutputStream(file);
            fout.write(buff);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fout != null) {
                try {
                    fout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    /**
     * 遍历文件夹下所有文件
     *
     * @param filePath 文件夹路径
     * @param fileList 所有文件路径
     */
    public static List<String> getFiles(String filePath, List<String> fileList) {
        File[] files = new File(filePath).listFiles();
        if (files == null) {
            return fileList;
        }
        for (File file : files) {
            if (file.isDirectory()) {
                getFiles(file.getAbsolutePath(), fileList);
            } else {
                fileList.add(file.getPath());
            }
        }
        return fileList;
    }

    /**
     * 获取文件夹下，所有子文件夹
     *
     * @param filePath         文件夹路径
     * @param childDirectories 所有子文件夹
     */
    public static List<File> getFilesDirectoryPath(String filePath, List<File> childDirectories) {
        File[] files = new File(filePath).listFiles();
        if (files == null) {
            return childDirectories;
        }
        for (File file : files) {
            if (file.isDirectory()) {
                childDirectories.add(file);
            }
        }
        return childDirectories;
    }

    /**
     * 或者指定目录下，指定名称的文件
     *
     * @param filePath 文件夹路径
     * @param fileName 指定名称
     */
    public static File getFileByName(String filePath, String fileName) {
        File[] files = new File(filePath).listFiles();
        if (files == null) {
            return null;
        }
        for (File file : files) {
            if (file.isDirectory()) {
                getFileByName(file.getAbsolutePath(), fileName);
            } else {
                if (fileName.equals(file.getName())) {
                    return file;
                }
            }
        }
        return null;
    }

    public static List<File> getFilesByLikeName(String filePath, String staff, List<File> fileList) {
        File[] files = new File(filePath).listFiles();
        if (files == null) {
            return null;
        }
        for (File file : files) {
            if (file.isDirectory()) {
                getFilesByLikeName(file.getAbsolutePath(), staff, fileList);
            } else {
                if (file.getName().contains(staff)) {
                    fileList.add(file);
                }
            }
        }
        return fileList;
    }

    public static String readFileToString(String filePath) throws IOException {
        // 使用 Paths.get 方法获取 Path 对象
        Path path = Paths.get(filePath);
        // 使用 Files.readAllBytes 读取文件内容到字节数组
        byte[] bytes = Files.readAllBytes(path);
        // 使用字符串构造器将字节数组转换为字符串
        String content = new String(bytes);
        return content;
    }

    /**
     * byte数组转换成16进制字符串
     *
     * @param src
     * @return
     */
    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder();
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    /**
     * 根据文件流判断图片类型
     *
     * @param fis
     * @return jpg/png/gif/bmp
     */
    public static String getPicType(FileInputStream fis) {
        //读取文件的前几个字节来判断图片格式
        byte[] b = new byte[4];
        try {
            fis.read(b, 0, b.length);
            String type = bytesToHexString(b).toUpperCase();
            if (type.contains("FFD8FF")) {
                return TYPE_JPG;
            } else if (type.contains("89504E47")) {
                return TYPE_PNG;
            } else {
                return TYPE_UNKNOWN;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
