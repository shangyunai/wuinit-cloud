package com.wuinit.illumination.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 线程池配置
 *
 * @author wujing
 **/
@Configuration
@EnableAsync
public class ThreadPoolConfig {
    /**
     * 核心线程池大小
     */
    private int corePoolSize = 10;
    /**
     * 最大可创建的线程数
     */
    private int maxPoolSize = 10;
    /**
     * 队列最大长度
     */
    private int queueCapacity = 300;
    /**
     * 线程池维护线程所允许的空闲时间
     */
    private int keepAliveSeconds = 30;
    /**
     * 核数
     */
    private int processor = Runtime.getRuntime().availableProcessors();

    @Bean(name = "threadPoolTaskExecutor")
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setMaxPoolSize(maxPoolSize);
        executor.setCorePoolSize(corePoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setKeepAliveSeconds(keepAliveSeconds);
        // 线程池对拒绝任务(无线程可用)的处理策略
        // ThreadPoolExecutor.AbortPolicy:丢弃任务并抛出RejectedExecutionException异常。  【默认】
        //ThreadPoolExecutor.DiscardPolicy：也是丢弃任务，但是不抛出异常。
        //ThreadPoolExecutor.DiscardOldestPolicy：丢弃线称队列的旧的任务，将新的任务添加
        //ThreadPoolExecutor.CallerRunsPolicy：由调用线程处理该任务 【谁调用，谁处理】
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardOldestPolicy());
        executor.initialize();
        return executor;
    }

    @Bean(name = "threadPoolProcessorTaskExecutor")
    public ThreadPoolTaskExecutor threadPoolProcessorTaskExecutor() {
        //处理器核心数
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setMaxPoolSize(2 * processor + 1);
        executor.setCorePoolSize(processor + 1);
        executor.setQueueCapacity(queueCapacity);
        executor.setKeepAliveSeconds(keepAliveSeconds);
        // 线程池对拒绝任务(无线程可用)的处理策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }

}
