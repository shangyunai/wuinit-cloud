package com.wuinit.illumination.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuinit.common.core.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 地图道路检测结果 map_street_result
 *
 * @author wuinit
 * @date 2023-10-17
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "map_street_result")
public class MapStreetResult implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 道路唯一标识 uuid
     */
    private String uuid;

    /**
     * 道路唯一标识 道路名称
     */
    private String name;


    /**
     * 平均照度
     */
    private BigDecimal lightAverage;

    /**
     * 照度均匀度
     */
    private BigDecimal lightUniformity;

    /**
     * 平均亮度
     */
    private BigDecimal averageBrightness;

    /**
     * 亮度均匀度
     */
    private BigDecimal brightnessUniformity;

    /**
     * 照度最大值
     */
    private BigDecimal maxLight;

    /**
     * 照度最小值
     */
    private BigDecimal minLight;

    /**
     * 亮度最大值
     */
    private BigDecimal maxBright;

    /**
     * 亮度最小值
     */
    private BigDecimal minBright;

    /**
     * 达标率
     */
    private BigDecimal rate;

    /**
     * 平均速度
     */
    private BigDecimal averageSpeed;

    /**
     * 起点经度
     */
    private BigDecimal startLon;

    /**
     * 起点纬度
     */
    private BigDecimal startLat;

    /**
     * 终点经度
     */
    private BigDecimal endLon;

    /**
     * 终点纬度
     */
    private BigDecimal endLat;

    /**
     * 照度合格结果
     */
    private String lightResult;

    /**
     * 道路检测次数
     */
    private Integer checkTime;

    /**
     * 道路检测时间
     */
    private String time;


    /**
     * 道路检测人员
     */
    private String person;
}
