package com.wuinit.illumination.service.impl;

import com.alibaba.nacos.shaded.com.google.gson.Gson;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.jmx.remote.internal.ArrayQueue;
import com.wuinit.common.core.constant.HttpStatus;
import com.wuinit.common.core.utils.StringUtils;
import com.wuinit.common.core.utils.uuid.IdUtils;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.common.core.web.page.TableDataInfo;
import com.wuinit.common.log.enums.BusinessType;
import com.wuinit.illumination.api.MapApi;
import com.wuinit.illumination.config.GeoserverConfig;
import com.wuinit.illumination.domain.entity.MapPoint;
import com.wuinit.illumination.domain.entity.MapStreet;
import com.wuinit.illumination.domain.entity.MapStreetExt;
import com.wuinit.illumination.domain.entity.MapStreetResult;
import com.wuinit.illumination.domain.form.MapStreetForm;
import com.wuinit.illumination.domain.query.MapStreetResultQuery;
import com.wuinit.illumination.domain.vo.MapStreetResultVo;
import com.wuinit.illumination.domain.vo.MapStreetTreeVo;
import com.wuinit.illumination.mapper.MapStreetExtMapper;
import com.wuinit.illumination.mapper.MapStreetMapper;
import com.wuinit.illumination.service.MapPointService;
import com.wuinit.illumination.service.MapStreetExtService;
import com.wuinit.illumination.service.MapStreetResultService;
import com.wuinit.illumination.service.MapStreetService;
import io.jsonwebtoken.lang.Collections;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Service
public class MapStreetServiceImpl extends ServiceImpl<MapStreetMapper, MapStreet> implements MapStreetService {

    @Autowired
    private GeoserverConfig geoserverConfig;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private MapPointService mapPointService;
    @Autowired
    private MapStreetExtService mapStreetExtService;
    @Resource
    private KafkaTemplate<String, Object> kafkaTemplate;
    @Autowired
    private MapStreetResultService mapStreetResultService;
    @Resource(name = "threadPoolProcessorTaskExecutor")
    private ThreadPoolTaskExecutor executor;

    @Override
    public TableDataInfo list(MapStreet mapStreet, int pageNum, int pageSize) {
        TableDataInfo tableDataInfo = new TableDataInfo();
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        String region = mapStreet.getRegion();
        if (StringUtils.isNotEmpty(region)) {
            mapStreetQueryWrapper.eq("region", region);
        }
        String streetStatus = mapStreet.getStreetStatus();
        if (StringUtils.isNotEmpty(streetStatus)) {
            mapStreetQueryWrapper.eq("street_status", streetStatus);
        }
        if (StringUtils.isNotEmpty(mapStreet.getUuid())) {
            mapStreetQueryWrapper.eq("uuid", mapStreet.getUuid());
        }
        Page<MapStreet> page = new Page<>(pageNum, pageSize);
        page = page(page, mapStreetQueryWrapper);
        List<MapStreet> streets = page.getRecords();
        long total = page.getTotal();
        tableDataInfo.setCode(HttpStatus.SUCCESS);
        tableDataInfo.setRows(streets);
        tableDataInfo.setMsg("查询成功");
        tableDataInfo.setTotal(total);
        return tableDataInfo;
    }

    @Override
    public AjaxResult getInfo(Long id) {
        return null;
    }

    @Override
    public AjaxResult insert(MapStreet mapStreet) {
        return null;
    }

    @Override
    public AjaxResult update(MapStreet mapStreet) {
        // TODO: 2023/11/15 更新道路备注信息
        Long id = mapStreet.getId();
        MapStreet byId = getById(id);
        byId.setStreetDecs(mapStreet.getStreetDecs());
        updateById(byId);
        return AjaxResult.success();
    }

    @Override
    public AjaxResult delete(Long[] id) {
        return null;
    }

    @Override
    public AjaxResult geoserverWfs(MapStreetForm mapStreetForm) {
        String optionType = mapStreetForm.getOptionType();
        String textXml = mapStreetForm.getTextXml();
        if (BusinessType.INSERT.name().equals(optionType)) {
            String geoserverWFS = MapApi.getInstance().geoserverWFS(textXml, geoserverConfig, restTemplate);
            if (geoserverWFS.contains("SUCCESS")) {
                MapStreet mapStreet = new MapStreet();
                BeanUtils.copyProperties(mapStreetForm, mapStreet);
                save(mapStreet);
                return AjaxResult.success();
            }
        } else if (BusinessType.UPDATE.name().equals(optionType)) {
            String geoserverWFS = MapApi.getInstance().geoserverWFS(textXml, geoserverConfig, restTemplate);
            if (geoserverWFS.contains("SUCCESS")) {
                String uuid = mapStreetForm.getUuid();
                QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
                mapStreetQueryWrapper.eq("uuid", uuid);
                MapStreet mapStreet = getOne(mapStreetQueryWrapper, false);
                if (mapStreet != null) {
                    BeanUtils.copyProperties(mapStreetForm, mapStreet);
                }
                updateById(mapStreet);
                return AjaxResult.success();
            }
        } else if (BusinessType.DELETE.name().equals(optionType)) {
            String geoserverWFS = MapApi.getInstance().geoserverWFS(textXml, geoserverConfig, restTemplate);
            if (geoserverWFS.contains("SUCCESS")) {
                String uuid = mapStreetForm.getUuid();
                QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
                mapStreetQueryWrapper.eq("uuid", uuid);
                remove(mapStreetQueryWrapper);
                return AjaxResult.success();
            }
        }
        return AjaxResult.error();
    }

    @Override
    public AjaxResult getUUID() {
        String uuid = IdUtils.simpleUUID();
        Map<String, String> uuidMap = new HashMap<>();
        uuidMap.put("uuid", uuid);
        return AjaxResult.success(uuidMap);
    }

    @Override
    public AjaxResult getStreetTree(String status) {
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(status)) {
            mapStreetQueryWrapper.eq("street_status", status);
        }
        List<MapStreet> mapStreets = list(mapStreetQueryWrapper);
        List<MapStreetTreeVo> mapStreetTreeVos = new ArrayList<>();
        Map<String, List<MapStreetTreeVo.Street>> treeMap = new HashMap<>();
        mapStreets.stream().forEach(item -> {
            String region = item.getRegion();
            if (!treeMap.containsKey(region)) {
                List<MapStreetTreeVo.Street> streets = new ArrayList<>();
                MapStreetTreeVo.Street street = new MapStreetTreeVo.Street();
                street.setName(item.getStreetName());
                street.setType("street");
                street.setMapStreet(item);
                streets.add(street);
                treeMap.put(region, streets);
            } else {
                List<MapStreetTreeVo.Street> streets = treeMap.get(region);
                MapStreetTreeVo.Street street = new MapStreetTreeVo.Street();
                street.setName(item.getStreetName());
                street.setType("street");
                street.setMapStreet(item);
                streets.add(street);
                treeMap.put(region, streets);
            }
        });
        treeMap.keySet().stream().forEach(item -> {
            MapStreetTreeVo mapStreetTreeVo = new MapStreetTreeVo();
            List<MapStreetTreeVo.Street> streets = treeMap.get(item);
            mapStreetTreeVo.setName(item + "(" + streets.size() + ")");
            mapStreetTreeVo.setType("region");
            mapStreetTreeVo.setStreets(streets);
            mapStreetTreeVos.add(mapStreetTreeVo);
        });
        return AjaxResult.success(mapStreetTreeVos);
    }

    @Override
    public AjaxResult getGeoserverConfig() {
        return AjaxResult.success(geoserverConfig);
    }

    @Override
    public String getGeoserverStreetMap() {
        return MapApi.getInstance().getStreetMap(geoserverConfig, restTemplate);
    }

    @Override
    public String getStreetByPoint(String pointStr) {
        return MapApi.getInstance().getStreetByPoint(pointStr, geoserverConfig, restTemplate);
    }

    @Override
    public AjaxResult updateStreetStatus() {
        List<MapStreet> mapStreets = list();
        new Thread(new Runnable() {
            @Override
            public void run() {
                mapStreets.stream().forEach(item -> {
                    String uuid = item.getUuid();
                    QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
                    mapPointQueryWrapper.eq("street_uuid", uuid);
                    mapPointQueryWrapper.eq("isDataOk", 1);
                    MapPoint mapPoint = mapPointService.getOne(mapPointQueryWrapper, false);
                    if (mapPoint != null) {
                        item.setStreetStatus("已检测");
                    } else {
                        item.setStreetStatus("未检测");
                    }
                    update(item);
                });
            }
        }).start();
        return AjaxResult.success();
    }

    @Override
    public AjaxResult sendKafka(String uuid) {
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        mapStreetQueryWrapper.eq("uuid", uuid);
        MapStreet mapStreet = getOne(mapStreetQueryWrapper, false);
        if (mapStreet != null) {
            String message = new Gson().toJson(mapStreet);
            kafkaTemplate.send("detecting_road", message);
        }
        return AjaxResult.success();
    }

    @Override
    public AjaxResult sendKafkaCheckStatus(String status) {
        HashMap<String, String> statusMap = new HashMap<>();
        statusMap.put("status", status);
        String message = new Gson().toJson(statusMap);
        kafkaTemplate.send("check_state", message);
        return AjaxResult.success();
    }

    @Override
    public AjaxResult streetExtSave(MapStreetExt mapStreetExt) {
        String uuid = mapStreetExt.getUuid();
        QueryWrapper<MapStreetExt> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("uuid", uuid);
        MapStreetExt one = mapStreetExtService.getOne(queryWrapper, false);
        if (one == null) {
            boolean save = mapStreetExtService.save(mapStreetExt);
            if (save) {
                return AjaxResult.success();
            }
        } else {
            boolean update = mapStreetExtService.update(mapStreetExt, queryWrapper);
            if (update) {
                return AjaxResult.success();
            }
        }
        return AjaxResult.error();
    }

    @Override
    public AjaxResult streetExtGet(String uuid) {
        QueryWrapper<MapStreetExt> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("uuid", uuid);
        MapStreetExt one = mapStreetExtService.getOne(queryWrapper, false);
        if (one != null) {
            return AjaxResult.success(one);
        }
        return AjaxResult.success();
    }

    @Override
    public TableDataInfo resultList(MapStreetResultQuery mapStreetResultQuery) {
        TableDataInfo tableDataInfo = new TableDataInfo();
        Integer pageNum = mapStreetResultQuery.getPageNum();
        Integer pageSize = mapStreetResultQuery.getPageSize();
        List<MapStreetResultVo> mapStreetResultVos = new ArrayList<>();
        List<MapStreetResult> mapStreetResults = new ArrayList<>();
        if (pageNum != null && pageSize != null) {
            Page page = new Page(pageNum, pageSize);
            page = mapStreetResultService.page(page);
            mapStreetResults = page.getRecords();
            tableDataInfo.setTotal(page.getTotal());
        } else {
            mapStreetResults = mapStreetResultService.list();
            tableDataInfo.setTotal(mapStreetResults.size());
        }
//        if (!Collections.isEmpty(mapStreetResults)) {
//            List<Future> futures = new ArrayList<>();
//            mapStreetResults.stream().forEach(mapStreetResult -> {
//                String uuid = mapStreetResult.getUuid();
//                //异步查询封装道路检测结果数据
//                Future<?> submit = executor.submit(new Runnable() {
//                    @Override
//                    public void run() {
//                        MapStreetResultVo mapStreetResultVo = new MapStreetResultVo();
//                        if ("合格".equals(mapStreetResult.getLightResult())) {
//                            mapStreetResult.setLightResult("正常");
//                        }
//                        mapStreetResultVo.setMapStreetResult(mapStreetResult);
//                        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
//                        mapStreetQueryWrapper.eq("uuid", uuid);
//                        MapStreet mapStreet = getOne(mapStreetQueryWrapper, false);
//                        //判断是否是一期的道路
////                        if ("一期800条道路".equals(mapStreet.getRegion())) {
////                            mapStreet.setRegion("-");
////                        }
//                        QueryWrapper<MapStreet> mapStreetQueryWrapper1 = new QueryWrapper<>();
//                        mapStreetQueryWrapper1.eq("street_name", mapStreet.getStreetName());
//                        List<MapStreet> list = list(mapStreetQueryWrapper1);
//                        if (list.size() > 1) {
//                            mapStreet.setStreetName(mapStreet.getStreetName() + mapStreet.getId());
//                        }
//                        mapStreetResultVo.setMapStreet(mapStreet);
//                        QueryWrapper<MapStreetExt> mapStreetExtQueryWrapper = new QueryWrapper<>();
//                        mapStreetExtQueryWrapper.eq("uuid", uuid);
//                        MapStreetExt mapStreetExt = mapStreetExtService.getOne(mapStreetExtQueryWrapper, false);
//                        mapStreetResultVo.setMapStreetExt(mapStreetExt);
//                        MapPoint mapPoint = new MapPoint();
//                        mapPoint.setStreetUuid(uuid);
//                        List<MapPoint> mapPoints = new ArrayList<>();
//                        List<List<MapPoint>> pointByUuid = mapPointService.getPointByUuid(mapPoint);
//                        pointByUuid.stream().forEach(points -> {
//                            mapPoints.addAll(points);
//                        });
//                        mapPoints.stream().forEach(item -> {
//                            if ("合格".equals(item.getCompliance())) {
//                                item.setCompliance("正常");
//                            }
//                        });
//                        mapStreetResultVo.setMapPoints(mapPoints);
//                        mapStreetResultVos.add(mapStreetResultVo);
//                    }
//                });
//                futures.add(submit);
//            });
//            futures.stream().forEach(future -> {
//                try {
//                    future.get();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                } catch (ExecutionException e) {
//                    e.printStackTrace();
//                }
//            });
//        }
        //单线程查询数据封装
        if (!Collections.isEmpty(mapStreetResults)) {
            mapStreetResults.stream().forEach(mapStreetResult -> {
                String uuid = mapStreetResult.getUuid();
                MapStreetResultVo mapStreetResultVo = new MapStreetResultVo();
                if ("合格".equals(mapStreetResult.getLightResult())) {
                    mapStreetResult.setLightResult("正常");
                }
                mapStreetResultVo.setMapStreetResult(mapStreetResult);
                QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
                mapStreetQueryWrapper.eq("uuid", uuid);
                MapStreet mapStreet = getOne(mapStreetQueryWrapper, false);
                //判断是否是一期的道路
//                        if ("一期800条道路".equals(mapStreet.getRegion())) {
//                            mapStreet.setRegion("-");
//                        }
                QueryWrapper<MapStreet> mapStreetQueryWrapper1 = new QueryWrapper<>();
                mapStreetQueryWrapper1.eq("street_name", mapStreet.getStreetName());
                mapStreetQueryWrapper1.ne("region", "一期800条道路");
                List<MapStreet> list = list(mapStreetQueryWrapper1);
                if (list.size() > 1) {
                    mapStreet.setStreetName(mapStreet.getStreetName() + mapStreet.getId());
                }
                mapStreetResultVo.setMapStreet(mapStreet);
                QueryWrapper<MapStreetExt> mapStreetExtQueryWrapper = new QueryWrapper<>();
                mapStreetExtQueryWrapper.eq("uuid", uuid);
                MapStreetExt mapStreetExt = mapStreetExtService.getOne(mapStreetExtQueryWrapper, false);
                mapStreetResultVo.setMapStreetExt(mapStreetExt);
                MapPoint mapPoint = new MapPoint();
                mapPoint.setStreetUuid(uuid);
                List<MapPoint> mapPoints = new ArrayList<>();
                List<List<MapPoint>> pointByUuid = mapPointService.getPointByUuid(mapPoint);
                pointByUuid.stream().forEach(points -> {
                    mapPoints.addAll(points);
                });
                mapPoints.stream().forEach(item -> {
                    if ("合格".equals(item.getCompliance())) {
                        item.setCompliance("正常");
                    }
                });
                mapStreetResultVo.setMapPoints(mapPoints);
                mapStreetResultVos.add(mapStreetResultVo);
            });
        }
        tableDataInfo.setRows(mapStreetResultVos);
        return tableDataInfo;
    }

    @Override
    public AjaxResult resultListAll() {
        List<MapStreetResult> list = mapStreetResultService.list();
        return AjaxResult.success(list);
    }

    @Override
    public AjaxResult getCanNotCheck() {
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        mapStreetQueryWrapper.ne("street_decs", "");
        List<MapStreet> streetList = list(mapStreetQueryWrapper);
        return AjaxResult.success(streetList);
    }
}
