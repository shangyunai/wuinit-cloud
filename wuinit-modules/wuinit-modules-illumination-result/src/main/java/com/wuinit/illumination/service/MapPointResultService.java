package com.wuinit.illumination.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wuinit.illumination.domain.entity.MapPointImage;
import com.wuinit.illumination.domain.entity.MapPointResult;

public interface MapPointResultService extends IService<MapPointResult> {

}
