package com.wuinit.illumination.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wuinit.illumination.domain.entity.MapPointImage;

public interface MapPointImageService extends IService<MapPointImage> {

}
