package com.wuinit.illumination.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuinit.illumination.domain.entity.MapPoint;
import com.wuinit.illumination.domain.entity.MapStreetResult;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MapStreetResultMapper extends BaseMapper<MapStreetResult> {

}
