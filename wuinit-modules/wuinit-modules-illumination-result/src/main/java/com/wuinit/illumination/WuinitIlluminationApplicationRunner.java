package com.wuinit.illumination;


import com.wuinit.illumination.datacleaning.DataCleanTask;
import com.wuinit.illumination.datacleaning.DataCleaningUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


@Component
@Order(value = 2)
public class WuinitIlluminationApplicationRunner implements org.springframework.boot.ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(WuinitIlluminationApplicationRunner.class);

    @Autowired
    private DataCleaningUtil dataCleaningUtil;
    @Autowired
    private DataCleanTask dataCleanTask;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("dataCleaning start");
        //离线上传点位对应图片
        //dataCleaningUtil.offlineUploadPointImage();
        //更新点位roadMCode
        //dataCleaningUtil.updatePointRoadCode();
        //合并第一次，第二次测量数据
        //dataCleaningUtil.integrateTheFirstAndSecondTimeData();
        //插入第二次测量数据
        //dataCleaningUtil.insertSecondPointData();
        //判断道路合格性
        //dataCleanTask.runDataClean();
        //插入一期800条数据
        //dataCleaningUtil.insertYiqi800PointData();
        //更新道路状态
        //dataCleaningUtil.updateRoadFeatureProperty();
        //初始化不能正常检测的道路状态
        //dataCleaningUtil.updateRoadFeaturePropertyCanNotCheck();
        //测试发送kafka信息
        //dataCleaningUtil.kafkaUploadPoint();
        log.info("dataCleaning finish");
    }


}
