package com.wuinit.illumination.domain.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StreetLightRateVo {
    /**
     * 范围名称
     */
    private String name;
    /**
     * 占比值
     */
    private BigDecimal value;
}
