package com.wuinit.illumination.domain.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class RegionStreetRateVo {
    private String regionName;
    private int roadCount;
    private int isLightOkCount;
    private BigDecimal rate;
}
