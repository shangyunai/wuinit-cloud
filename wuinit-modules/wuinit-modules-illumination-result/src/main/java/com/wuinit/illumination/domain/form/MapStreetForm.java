package com.wuinit.illumination.domain.form;

import lombok.Data;

@Data
public class MapStreetForm {
    /**
     * 操作类型 add delete update
     */
    private String optionType;
    /**
     * geoserver xml
     */
    private String textXml;
    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String region;

    /**
     * 道路名称
     */
    private String streetName;

    /**
     * 道路等级
     */
    private String streetLevel;

    /**
     * 道路描述
     */
    private String streetDecs;

    /**
     * 道路类型
     */
    private String streetType;
    /**
     * 道路唯一标识
     */
    private String uuid;
    /**
     * 道路中心点
     */
    private String streetCenter;
    /**
     * 道路开始点
     */
    private String streetStart;
    /**
     * 道路结束点
     */
    private String streetEnd;

}
