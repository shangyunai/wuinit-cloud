package com.wuinit.illumination.kafka;

import com.alibaba.nacos.shaded.com.google.gson.Gson;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuinit.common.core.utils.StringUtils;
import com.wuinit.illumination.api.MapApi;
import com.wuinit.illumination.config.GeoserverConfig;
import com.wuinit.illumination.domain.entity.MapPoint;
import com.wuinit.illumination.domain.entity.MapPointResult;
import com.wuinit.illumination.domain.entity.MapStreet;
import com.wuinit.illumination.domain.result.MapFeatureResult;
import com.wuinit.illumination.service.MapPointResultService;
import com.wuinit.illumination.service.MapPointService;
import com.wuinit.illumination.service.MapStreetService;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class KafkaConsumer {

    @Autowired
    private MapPointService mapPointService;
    @Autowired
    private MapPointResultService mapPointResultService;
    @Autowired
    private MapStreetService mapStreetService;
    @Autowired
    private GeoserverConfig geoserverConfig;
    @Autowired
    private RestTemplate restTemplate;

    @Value("${kafka.topic.point}")
    private String topicPoint;

    @Resource(name = "threadPoolProcessorTaskExecutor")
    private ThreadPoolTaskExecutor executor;
    //监听消费
    @KafkaListener(topics = "${kafka.topic.point}")
    public void onMessage(ConsumerRecord<String, Object> record) {
        String value = record.value().toString();
        log.info("---value----:{}", value);
        try {
            KafkaMessage kafkaMessage = new Gson().fromJson(value, KafkaMessage.class);
            String lon = kafkaMessage.getLon();
            String lat = kafkaMessage.getLat();
            if (StringUtils.isNotEmpty(lon) && StringUtils.isNotEmpty(lat)) {
                //查询属于那条街道
                List<String> uuids = new ArrayList<>();
                String point = "POINT(" + lon.trim() + " " + lat.trim() + ")";
                //相交
                String streetByPoint = MapApi.getInstance().getStreetByPoint(point, geoserverConfig, restTemplate);
                if (streetByPoint != null) {
                    MapFeatureResult mapFeatureResult = new Gson().fromJson(streetByPoint, MapFeatureResult.class);
                    if (mapFeatureResult != null) {
                        List<MapFeatureResult.FeaturesDTO> features = mapFeatureResult.getFeatures();
                        if (!Collections.isEmpty(features)) {
                            features.stream().forEach(feature -> {
                                MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                if (properties != null) {
                                    String uuid = properties.getUuid();
                                    if (StringUtils.isNotEmpty(uuid)) {
                                        uuids.add(uuid);
                                    }
                                }
                            });
                        } else {
                            //包含
                            String streetByPointContains = MapApi.getInstance().getStreetByPointContains(point, geoserverConfig, restTemplate);
                            if (streetByPointContains != null) {
                                MapFeatureResult mapFeatureResultContains = new Gson().fromJson(streetByPointContains, MapFeatureResult.class);
                                if (mapFeatureResultContains != null) {
                                    List<MapFeatureResult.FeaturesDTO> featuresContains = mapFeatureResultContains.getFeatures();
                                    if (!Collections.isEmpty(featuresContains)) {
                                        featuresContains.stream().forEach(feature -> {
                                            MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                            if (properties != null) {
                                                String uuid = properties.getUuid();
                                                if (StringUtils.isNotEmpty(uuid)) {
                                                    uuids.add(uuid);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
                MapPoint mapPoint = new MapPoint();
                mapPoint.setLon(new BigDecimal(kafkaMessage.getLon().trim()));
                mapPoint.setLat(new BigDecimal(kafkaMessage.getLat().trim()));
                String time = kafkaMessage.getTime();
                if (StringUtils.isNotEmpty(time)) {
                    mapPoint.setTime(Long.valueOf(time.trim()));
                }
                String region = kafkaMessage.getRegion();
                if (StringUtils.isNotEmpty(region)) {
                    mapPoint.setRegion(region.trim());
                }
                String roadname = kafkaMessage.getRoadname();
                if (StringUtils.isNotEmpty(roadname)) {
                    mapPoint.setRoadname(roadname.trim());
                }
                String roadlevel = kafkaMessage.getRoadlevel();
                if (StringUtils.isNotEmpty(roadlevel)) {
                    mapPoint.setRoadlevel(roadlevel.trim());
                }
                String lightdistribution = kafkaMessage.getLightdistribution();
                if (StringUtils.isNotEmpty(lightdistribution)) {
                    mapPoint.setLightdistribution(lightdistribution.trim());
                }
                String lighttype = kafkaMessage.getLighttype();
                if (StringUtils.isNotEmpty(lighttype)) {
                    mapPoint.setLighttype(lighttype.trim());
                }
                boolean dataOk = kafkaMessage.isDataOk();
                if (dataOk) {
                    mapPoint.setIsDataOk(1);
                } else {
                    mapPoint.setIsDataOk(0);
                }
                String dataCuuid = kafkaMessage.getDataCuuid();
                if (StringUtils.isNotEmpty(dataCuuid)) {
                    mapPoint.setDataCuuid(dataCuuid.trim());
                }
                mapPoint.setLightAverage(new BigDecimal(kafkaMessage.getLightAverage().trim()));
                mapPoint.setLightuniformity(new BigDecimal(kafkaMessage.getLightuniformity().trim()));
                mapPoint.setAveragebrightness(new BigDecimal(kafkaMessage.getAveragebrightness().trim()));
                //判断亮度均匀度问题
                if ("nan".equals(kafkaMessage.getBrightnessuniformity().trim())) {
                    mapPoint.setBrightnessuniformity(new BigDecimal(0));
                } else {
                    mapPoint.setBrightnessuniformity(new BigDecimal(kafkaMessage.getBrightnessuniformity().trim()));
                }
                mapPoint.setMax(new BigDecimal(kafkaMessage.getMax().trim()));
                mapPoint.setMin(new BigDecimal(kafkaMessage.getMin().trim()));
                mapPoint.setLongitudinal(new BigDecimal(kafkaMessage.getLongitudinal().trim()));
                mapPoint.setSpeed(new BigDecimal(kafkaMessage.getSpeed().trim()));
                String compliance = kafkaMessage.getCompliance();
                if (StringUtils.isNotEmpty(compliance)) {
                    mapPoint.setCompliance(compliance.trim());
                }
                String result = kafkaMessage.getResult();
                if (StringUtils.isNotEmpty(result)) {
                    mapPoint.setResult(result.trim());
                }
                //新增道路编号
                String roadCode = kafkaMessage.getRoadMCode();
                if (StringUtils.isNotEmpty(roadCode)) {
                    mapPoint.setRoadMCode(roadCode);
                }
                //判断重复点位不入库
                QueryWrapper<MapPoint> mapPointQueryWrapper = new QueryWrapper<>();
                mapPointQueryWrapper.eq("lon", mapPoint.getLon());
                mapPointQueryWrapper.eq("lat", mapPoint.getLat());
                mapPointQueryWrapper.eq("time", mapPoint.getTime());
                MapPoint mapPointOne = mapPointService.getOne(mapPointQueryWrapper, false);
                if (mapPointOne == null) {
                    if (!Collections.isEmpty(uuids)) {
                        uuids.stream().forEach(uuid -> {
                            mapPoint.setStreetUuid(uuid);
                            mapPointService.save(mapPoint);
                            if (mapPoint.getIsDataOk() == 1) {
                                //有效数据，判断合格性
                                checkPointQualification(mapPoint, uuid);
                                mapPointService.updateById(mapPoint);
                                //存入有效结果数据表
                                MapPointResult mapPointResult = new MapPointResult();
                                BeanUtils.copyProperties(mapPoint, mapPointResult);
                                mapPointResult.setId(null);
                                mapPointResultService.save(mapPointResult);
                            }
                            log.info("------ insert mappoint ok -------");
                        });
                    } else {
                        mapPointService.save(mapPoint);
                        if (mapPoint.getIsDataOk() == 1) {
                            //无uuid,但是是有效数据，及道路线框外数据
                            String roadCodeP = mapPoint.getRoadMCode();
                            QueryWrapper<MapPointResult> mapPointResultQueryWrapper = new QueryWrapper<>();
                            mapPointResultQueryWrapper.select("count(1) as c", "street_uuid").groupBy("street_uuid").orderByDesc("c").eq("roadMCode", roadCodeP);
                            List<Map<String, Object>> maps = mapPointResultService.getBaseMapper().selectMaps(mapPointResultQueryWrapper);
                            if (!Collections.isEmpty(maps)) {
                                Map<String, Object> stringObjectMap = maps.get(0);
                                String streetUUid = (String) stringObjectMap.get("street_uuid");
                                //判断合格性
                                checkPointQualification(mapPoint, streetUUid);
                                mapPoint.setStreetUuid(streetUUid);
                                //更新原始数据表
                                mapPointService.updateById(mapPoint);
                                MapPointResult mapPointResult = new MapPointResult();
                                BeanUtils.copyProperties(mapPoint, mapPointResult);
                                mapPointResult.setId(null);
                                mapPointResult.setStreetUuid(streetUUid);
                                //存入有效结果数据表
                                mapPointResultService.save(mapPointResult);
                            }
                        }
                        log.info("------ insert mappoint ok -------");
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("kafka save mappoint error:[{}]", e.toString());
        }
    }

    /**
     * 判断点的合格性
     */
    private MapPoint checkPointQualification(MapPoint mapPoint, String uuid) {
        //有效数据，判断合格性
        QueryWrapper<MapStreet> mapStreetQueryWrapper = new QueryWrapper<>();
        mapStreetQueryWrapper.eq("uuid", uuid);
        MapStreet mapStreet = mapStreetService.getOne(mapStreetQueryWrapper, false);
        if (mapStreet != null) {
            String streetType = mapStreet.getStreetType();
            BigDecimal lightAverage = mapPoint.getLightAverage();
            switch (streetType) {
                case "主干路":
                case "快速路":
                    if (lightAverage.compareTo(new BigDecimal(30)) >= 0) {
                        //合格
                        mapPoint.setCompliance("合格");
                    } else {
                        //偏低
                        mapPoint.setCompliance("偏低");
                    }
                    break;
                case "次干路":
                    if (lightAverage.compareTo(new BigDecimal(20)) >= 0) {
                        //合格
                        mapPoint.setCompliance("合格");
                    } else {
                        //偏低
                        mapPoint.setCompliance("偏低");
                    }
                    break;
                case "支路":
                    if (lightAverage.compareTo(new BigDecimal(10)) >= 0) {
                        //合格
                        mapPoint.setCompliance("合格");
                    } else {
                        //偏低
                        mapPoint.setCompliance("偏低");
                    }
                    break;
            }
        }
        return mapPoint;
    }
}
