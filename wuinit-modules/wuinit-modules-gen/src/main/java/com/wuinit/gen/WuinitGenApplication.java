package com.wuinit.gen;

import com.wuinit.common.security.annotation.EnableCustomConfig;
import com.wuinit.common.security.annotation.EnableRyFeignClients;
import com.wuinit.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 代码生成
 * 
 * @author wuinit
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class WuinitGenApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(WuinitGenApplication.class, args);
        System.out.println("======= wuinit gen start =======");
    }
}
