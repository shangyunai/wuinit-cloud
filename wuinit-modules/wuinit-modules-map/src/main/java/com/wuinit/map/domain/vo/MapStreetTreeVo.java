package com.wuinit.map.domain.vo;

import com.wuinit.map.domain.entity.MapStreet;
import lombok.Data;

import java.util.List;

@Data
public class MapStreetTreeVo {
    private String name;
    private String type;
    private List<Street> streets;

    @Data
    public static class Street {
        private String name;
        private String type;
        private MapStreet mapStreet;
    }
}
