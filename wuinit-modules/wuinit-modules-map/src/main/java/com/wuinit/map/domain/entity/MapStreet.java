package com.wuinit.map.domain.entity;

import com.wuinit.common.core.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 地图街道对象 map_street
 *
 * @author wuinit
 * @date 2023-09-30
 */
public class MapStreet {
    private static final long serialVersionUID = 1L;

    /**
     * 应用ID
     */
    private Long id;
    /**
     * 道路唯一标识
     */
    private String uuid;
    /**
     * 省
     */
    @Excel(name = "省")
    private String province;

    /**
     * 市
     */
    @Excel(name = "市")
    private String city;

    /**
     * 区
     */
    @Excel(name = "区")
    private String region;

    /**
     * 道路名称
     */
    @Excel(name = "道路名称")
    private String streetName;

    /**
     * 道路等级
     */
    @Excel(name = "道路等级")
    private String streetLevel;

    /**
     * 道路描述
     */
    @Excel(name = "道路描述")
    private String streetDecs;

    /**
     * 道路类型
     */
    @Excel(name = "道路类型")
    private String streetType;

    /**
     * 道路状态
     */
    @Excel(name = "道路状态")
    private String streetStatus;

    /**
     * 道路中心点
     */
    @Excel(name = "道路中心点")
    private String streetCenter;

    /**
     * 道路起点
     */
    @Excel(name = "道路起点")
    private String streetStart;

    /**
     * 道路终点
     */
    @Excel(name = "道路终点")
    private String streetEnd;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getProvince() {
        return province;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetLevel(String streetLevel) {
        this.streetLevel = streetLevel;
    }

    public String getStreetLevel() {
        return streetLevel;
    }

    public void setStreetDecs(String streetDecs) {
        this.streetDecs = streetDecs;
    }

    public String getStreetDecs() {
        return streetDecs;
    }

    public void setStreetType(String streetType) {
        this.streetType = streetType;
    }

    public String getStreetType() {
        return streetType;
    }

    public void setStreetStatus(String streetStatus) {
        this.streetStatus = streetStatus;
    }

    public String getStreetStatus() {
        return streetStatus;
    }

    public void setStreetCenter(String streetCenter) {
        this.streetCenter = streetCenter;
    }

    public String getStreetCenter() {
        return streetCenter;
    }

    public void setStreetStart(String streetStart) {
        this.streetStart = streetStart;
    }

    public String getStreetStart() {
        return streetStart;
    }

    public void setStreetEnd(String streetEnd) {
        this.streetEnd = streetEnd;
    }

    public String getStreetEnd() {
        return streetEnd;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("province", getProvince())
                .append("city", getCity())
                .append("region", getRegion())
                .append("streetName", getStreetName())
                .append("streetLevel", getStreetLevel())
                .append("streetDecs", getStreetDecs())
                .append("streetType", getStreetType())
                .append("streetStatus", getStreetStatus())
                .append("streetCenter", getStreetCenter())
                .append("streetStart", getStreetStart())
                .append("streetEnd", getStreetEnd())
                .toString();
    }
}
