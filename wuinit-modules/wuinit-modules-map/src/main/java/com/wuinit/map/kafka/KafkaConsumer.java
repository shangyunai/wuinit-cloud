package com.wuinit.map.kafka;

import com.alibaba.nacos.shaded.com.google.gson.Gson;
import com.wuinit.common.core.utils.DateUtils;
import com.wuinit.common.core.utils.StringUtils;
import com.wuinit.map.api.MapApi;
import com.wuinit.map.config.GeoserverConfig;
import com.wuinit.map.domain.entity.MapPoint;
import com.wuinit.map.domain.entity.MapStreet;
import com.wuinit.map.domain.result.MapFeatureResult;
import com.wuinit.map.service.IMapPointService;
import com.wuinit.map.service.IMapStreetService;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;

@Component
@Slf4j
public class KafkaConsumer {
    @Autowired
    private IMapPointService mapPointService;
    @Autowired
    private IMapStreetService mapStreetService;
    @Autowired
    private GeoserverConfig geoserverConfig;
    @Autowired
    private RestTemplate restTemplate;

    //监听消费
    @KafkaListener(topics = {"test_topic_name"})
    public void onMessage(ConsumerRecord<String, Object> record) {
        log.info("消费：" + record.topic() + "-" + record.partition() + "=" + record.value());
        // TODO: 2023/10/10 保存点数据
        String value = record.value().toString();
        log.info("---value----:{}", value);
        try {
            KafkaMessage kafkaMessage = new Gson().fromJson(value, KafkaMessage.class);
            String lon = kafkaMessage.getLon();
            String lat = kafkaMessage.getLat();
            if (StringUtils.isNotEmpty(lon) && StringUtils.isNotEmpty(lat)) {
                //查询属于那条街道
                String uuid = null;
                String point = "POINT(" + lon.trim() + " " + lat.trim() + ")";
                String streetByPoint = MapApi.getInstance().getStreetByPoint(point, geoserverConfig, restTemplate);
                if (streetByPoint != null) {
                    MapFeatureResult mapFeatureResult = new Gson().fromJson(streetByPoint, MapFeatureResult.class);
                    if (mapFeatureResult != null) {
                        List<MapFeatureResult.FeaturesDTO> features = mapFeatureResult.getFeatures();
                        if (features.size() > 0) {
                            MapFeatureResult.FeaturesDTO featuresDTO = features.get(0);
                            MapFeatureResult.FeaturesDTO.PropertiesDTO properties = featuresDTO.getProperties();
                            if (properties != null) {
                                uuid = properties.getUuid();
                            }
                        } else {
                            String streetByPointContains = MapApi.getInstance().getStreetByPointContains(point, geoserverConfig, restTemplate);
                            if (streetByPointContains != null) {
                                MapFeatureResult mapFeatureResultContains = new Gson().fromJson(streetByPointContains, MapFeatureResult.class);
                                if (mapFeatureResultContains != null) {
                                    List<MapFeatureResult.FeaturesDTO> featuresContains = mapFeatureResultContains.getFeatures();
                                    if (featuresContains.size() > 0) {
                                        MapFeatureResult.FeaturesDTO featuresDTO = features.get(0);
                                        MapFeatureResult.FeaturesDTO.PropertiesDTO properties = featuresDTO.getProperties();
                                        if (properties != null) {
                                            uuid = properties.getUuid();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                MapPoint mapPoint = new MapPoint();
                mapPoint.setTime(Long.valueOf(kafkaMessage.getTime().trim()));
                if (StringUtils.isNotEmpty(uuid)) {
                    mapPoint.setStreetUuid(uuid);
                    //设置道路已检测
//                    MapStreet mapStreet = mapStreetService.selectMapStreetByUuid(uuid);
//                    if (mapStreet != null) {
//                        mapStreet.setStreetStatus("已检测");
//                        mapStreetService.updateMapStreet(mapStreet);
//                    }
                }
                mapPoint.setLon(new BigDecimal(kafkaMessage.getLon().trim()));
                mapPoint.setLat(new BigDecimal(kafkaMessage.getLat().trim()));
                String region = kafkaMessage.getRegion();
                if (StringUtils.isNotEmpty(region)) {
                    mapPoint.setRegion(region.trim());
                }
                String roadname = kafkaMessage.getRoadname();
                if (StringUtils.isNotEmpty(roadname)) {
                    mapPoint.setRoadname(roadname.trim());
                }
                String roadlevel = kafkaMessage.getRoadlevel();
                if (StringUtils.isNotEmpty(roadlevel)) {
                    mapPoint.setRoadlevel(roadlevel.trim());
                }
                String lightdistribution = kafkaMessage.getLightdistribution();
                if (StringUtils.isNotEmpty(lightdistribution)) {
                    mapPoint.setLightdistribution(lightdistribution.trim());
                }
                String lighttype = kafkaMessage.getLighttype();
                if (StringUtils.isNotEmpty(lighttype)) {
                    mapPoint.setLighttype(lighttype.trim());
                }
                boolean dataOk = kafkaMessage.isDataOk();
                if (dataOk) {
                    mapPoint.setIsDataOk(1);
                } else {
                    mapPoint.setIsDataOk(0);
                }
                String dataCuuid = kafkaMessage.getDataCuuid();
                if (StringUtils.isNotEmpty(dataCuuid)) {
                    mapPoint.setDataCuuid(dataCuuid.trim());
                }
                mapPoint.setLightAverage(new BigDecimal(kafkaMessage.getLightAverage().trim()));
                mapPoint.setLightuniformity(new BigDecimal(kafkaMessage.getLightuniformity().trim()));
                mapPoint.setAveragebrightness(new BigDecimal(kafkaMessage.getAveragebrightness().trim()));
                mapPoint.setBrightnessuniformity(new BigDecimal(kafkaMessage.getBrightnessuniformity().trim()));
                mapPoint.setMax(new BigDecimal(kafkaMessage.getMax().trim()));
                mapPoint.setMin(new BigDecimal(kafkaMessage.getMin().trim()));
                mapPoint.setLongitudinal(new BigDecimal(kafkaMessage.getLongitudinal().trim()));
                mapPoint.setSpeed(new BigDecimal(kafkaMessage.getSpeed().trim()));
                String compliance = kafkaMessage.getCompliance();
                if (StringUtils.isNotEmpty(compliance)) {
                    mapPoint.setCompliance(compliance.trim());
                }
                String result = kafkaMessage.getResult();
                if (StringUtils.isNotEmpty(result)) {
                    mapPoint.setResult(result.trim());
                }
                //判断重复点位不入库
                MapPoint mapPointQuery = new MapPoint();
                mapPointQuery.setLon(mapPoint.getLon());
                mapPointQuery.setLat(mapPoint.getLat());
                mapPointQuery.setTime(mapPoint.getTime());
                List<MapPoint> mapPoints = mapPointService.selectMapPointList(mapPointQuery);
                if (Collections.isEmpty(mapPoints)) {
                    mapPointService.insertMapPoint(mapPoint);
                    log.info("------ insert mappoint ok -------");
                }
            }

        } catch (Exception e) {
            log.error("kafka get mappoint error:[{}]", e.toString());
            e.printStackTrace();
        }

    }
}
