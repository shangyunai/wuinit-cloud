package com.wuinit.map.service.impl;

import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.map.domain.entity.MapPoint;
import com.wuinit.map.domain.form.PointBlindingImageForm;
import com.wuinit.map.mapper.MapPointMapper;
import com.wuinit.map.service.IMapPointService;
import com.wuinit.system.api.RemoteFileService;
import io.jsonwebtoken.lang.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 地图点位Service业务层处理
 *
 * @author wuinit
 * @date 2023-10-10
 */
@Service
public class MapPointServiceImpl implements IMapPointService {
    @Autowired
    private MapPointMapper mapPointMapper;
    @Autowired
    private RemoteFileService remoteFileService;

    /**
     * 查询地图点位
     *
     * @param id 地图点位主键
     * @return 地图点位
     */
    @Override
    public MapPoint selectMapPointById(Long id) {
        return mapPointMapper.selectMapPointById(id);
    }

    /**
     * 查询地图点位列表
     *
     * @param mapPoint 地图点位
     * @return 地图点位
     */
    @Override
    public List<MapPoint> selectMapPointList(MapPoint mapPoint) {
        return mapPointMapper.selectMapPointList(mapPoint);
    }

    @Override
    public List<List<BigDecimal>> selectMapPointListCheck(MapPoint mapPoint) {
        List<MapPoint> mapPoints = mapPointMapper.selectMapPointListCheck(mapPoint);
        List<List<BigDecimal>> linePointDatas = new ArrayList<>();
        mapPoints.stream().forEach(item -> {
            ArrayList<BigDecimal> point = new ArrayList<>();
            point.add(item.getLon());
            point.add(item.getLat());
            linePointDatas.add(point);
        });
        return linePointDatas;
    }

    /**
     * 新增地图点位
     *
     * @param mapPoint 地图点位
     * @return 结果
     */
    @Override
    public int insertMapPoint(MapPoint mapPoint) {
        return mapPointMapper.insertMapPoint(mapPoint);
    }

    /**
     * 修改地图点位
     *
     * @param mapPoint 地图点位
     * @return 结果
     */
    @Override
    public int updateMapPoint(MapPoint mapPoint) {
        return mapPointMapper.updateMapPoint(mapPoint);
    }

    /**
     * 批量删除地图点位
     *
     * @param ids 需要删除的地图点位主键
     * @return 结果
     */
    @Override
    public int deleteMapPointByIds(Long[] ids) {
        return mapPointMapper.deleteMapPointByIds(ids);
    }

    /**
     * 删除地图点位信息
     *
     * @param id 地图点位主键
     * @return 结果
     */
    @Override
    public int deleteMapPointById(Long id) {
        return mapPointMapper.deleteMapPointById(id);
    }
    
    public AjaxResult blindingImage(PointBlindingImageForm pointBlindingImageForm) {
        MapPoint mapPoint = new MapPoint();
        mapPoint.setTime(pointBlindingImageForm.getTime());
        List<MapPoint> mapPoints = mapPointMapper.selectMapPointList(mapPoint);
        if (!Collections.isEmpty(mapPoints)) {
            //base64 转 file
            //remoteFileService.upload()

        }
        return AjaxResult.error("图片上传失败");
    }
}
