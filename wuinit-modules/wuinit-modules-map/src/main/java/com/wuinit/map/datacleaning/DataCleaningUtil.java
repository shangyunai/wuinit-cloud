package com.wuinit.map.datacleaning;

import com.alibaba.nacos.shaded.com.google.gson.Gson;
import com.wuinit.map.api.MapApi;
import com.wuinit.map.config.GeoserverConfig;
import com.wuinit.map.domain.entity.MapPoint;
import com.wuinit.map.domain.entity.MapStreet;
import com.wuinit.map.domain.result.MapFeatureResult;
import com.wuinit.map.mapper.MapPointMapper;
import com.wuinit.map.mapper.MapStreetMapper;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;

@Slf4j
@Component
public class DataCleaningUtil {
    @Autowired
    private MapPointMapper mapPointMapper;
    @Autowired
    private MapStreetMapper mapStreetMapper;
    @Autowired
    private GeoserverConfig geoserverConfig;
    @Autowired
    private RestTemplate restTemplate;
    /**
     * 道路采集点时间最长间隔 10s
     */
    private static final long TIME_INTERVAL = 20l;

    public void dataCleaning() {
        //查询所有道路信息
        List<MapStreet> mapStreets = mapStreetMapper.selectMapStreetList(new MapStreet());
        mapStreets.stream().forEach(street -> {
            List<MapPoint> records = mapPointMapper.selectMapPointList(new MapPoint());
            if (!Collections.isEmpty(records)) {
                records.stream().forEach(point -> {
                    //判断检测点位是否合格
                    String streetUuid = point.getStreetUuid();
                    MapStreet mapStreet = mapStreetMapper.selectMapStreetByUUID(streetUuid);
                    if (mapStreet != null) {
                        String streetType = mapStreet.getStreetType();
                        //判断道路级别
                        BigDecimal lightAverage = point.getLightAverage();
                        switch (streetType) {
                            case "主干路":
                            case "快速路":
                                if (lightAverage.compareTo(new BigDecimal(30)) >= 0) {
                                    //合格
                                    point.setCompliance("合格");
                                } else {
                                    //偏低
                                    point.setCompliance("偏低");
                                }
                                break;
                            case "次干路":
                                if (lightAverage.compareTo(new BigDecimal(20)) >= 0) {
                                    //合格
                                    point.setCompliance("合格");
                                } else {
                                    //偏低
                                    point.setCompliance("偏低");
                                }
                                break;
                            case "支路":
                                if (lightAverage.compareTo(new BigDecimal(10)) >= 0) {
                                    //合格
                                    point.setCompliance("合格");
                                } else {
                                    //偏低
                                    point.setCompliance("偏低");
                                }
                                break;
                        }
                    }
                    mapPointMapper.updateMapPoint(point);
                });
            }
        });
    }

    /**
     * 补全道路缺失的道路框外点
     */
    public void fixLinePoint() {
        List<MapStreet> mapStreets = mapStreetMapper.selectMapStreetList(new MapStreet());
        for (MapStreet street : mapStreets) {
            //获取道路所有点
            MapPoint mapPoint = new MapPoint();
            mapPoint.setStreetUuid(street.getUuid());
            List<MapPoint> mapPoints = mapPointMapper.selectMapPointList(mapPoint);
            long pointTime = 0l;
            for (MapPoint point : mapPoints) {
                if (pointTime != 0) {
                    //实际间隔秒数
                    long time = point.getTime();
                    long interval = pointTime - time;
                    if (interval <= TIME_INTERVAL && interval > 1) {
                        for (int i = 1; i < interval; i++) {
                            MapPoint mapPoint1 = new MapPoint();
                            mapPoint1.setTime(time + interval);
                            List<MapPoint> mapPoints1 = mapPointMapper.selectMapPointList(mapPoint1);
                            mapPoints1.stream().forEach(item -> {
                                item.setStreetUuid(point.getStreetUuid());
                                mapPointMapper.insertMapPointSlave(item);
                                log.info("-----补充一条数据--------");
                            });
                        }
                    }
                }
                pointTime = point.getTime();
            }
        }


    }

    /**
     * 补充交叉点
     */
    public void completeXLinePoints() {
        //查询所有已有道路属性的点
        List<MapPoint> list = mapPointMapper.selectMapPointList(new MapPoint());
        log.info("查询到已有道路属性的点位数量：[{}]", list.size());
        list.stream().forEach(point -> {
            boolean isUuid = false;
            long startTime = System.currentTimeMillis();
            //查询属于那条街道
            String pointStr = "POINT(" + point.getLon() + " " + point.getLat() + ")";
            String streetByPoint = MapApi.getInstance().getStreetByPoint(pointStr, geoserverConfig, restTemplate);
            if (streetByPoint != null) {
                MapFeatureResult mapFeatureResult = new Gson().fromJson(streetByPoint, MapFeatureResult.class);
                if (mapFeatureResult != null) {
                    List<MapFeatureResult.FeaturesDTO> features = mapFeatureResult.getFeatures();
                    if (!Collections.isEmpty(features)) {
                        isUuid = true;
                        features.stream().forEach(feature -> {
                            MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                            if (properties != null) {
                                String pUuid = properties.getUuid();
                                //新增检测点位
                                point.setStreetUuid(pUuid);
                                point.setId(null);
                                MapPoint mapPoint = new MapPoint();
                                BeanUtils.copyProperties(point, mapPoint);
                                mapPointMapper.insertMapPointSlave(mapPoint);
                            }
                        });
                    } else {
                        String streetByPointContains = MapApi.getInstance().getStreetByPointContains(pointStr, geoserverConfig, restTemplate);
                        if (streetByPointContains != null) {
                            MapFeatureResult mapFeatureResultContains = new Gson().fromJson(streetByPointContains, MapFeatureResult.class);
                            if (mapFeatureResultContains != null) {
                                List<MapFeatureResult.FeaturesDTO> featuresContains = mapFeatureResult.getFeatures();
                                if (!Collections.isEmpty(featuresContains)) {
                                    isUuid = true;
                                    featuresContains.stream().forEach(feature -> {
                                        MapFeatureResult.FeaturesDTO.PropertiesDTO properties = feature.getProperties();
                                        if (properties != null) {
                                            String pUuid = properties.getUuid();
                                            //新增检测点位
                                            point.setStreetUuid(pUuid);
                                            point.setId(null);
                                            MapPoint mapPoint = new MapPoint();
                                            BeanUtils.copyProperties(point, mapPoint);
                                            mapPointMapper.insertMapPointSlave(mapPoint);
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
            }
            if (!isUuid) {
                //未找到道路属性点位
                point.setId(null);
                mapPointMapper.insertMapPointSlave(point);
            }
            long endTime = System.currentTimeMillis();
            log.info("补全交叉线测量点位，处理一条耗时：[{}]ms", endTime - startTime);
        });

    }
}
