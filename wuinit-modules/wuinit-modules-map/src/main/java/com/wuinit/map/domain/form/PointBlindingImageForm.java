package com.wuinit.map.domain.form;

import lombok.Data;

@Data
public class PointBlindingImageForm {
    /**
     * 时间戳
     */
    private Long time;
    /**
     * 图片base64编码
     */
    private String imageBase64;
    //扩展设备id
    private String deviceId;
}
