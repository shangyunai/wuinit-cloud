package com.wuinit.map.controller;

import com.wuinit.common.core.web.controller.BaseController;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.common.core.web.page.TableDataInfo;
import com.wuinit.map.domain.entity.MapPoint;
import com.wuinit.map.service.IMapPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 地图点Controller
 *
 * @author wuinit
 * @date 2023-09-30
 */
@RestController
@RequestMapping("/point")
public class MapPointController extends BaseController {
    @Autowired
    private IMapPointService mapPointService;

    /**
     * 查询地图所有点集合
     */
    @GetMapping("/all")
    public AjaxResult all(MapPoint mapPoint) {
        List<MapPoint> list = mapPointService.selectMapPointList(mapPoint);
        return AjaxResult.success(list);
    }

    /**
     * 查询地图所有点集合
     */
    @GetMapping("/all/check")
    public AjaxResult allCheck(MapPoint mapPoint) {
        List<List<BigDecimal>> list = mapPointService.selectMapPointListCheck(mapPoint);
        return AjaxResult.success(list);
    }

    /**
     * 分页查询点集合
     *
     * @param mapPoint
     * @return
     */
    @GetMapping("/list")
    public TableDataInfo list(MapPoint mapPoint) {
        startPage();
        List<MapPoint> list = mapPointService.selectMapPointList(mapPoint);
        return getDataTable(list);
    }

    /**
     * 查询地图所有点集合
     */
    @PostMapping("/by/uuid")
    public AjaxResult getByUuid(@RequestBody MapPoint mapPoint) {
        List<MapPoint> list = mapPointService.selectMapPointList(mapPoint);
        return AjaxResult.success(list);
    }
}

