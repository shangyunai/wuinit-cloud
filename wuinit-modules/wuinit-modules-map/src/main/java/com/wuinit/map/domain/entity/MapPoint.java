package com.wuinit.map.domain.entity;

import com.wuinit.common.core.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 地图点位对象 map_point
 *
 * @author wuinit
 * @date 2023-10-17
 */
public class MapPoint
{
    private static final long serialVersionUID = 1L;

    /** 应用ID */
    private Long id;

    /** 经度 */
    @Excel(name = "经度")
    private BigDecimal lon;

    /** 纬度 */
    @Excel(name = "纬度")
    private BigDecimal lat;

    /** 街道uuid */
    @Excel(name = "街道uuid")
    private String streetUuid;

    /** 测量时间戳 */
    @Excel(name = "测量时间戳")
    private Long time;

    /** 区域名称 */
    @Excel(name = "区域名称")
    private String Region;

    /** 道路名称 */
    @Excel(name = "道路名称")
    private String Roadname;

    /** 道路等级 */
    @Excel(name = "道路等级")
    private String Roadlevel;

    /** 路灯分布 */
    @Excel(name = "路灯分布")
    private String lightdistribution;

    /** 路灯类型 */
    @Excel(name = "路灯类型")
    private String lighttype;

    /** 平均照度 */
    @Excel(name = "平均照度")
    private BigDecimal lightAverage;

    /** 照度均匀度 */
    @Excel(name = "照度均匀度")
    private BigDecimal lightuniformity;

    /** 平均亮度 */
    @Excel(name = "平均亮度")
    private BigDecimal Averagebrightness;

    /** 亮度均匀度 */
    @Excel(name = "亮度均匀度")
    private BigDecimal Brightnessuniformity;

    /** 照度最大值 */
    @Excel(name = "照度最大值")
    private BigDecimal max;

    /** 照度最小值 */
    @Excel(name = "照度最小值")
    private BigDecimal min;

    /** 纵向均匀度 */
    @Excel(name = "纵向均匀度")
    private BigDecimal Longitudinal;

    /** 测量速度 */
    @Excel(name = "测量速度")
    private BigDecimal speed;

    /** 道路是否达标 达标/ 不合格 */
    @Excel(name = "道路是否达标 达标/ 不合格")
    private String Compliance;

    /** 陡增减结果 正常/陡增/陡降 */
    @Excel(name = "陡增减结果 正常/陡增/陡降")
    private String result;

    /** 是否是有效数据 0 无效 1 有效 */
    @Excel(name = "是否是有效数据 0 无效 1 有效")
    private Integer isDataOk;

    /** 检测道路uuid */
    @Excel(name = "检测道路uuid")
    private String dataCuuid;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setLon(BigDecimal lon)
    {
        this.lon = lon;
    }

    public BigDecimal getLon()
    {
        return lon;
    }
    public void setLat(BigDecimal lat)
    {
        this.lat = lat;
    }

    public BigDecimal getLat()
    {
        return lat;
    }
    public void setStreetUuid(String streetUuid)
    {
        this.streetUuid = streetUuid;
    }

    public String getStreetUuid()
    {
        return streetUuid;
    }
    public void setTime(Long time)
    {
        this.time = time;
    }

    public Long getTime()
    {
        return time;
    }
    public void setRegion(String Region)
    {
        this.Region = Region;
    }

    public String getRegion()
    {
        return Region;
    }
    public void setRoadname(String Roadname)
    {
        this.Roadname = Roadname;
    }

    public String getRoadname()
    {
        return Roadname;
    }
    public void setRoadlevel(String Roadlevel)
    {
        this.Roadlevel = Roadlevel;
    }

    public String getRoadlevel()
    {
        return Roadlevel;
    }
    public void setLightdistribution(String lightdistribution)
    {
        this.lightdistribution = lightdistribution;
    }

    public String getLightdistribution()
    {
        return lightdistribution;
    }
    public void setLighttype(String lighttype)
    {
        this.lighttype = lighttype;
    }

    public String getLighttype()
    {
        return lighttype;
    }
    public void setLightAverage(BigDecimal lightAverage)
    {
        this.lightAverage = lightAverage;
    }

    public BigDecimal getLightAverage()
    {
        return lightAverage;
    }
    public void setLightuniformity(BigDecimal lightuniformity)
    {
        this.lightuniformity = lightuniformity;
    }

    public BigDecimal getLightuniformity()
    {
        return lightuniformity;
    }
    public void setAveragebrightness(BigDecimal Averagebrightness)
    {
        this.Averagebrightness = Averagebrightness;
    }

    public BigDecimal getAveragebrightness()
    {
        return Averagebrightness;
    }
    public void setBrightnessuniformity(BigDecimal Brightnessuniformity)
    {
        this.Brightnessuniformity = Brightnessuniformity;
    }

    public BigDecimal getBrightnessuniformity()
    {
        return Brightnessuniformity;
    }
    public void setMax(BigDecimal max)
    {
        this.max = max;
    }

    public BigDecimal getMax()
    {
        return max;
    }
    public void setMin(BigDecimal min)
    {
        this.min = min;
    }

    public BigDecimal getMin()
    {
        return min;
    }
    public void setLongitudinal(BigDecimal Longitudinal)
    {
        this.Longitudinal = Longitudinal;
    }

    public BigDecimal getLongitudinal()
    {
        return Longitudinal;
    }
    public void setSpeed(BigDecimal speed)
    {
        this.speed = speed;
    }

    public BigDecimal getSpeed()
    {
        return speed;
    }
    public void setCompliance(String Compliance)
    {
        this.Compliance = Compliance;
    }

    public String getCompliance()
    {
        return Compliance;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    public String getResult()
    {
        return result;
    }
    public void setIsDataOk(Integer isDataOk)
    {
        this.isDataOk = isDataOk;
    }

    public Integer getIsDataOk()
    {
        return isDataOk;
    }
    public void setDataCuuid(String dataCuuid)
    {
        this.dataCuuid = dataCuuid;
    }

    public String getDataCuuid()
    {
        return dataCuuid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("lon", getLon())
                .append("lat", getLat())
                .append("streetUuid", getStreetUuid())
                .append("time", getTime())
                .append("Region", getRegion())
                .append("Roadname", getRoadname())
                .append("Roadlevel", getRoadlevel())
                .append("lightdistribution", getLightdistribution())
                .append("lighttype", getLighttype())
                .append("lightAverage", getLightAverage())
                .append("lightuniformity", getLightuniformity())
                .append("Averagebrightness", getAveragebrightness())
                .append("Brightnessuniformity", getBrightnessuniformity())
                .append("max", getMax())
                .append("min", getMin())
                .append("Longitudinal", getLongitudinal())
                .append("speed", getSpeed())
                .append("Compliance", getCompliance())
                .append("result", getResult())
                .append("isDataOk", getIsDataOk())
                .append("dataCuuid", getDataCuuid())
                .toString();
    }
}
