package com.wuinit.map.mapper;

import com.wuinit.map.domain.entity.MapStreet;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 地图街道Mapper接口
 *
 * @author syyt
 * @date 2023-09-30
 */
@Repository
public interface MapStreetMapper {
    /**
     * 查询地图街道
     *
     * @param id 地图街道主键
     * @return 地图街道
     */
    public MapStreet selectMapStreetById(Long id);

    /**
     * 查询地图街道
     *
     * @param uuid 地图街道唯一标识
     * @return 地图街道
     */
    public MapStreet selectMapStreetByUUID(String uuid);

    /**
     * 查询地图街道列表
     *
     * @param mapStreet 地图街道
     * @return 地图街道集合
     */
    public List<MapStreet> selectMapStreetList(MapStreet mapStreet);

    /**
     * 新增地图街道
     *
     * @param mapStreet 地图街道
     * @return 结果
     */
    public int insertMapStreet(MapStreet mapStreet);

    /**
     * 修改地图街道
     *
     * @param mapStreet 地图街道
     * @return 结果
     */
    public int updateMapStreet(MapStreet mapStreet);

    /**
     * 删除地图街道
     *
     * @param id 地图街道主键
     * @return 结果
     */
    public int deleteMapStreetById(Long id);
    /**
     * 删除地图街道
     *
     * @param uuid 地图街道唯一标识符
     * @return 结果
     */
    public int deleteMapStreetByUUID(String uuid);
    /**
     * 批量删除地图街道
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMapStreetByIds(Long[] ids);
}
