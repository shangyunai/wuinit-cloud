package com.wuinit.map.service;

import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.map.domain.entity.MapStreet;
import com.wuinit.map.domain.form.MapStreetForm;

import java.util.List;

/**
 * 地图街道Service接口
 *
 * @author wuinit
 * @date 2023-09-30
 */
public interface IMapStreetService {
    /**
     * 查询地图街道
     *
     * @param id 地图街道主键
     * @return 地图街道
     */
    public MapStreet selectMapStreetById(Long id);

    /**
     * 查询地图街道
     *
     * @param uuid 地图街道主键
     * @return 地图街道
     */
    public MapStreet selectMapStreetByUuid(String uuid);

    /**
     * 查询地图街道列表
     *
     * @param mapStreet 地图街道
     * @return 地图街道集合
     */
    public List<MapStreet> selectMapStreetList(MapStreet mapStreet);

    /**
     * 新增地图街道
     *
     * @param mapStreet 地图街道
     * @return 结果
     */
    public int insertMapStreet(MapStreet mapStreet);

    /**
     * 修改地图街道
     *
     * @param mapStreet 地图街道
     * @return 结果
     */
    public int updateMapStreet(MapStreet mapStreet);

    /**
     * 批量删除地图街道
     *
     * @param ids 需要删除的地图街道主键集合
     * @return 结果
     */
    public int deleteMapStreetByIds(Long[] ids);

    /**
     * 删除地图街道信息
     *
     * @param id 地图街道主键
     * @return 结果
     */
    public int deleteMapStreetById(Long id);

    /**
     * 地图要素编辑 WFS
     *
     * @param mapStreetForm
     * @return
     */
    public AjaxResult geoserverWfs(MapStreetForm mapStreetForm);

    /**
     * 地图街道UUID
     *
     * @return
     */
    public AjaxResult getUUID();

    /**
     * 获取地图道路列表树
     *
     * @return
     */
    public AjaxResult getStreetTree();

    /**
     * 获取geoserver
     *
     * @return
     */
    public AjaxResult getGeoserverConfig();

    /**
     * 获取道路地图
     *
     * @return
     */
    public String getGeoserverStreetMap();

    /**
     * 根据点获取道路信息
     *
     * @return
     */
    public String getStreetByPoint(String pointStr);

    /**
     * 同步跟新道路检测状态，是否检测
     *
     * @return
     */
    public AjaxResult updateStreetStatus();

    /**
     * kafka 下发道路信息
     *
     * @return
     */
    public AjaxResult sendKafka(String uuid);

    /**
     * kafka 通讯检测状态
     *
     * @return
     */
    public AjaxResult sendKafkaCheckStatus(String status);
}
