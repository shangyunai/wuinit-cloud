package com.wuinit.map.domain.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class MapFeatureResult {

    @JsonProperty("type")
    private String type;
    @JsonProperty("features")
    private List<FeaturesDTO> features;
    @JsonProperty("totalFeatures")
    private Integer totalFeatures;
    @JsonProperty("numberMatched")
    private Integer numberMatched;
    @JsonProperty("numberReturned")
    private Integer numberReturned;
    @JsonProperty("timeStamp")
    private String timeStamp;
    @JsonProperty("crs")
    private CrsDTO crs;

    @NoArgsConstructor
    @Data
    public static class CrsDTO {
        @JsonProperty("type")
        private String type;
        @JsonProperty("properties")
        private PropertiesDTO properties;

        @NoArgsConstructor
        @Data
        public static class PropertiesDTO {
            @JsonProperty("name")
            private String name;
        }
    }

    @NoArgsConstructor
    @Data
    public static class FeaturesDTO {
        @JsonProperty("type")
        private String type;
        @JsonProperty("id")
        private String id;
        @JsonProperty("geometry")
        private GeometryDTO geometry;
        @JsonProperty("geometry_name")
        private String geometryName;
        @JsonProperty("properties")
        private PropertiesDTO properties;

        @NoArgsConstructor
        @Data
        public static class GeometryDTO {
            @JsonProperty("type")
            private String type;
            @JsonProperty("coordinates")
            private List<List<List<List<Double>>>> coordinates;
        }

        @NoArgsConstructor
        @Data
        public static class PropertiesDTO {
            @JsonProperty("uuid")
            private String uuid;
            @JsonProperty("province")
            private String province;
            @JsonProperty("city")
            private String city;
            @JsonProperty("region")
            private String region;
            @JsonProperty("name")
            private String name;
            @JsonProperty("level")
            private String level;
            @JsonProperty("decs")
            private Object decs;
            @JsonProperty("type")
            private String type;
            @JsonProperty("center")
            private String center;
            @JsonProperty("start")
            private String start;
            @JsonProperty("end")
            private String end;
        }
    }
}
