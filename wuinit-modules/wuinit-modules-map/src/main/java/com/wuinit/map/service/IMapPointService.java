package com.wuinit.map.service;

import com.wuinit.map.domain.entity.MapPoint;

import java.math.BigDecimal;
import java.util.List;

/**
 * 地图点位Service接口
 *
 * @author wuinit
 * @date 2023-10-10
 */
public interface IMapPointService {
    /**
     * 查询地图点位
     *
     * @param id 地图点位主键
     * @return 地图点位
     */
    public MapPoint selectMapPointById(Long id);

    /**
     * 查询地图点位列表
     *
     * @param mapPoint 地图点位
     * @return 地图点位集合
     */
    public List<MapPoint> selectMapPointList(MapPoint mapPoint);

    /**
     * 查询地图点位列表
     *
     * @param mapPoint 地图点位
     * @return 地图点位集合
     */
    public List<List<BigDecimal>> selectMapPointListCheck(MapPoint mapPoint);

    /**
     * 新增地图点位
     *
     * @param mapPoint 地图点位
     * @return 结果
     */
    public int insertMapPoint(MapPoint mapPoint);

    /**
     * 修改地图点位
     *
     * @param mapPoint 地图点位
     * @return 结果
     */
    public int updateMapPoint(MapPoint mapPoint);

    /**
     * 批量删除地图点位
     *
     * @param ids 需要删除的地图点位主键集合
     * @return 结果
     */
    public int deleteMapPointByIds(Long[] ids);

    /**
     * 删除地图点位信息
     *
     * @param id 地图点位主键
     * @return 结果
     */
    public int deleteMapPointById(Long id);
}
