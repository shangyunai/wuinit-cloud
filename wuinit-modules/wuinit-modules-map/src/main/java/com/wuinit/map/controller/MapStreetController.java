package com.wuinit.map.controller;

import com.wuinit.common.core.utils.poi.ExcelUtil;
import com.wuinit.common.core.web.controller.BaseController;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.common.core.web.page.TableDataInfo;
import com.wuinit.common.log.annotation.Log;
import com.wuinit.common.log.enums.BusinessType;
import com.wuinit.common.security.annotation.RequiresPermissions;
import com.wuinit.map.domain.entity.MapStreet;
import com.wuinit.map.domain.form.MapStreetForm;
import com.wuinit.map.service.IMapStreetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 地图街道Controller
 *
 * @author wuinit
 * @date 2023-09-30
 */
@RestController
@RequestMapping("/street")
public class MapStreetController extends BaseController {
    @Autowired
    private IMapStreetService mapStreetService;

    /**
     * 查询地图街道列表
     */
    //@RequiresPermissions("devops:street:list")
    @GetMapping("/list")
    public TableDataInfo list(MapStreet mapStreet) {
        startPage();
        List<MapStreet> list = mapStreetService.selectMapStreetList(mapStreet);
        return getDataTable(list);
    }

    /**
     * 导出地图街道列表
     */
    //@RequiresPermissions("devops:street:export")
    @Log(title = "地图街道", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MapStreet mapStreet) {
        List<MapStreet> list = mapStreetService.selectMapStreetList(mapStreet);
        ExcelUtil<MapStreet> util = new ExcelUtil<MapStreet>(MapStreet.class);
        util.exportExcel(response, list, "地图街道数据");
    }

    /**
     * 获取地图街道详细信息
     */
    //@RequiresPermissions("devops:street:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(mapStreetService.selectMapStreetById(id));
    }

    /**
     * 新增地图街道
     */
    //@RequiresPermissions("devops:street:add")
    @Log(title = "地图街道", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MapStreet mapStreet) {
        return toAjax(mapStreetService.insertMapStreet(mapStreet));
    }

    /**
     * 修改地图街道
     */
    //@RequiresPermissions("devops:street:edit")
    @Log(title = "地图街道", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MapStreet mapStreet) {
        return toAjax(mapStreetService.updateMapStreet(mapStreet));
    }

    /**
     * 删除地图街道
     */
    //@RequiresPermissions("devops:street:remove")
    @Log(title = "地图街道", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(mapStreetService.deleteMapStreetByIds(ids));
    }

    /**
     * 地图服务WFS 地图要素操作
     *
     * @param mapStreetForm
     * @return
     */
    @PostMapping("/geoserver/wfs")
    public AjaxResult geoserverWfs(@RequestBody MapStreetForm mapStreetForm) {
        return mapStreetService.geoserverWfs(mapStreetForm);
    }

    /**
     * 获取道路唯一标识
     *
     * @return
     */
    @GetMapping("/get/uuid")
    public AjaxResult getUUID() {
        return mapStreetService.getUUID();
    }

    /**
     * 获取道路列表树
     *
     * @return
     */
    @GetMapping("/get/street/tree")
    public AjaxResult getStreetTree() {
        return mapStreetService.getStreetTree();
    }

    /**
     * 获取道路列表树
     *
     * @return
     */
    @GetMapping("/get/geoserver/config")
    public AjaxResult getGeoserverConfig() {
        return mapStreetService.getGeoserverConfig();
    }

    /**
     * 获取道路地图
     *
     * @return
     */
    @GetMapping("/get/geoserver/street/map")
    public String getGeoserverStreetMap() {
        return mapStreetService.getGeoserverStreetMap();
    }

    /**
     * 根据点获取道路信息
     *
     * @return
     */
    @GetMapping("/get/street/by/point")
    public String getStreetByPoint(String pointStr) {
        return mapStreetService.getStreetByPoint(pointStr);
    }

    /**
     * 更新道路检测状态
     *
     * @return
     */
    @GetMapping("/update/street/status")
    public AjaxResult updateStreetStatus() {
        return mapStreetService.updateStreetStatus();
    }

    /**
     * kafka 下发道路信息
     *
     * @param uuid
     * @return
     */
    @GetMapping("/send/kafka")
    public AjaxResult sendKafka(String uuid) {
        return mapStreetService.sendKafka(uuid);
    }

    /**
     * kafka 下发道路信息
     *
     * @param status
     * @return
     */
    @GetMapping("/send/kafka/check/status")
    public AjaxResult sendKafkaCheckStatus(String status) {
        return mapStreetService.sendKafkaCheckStatus(status);
    }
}

