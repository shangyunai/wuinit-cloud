package com.wuinit.map.service.impl;

import com.alibaba.nacos.shaded.com.google.gson.Gson;
import com.wuinit.common.core.utils.uuid.IdUtils;
import com.wuinit.common.core.web.domain.AjaxResult;
import com.wuinit.common.log.enums.BusinessType;
import com.wuinit.map.api.MapApi;
import com.wuinit.map.config.GeoserverConfig;
import com.wuinit.map.domain.entity.MapPoint;
import com.wuinit.map.domain.entity.MapStreet;
import com.wuinit.map.domain.form.MapStreetForm;
import com.wuinit.map.domain.vo.MapStreetTreeVo;
import com.wuinit.map.mapper.MapPointMapper;
import com.wuinit.map.mapper.MapStreetMapper;
import com.wuinit.map.service.IMapStreetService;
import feign.FeignException;
import io.jsonwebtoken.lang.Collections;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.*;

/**
 * 地图街道Service业务层处理
 *
 * @author wuinit
 * @date 2023-09-30
 */
@Service
public class MapStreetServiceImpl implements IMapStreetService {
    @Autowired
    private MapStreetMapper mapStreetMapper;
    @Autowired
    private MapPointMapper mapPointMapper;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private GeoserverConfig geoserverConfig;

    /**
     * 查询地图街道
     *
     * @param id 地图街道主键
     * @return 地图街道
     */
    @Override
    public MapStreet selectMapStreetById(Long id) {
        return mapStreetMapper.selectMapStreetById(id);
    }

    @Override
    public MapStreet selectMapStreetByUuid(String uuid) {
        return selectMapStreetByUuid(uuid);
    }

    /**
     * 查询地图街道列表
     *
     * @param mapStreet 地图街道
     * @return 地图街道
     */
    @Override
    public List<MapStreet> selectMapStreetList(MapStreet mapStreet) {
        return mapStreetMapper.selectMapStreetList(mapStreet);
    }

    /**
     * 新增地图街道
     *
     * @param mapStreet 地图街道
     * @return 结果
     */
    @Override
    public int insertMapStreet(MapStreet mapStreet) {
        return mapStreetMapper.insertMapStreet(mapStreet);
    }

    /**
     * 修改地图街道
     *
     * @param mapStreet 地图街道
     * @return 结果
     */
    @Override
    public int updateMapStreet(MapStreet mapStreet) {
        return mapStreetMapper.updateMapStreet(mapStreet);
    }

    /**
     * 批量删除地图街道
     *
     * @param ids 需要删除的地图街道主键
     * @return 结果
     */
    @Override
    public int deleteMapStreetByIds(Long[] ids) {
        return mapStreetMapper.deleteMapStreetByIds(ids);
    }

    /**
     * 删除地图街道信息
     *
     * @param id 地图街道主键
     * @return 结果
     */
    @Override
    public int deleteMapStreetById(Long id) {
        return mapStreetMapper.deleteMapStreetById(id);
    }

    /**
     * 地图要素操作
     *
     * @param mapStreetForm
     * @return
     */
    @Override
    public AjaxResult geoserverWfs(MapStreetForm mapStreetForm) {
        String optionType = mapStreetForm.getOptionType();
        String textXml = mapStreetForm.getTextXml();
        if (BusinessType.INSERT.name().equals(optionType)) {
            String geoserverWFS = MapApi.getInstance().geoserverWFS(textXml, geoserverConfig, restTemplate);
            if (geoserverWFS.contains("SUCCESS")) {
                MapStreet mapStreet = new MapStreet();
                BeanUtils.copyProperties(mapStreetForm, mapStreet);
                mapStreetMapper.insertMapStreet(mapStreet);
                return AjaxResult.success();
            }
        } else if (BusinessType.UPDATE.name().equals(optionType)) {
            String geoserverWFS = MapApi.getInstance().geoserverWFS(textXml, geoserverConfig, restTemplate);
            if (geoserverWFS.contains("SUCCESS")) {
                String uuid = mapStreetForm.getUuid();
                MapStreet mapStreet = mapStreetMapper.selectMapStreetByUUID(uuid);
                if (mapStreet != null) {
                    BeanUtils.copyProperties(mapStreetForm, mapStreet);
                }
                mapStreetMapper.updateMapStreet(mapStreet);
                return AjaxResult.success();
            }
        } else if (BusinessType.DELETE.name().equals(optionType)) {
            String geoserverWFS = MapApi.getInstance().geoserverWFS(textXml, geoserverConfig, restTemplate);
            if (geoserverWFS.contains("SUCCESS")) {
                String uuid = mapStreetForm.getUuid();
                mapStreetMapper.deleteMapStreetByUUID(uuid);
                return AjaxResult.success();
            }
        }
        return AjaxResult.error();
    }

    @Override
    public AjaxResult getUUID() {
        String uuid = IdUtils.simpleUUID();
        Map<String, String> uuidMap = new HashMap<>();
        uuidMap.put("uuid", uuid);
        return AjaxResult.success(uuidMap);
    }

    @Override
    public AjaxResult getStreetTree() {
        List<MapStreet> mapStreets = mapStreetMapper.selectMapStreetList(new MapStreet());
        List<MapStreetTreeVo> mapStreetTreeVos = new ArrayList<>();
        Map<String, List<MapStreetTreeVo.Street>> treeMap = new HashMap<>();
        mapStreets.stream().forEach(item -> {
            String region = item.getRegion();
            if (!treeMap.containsKey(region)) {
                List<MapStreetTreeVo.Street> streets = new ArrayList<>();
                MapStreetTreeVo.Street street = new MapStreetTreeVo.Street();
                street.setName(item.getStreetName());
                street.setType("street");
                street.setMapStreet(item);
                streets.add(street);
                treeMap.put(region, streets);
            } else {
                List<MapStreetTreeVo.Street> streets = treeMap.get(region);
                MapStreetTreeVo.Street street = new MapStreetTreeVo.Street();
                street.setName(item.getStreetName());
                street.setType("street");
                street.setMapStreet(item);
                streets.add(street);
                treeMap.put(region, streets);
            }
        });
        treeMap.keySet().stream().forEach(item -> {
            MapStreetTreeVo mapStreetTreeVo = new MapStreetTreeVo();
            List<MapStreetTreeVo.Street> streets = treeMap.get(item);
            mapStreetTreeVo.setName(item + "(" + streets.size() + ")");
            mapStreetTreeVo.setType("region");
            mapStreetTreeVo.setStreets(streets);
            mapStreetTreeVos.add(mapStreetTreeVo);
        });
        return AjaxResult.success(mapStreetTreeVos);
    }

    @Override
    public AjaxResult getGeoserverConfig() {
        return AjaxResult.success(geoserverConfig);

    }

    @Override
    public String getGeoserverStreetMap() {
        return MapApi.getInstance().getStreetMap(geoserverConfig, restTemplate);
    }

    @Override
    public String getStreetByPoint(String pointStr) {
        return MapApi.getInstance().getStreetByPoint(pointStr, geoserverConfig, restTemplate);
    }

    @Override
    public AjaxResult updateStreetStatus() {
        List<MapStreet> mapStreets = mapStreetMapper.selectMapStreetList(new MapStreet());
        mapStreets.stream().forEach(item -> {
            MapPoint mapPoint = new MapPoint();
            mapPoint.setStreetUuid(item.getUuid());
            List<MapPoint> mapPoints = mapPointMapper.selectMapPointList(mapPoint);
            if (!Collections.isEmpty(mapPoints)) {
                item.setStreetStatus("已检测");
            } else {
                item.setStreetStatus("未检测");
            }
            mapStreetMapper.updateMapStreet(item);
        });
        return AjaxResult.success();
    }

    /**
     * kafka 下发道路信息 通知前端采集设备
     */
    @Resource
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Override
    public AjaxResult sendKafka(String uuid) {
        MapStreet mapStreet = mapStreetMapper.selectMapStreetByUUID(uuid);
        if (mapStreet != null) {
            String message = new Gson().toJson(mapStreet);
            kafkaTemplate.send("detecting_road", message);
        }
        return AjaxResult.success();
    }

    @Override
    public AjaxResult sendKafkaCheckStatus(String status) {
        HashMap<String, String> statusMap = new HashMap<>();
        statusMap.put("status", status);
        String message = new Gson().toJson(statusMap);
        kafkaTemplate.send("check_state", message);
        return AjaxResult.success();
    }
}
