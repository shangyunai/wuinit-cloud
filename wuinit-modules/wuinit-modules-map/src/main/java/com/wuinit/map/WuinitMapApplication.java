package com.wuinit.map;

import com.wuinit.common.security.annotation.EnableCustomConfig;
import com.wuinit.common.security.annotation.EnableRyFeignClients;
import com.wuinit.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * 地图模块
 *
 * @author wuinit
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class WuinitMapApplication {
    public static void main(String[] args) {
        SpringApplication.run(WuinitMapApplication.class, args);
        System.out.println("======= wuinit map start =======");
    }

    @Bean
    public RestTemplate restTemplate() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(5000);
        factory.setReadTimeout(60000);
        return new RestTemplate(factory);
    }
}
