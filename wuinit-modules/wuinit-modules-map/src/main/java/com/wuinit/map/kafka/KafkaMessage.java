package com.wuinit.map.kafka;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class KafkaMessage {

    @JsonProperty("Region")
    private String Region;
    @JsonProperty("Roadname")
    private String Roadname;
    @JsonProperty("Roadlevel")
    private String Roadlevel;
    @JsonProperty("lightdistribution")
    private String lightdistribution;
    @JsonProperty("lighttype")
    private String lighttype;
    @JsonProperty("lightAverage")
    private String lightAverage;
    @JsonProperty("lightuniformity")
    private String lightuniformity;
    @JsonProperty("Averagebrightness")
    private String Averagebrightness;
    @JsonProperty("Brightnessuniformity")
    private String Brightnessuniformity;
    @JsonProperty("max")
    private String max;
    @JsonProperty("min")
    private String min;
    @JsonProperty("Longitudinal")
    private String Longitudinal;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lon")
    private String lon;
    @JsonProperty("time")
    private String time;
    @JsonProperty("speed")
    private String speed;
    @JsonProperty("Compliance")
    private String Compliance;
    @JsonProperty("result")
    private String result;
    @JsonProperty("isDataOk")
    private boolean isDataOk;
    @JsonProperty("dataCuuid")
    private String dataCuuid;
}
