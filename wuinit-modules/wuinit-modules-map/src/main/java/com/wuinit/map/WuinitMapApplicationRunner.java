package com.wuinit.map;


import com.wuinit.map.datacleaning.DataCleaningUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


@Component
@Order(value = 2)
public class WuinitMapApplicationRunner implements org.springframework.boot.ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(WuinitMapApplicationRunner.class);

    @Autowired
    private DataCleaningUtil dataCleaningUtil;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //dataCleaningUtil.completeXLinePoints();
    }


}
