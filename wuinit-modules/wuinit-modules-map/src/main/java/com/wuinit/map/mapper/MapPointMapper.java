package com.wuinit.map.mapper;

import com.baomidou.dynamic.datasource.annotation.Slave;
import com.wuinit.map.domain.entity.MapPoint;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 地图点位Mapper接口
 *
 * @author wuinit
 * @date 2023-10-17
 */
@Repository
public interface MapPointMapper {
    /**
     * 查询地图点位
     *
     * @param id 地图点位主键
     * @return 地图点位
     */
    public MapPoint selectMapPointById(Long id);
    /**
     * 查询地图点位列表
     *
     * @param mapPoint 地图点位
     * @return 地图点位集合
     */
    public List<MapPoint> selectMapPointList(MapPoint mapPoint);
    /**
     * 查询地图点位列表
     *
     * @param mapPoint 地图点位
     * @return 地图点位集合
     */
    @Slave
    public List<MapPoint> selectMapPointListSlave(MapPoint mapPoint);

    /**
     * 查询地图有效点位列表
     *
     * @param mapPoint 地图点位
     * @return 地图点位集合
     */
    public List<MapPoint> selectMapPointListCheck(MapPoint mapPoint);

    /**
     * 新增地图点位
     *
     * @param mapPoint 地图点位
     * @return 结果
     */
    public int insertMapPoint(MapPoint mapPoint);

    /**
     * 新增地图点位
     *
     * @param mapPoint 地图点位
     * @return 结果
     */
    @Slave
    public int insertMapPointSlave(MapPoint mapPoint);

    /**
     * 修改地图点位
     *
     * @param mapPoint 地图点位
     * @return 结果
     */
    @Slave
    public int updateMapPoint(MapPoint mapPoint);

    /**
     * 删除地图点位
     *
     * @param id 地图点位主键
     * @return 结果
     */
    public int deleteMapPointById(Long id);

    /**
     * 批量删除地图点位
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMapPointByIds(Long[] ids);
}
