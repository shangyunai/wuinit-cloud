package com.wuinit.common.log.enums;

/**
 * 操作状态
 * 
 * @author wuinit
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
