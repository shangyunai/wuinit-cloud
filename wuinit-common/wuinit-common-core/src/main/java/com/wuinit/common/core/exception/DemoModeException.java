package com.wuinit.common.core.exception;

/**
 * 演示模式异常
 * 
 * @author wuinit
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
