package com.wuinit.common.core.exception;

/**
 * 权限异常
 * 
 * @author wuinit
 */
public class PreAuthorizeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public PreAuthorizeException()
    {
    }
}
