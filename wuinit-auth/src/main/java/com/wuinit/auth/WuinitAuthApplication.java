package com.wuinit.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import com.wuinit.common.security.annotation.EnableRyFeignClients;

/**
 * 认证授权中心
 *
 * @author wuinit
 */
@EnableRyFeignClients
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class WuinitAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(WuinitAuthApplication.class, args);
        System.out.println("======= wuinit auth start =======");
    }
}
