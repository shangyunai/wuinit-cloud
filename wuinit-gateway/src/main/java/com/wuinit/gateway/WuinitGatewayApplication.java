package com.wuinit.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 网关启动程序
 *
 * @author wuinit
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class WuinitGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(WuinitGatewayApplication.class, args);
        System.out.println("======= wuinit gateway start =======");
    }
}
